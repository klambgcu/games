package com.kelsoft.stats;

public interface Stats
{
	public StringBuffer doReport() throws Exception;
}