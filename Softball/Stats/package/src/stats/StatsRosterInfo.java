package com.kelsoft.stats;

import java.util.Arrays;
import com.kelsoft.charts.ChartInfo;

public class StatsRosterInfo
{
	private int countPlayers  = 0;
	private int countPitchers = 0;
	private int countCatchers = 0;
	private int gamesPlayed   = 0;

	private ChartInfo[] batAvg;
	private ChartInfo[] onBase;
	private ChartInfo[] slugging;

	private ChartInfo[] batAvgSorted;
	private ChartInfo[] onBaseSorted;
	private ChartInfo[] sluggingSorted;


	public StatsRosterInfo()
	{
	}

	public StatsRosterInfo(Games games) throws Exception
	{
		// Get player count for each category (Batter, Pitcher, Catcher)
		try
		{
			Game game = games.getGame(KELStatsConstants.TAB_ROSTER);

			// Get Players Count
			int count = 2;
			while (count < (2 + KELStatsConstants.MAX_NUM_PLAYERS) && (! "".equals(game.getData(count, 0))))
				count++;
			setCountPlayers(count - 2);

			// Get Pitcher Count
			count = 2;
			while (count < (2 + KELStatsConstants.MAX_NUM_PITCHERS) && (! "".equals(game.getData(count, 3))) )
				count++;
			setCountPitchers(count - 2);

			// Get Catcher Count
			count = 2;
			while (count < (2 + KELStatsConstants.MAX_NUM_CATCHERS) && (! "".equals(game.getData(count, 6))) )
				count++;
			setCountCatchers(count - 2);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}

		// Get ChartInfo information for BatAvg, OnBasePercentage and SluggingPercentage
		try
		{
			batAvg         = new ChartInfo[getCountPlayers()];
			onBase         = new ChartInfo[getCountPlayers()];
			slugging       = new ChartInfo[getCountPlayers()];

			batAvgSorted   = new ChartInfo[getCountPlayers()];
			onBaseSorted   = new ChartInfo[getCountPlayers()];
			sluggingSorted = new ChartInfo[getCountPlayers()];

			Game game = games.getGame(KELStatsConstants.TAB_SUMMARY);
			int   player  = 0;

			gamesPlayed = Integer.parseInt(game.getData(KELStatsConstants.SUMMARY_GAMES_PLAYEDY, KELStatsConstants.SUMMARY_GAMES_PLAYEDX));

			// Get values into unsorted containers
			for (int i = KELStatsConstants.BATTING_START2;
			         i < (KELStatsConstants.BATTING_START2 + getCountPlayers());
			         i++)
			{
				String name = game.getData(i, KELStatsConstants.PLAYER_NAME_POSX);

				double avg  = Double.parseDouble(game.getData(i, KELStatsConstants.POSX_BATTING_AVERAGE));
				double obp  = Double.parseDouble(game.getData(i, KELStatsConstants.POSX_ONBASE_PERCENTAGE));
				double slg  = Double.parseDouble(game.getData(i, KELStatsConstants.POSX_SLUGGING_PERCENTAGE));

				batAvg[player]   = new ChartInfo(name, avg);
				onBase[player]   = new ChartInfo(name, obp);
				slugging[player] = new ChartInfo(name, slg);
				player++;
			}

			// Copy values into sorting container arrays
			System.arraycopy(batAvg,   0, batAvgSorted,   0, getCountPlayers());
			System.arraycopy(onBase,   0, onBaseSorted,   0, getCountPlayers());
			System.arraycopy(slugging, 0, sluggingSorted, 0, getCountPlayers());

			// Sort container arrays
			Arrays.sort(batAvgSorted);
			Arrays.sort(onBaseSorted);
			Arrays.sort(sluggingSorted);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	public int getCountPlayers()
	{
		return countPlayers;
	}

	public int getCountPitchers()
	{
		return countPitchers;
	}

	public int getCountCatchers()
	{
		return countCatchers;
	}

	public int getGamesPlayed()
	{
		return gamesPlayed;
	}

	public void setCountPlayers(int count)
	{
		countPlayers = count;
	}

	public void setCountPitchers(int count)
	{
		countPitchers = count;
	}

	public void setCountCatchers(int count)
	{
		countCatchers = count;
	}

	public void setGamesPlayed(int count)
	{
		gamesPlayed = count;
	}

	public ChartInfo[] getAll(int type) throws IllegalArgumentException
	{
		switch (type)
		{
			case KELStatsConstants.CHART_TYPE_BATAVG:   return batAvg;
			case KELStatsConstants.CHART_TYPE_ONBASE:   return onBase;
			case KELStatsConstants.CHART_TYPE_SLUGGING: return slugging;
			default:
			{
				throw new IllegalArgumentException("Parameter type must be value of constant: BATAVG, ONBASE, SLUGGING");
			}
		}
	}

	public ChartInfo[] getAllSorted(int type) throws IllegalArgumentException
	{
		switch (type)
		{
			case KELStatsConstants.CHART_TYPE_BATAVG:   return batAvgSorted;
			case KELStatsConstants.CHART_TYPE_ONBASE:   return onBaseSorted;
			case KELStatsConstants.CHART_TYPE_SLUGGING: return sluggingSorted;
			default:
			{
				throw new IllegalArgumentException("Parameter type must be value of constant: BATAVG, ONBASE, SLUGGING");
			}
		}
	}

	public ChartInfo getItem(int type, int item) throws IllegalArgumentException
	{
		// Validate item parameter
		if (item < 0 || item >= getCountPlayers())
		{
			throw new IllegalArgumentException("Parameter item must be between 0 and" + getCountPlayers());
		}

		switch (type)
		{
			case KELStatsConstants.CHART_TYPE_BATAVG:   return batAvg[item];
			case KELStatsConstants.CHART_TYPE_ONBASE:   return onBase[item];
			case KELStatsConstants.CHART_TYPE_SLUGGING: return slugging[item];
			default:
			{
				throw new IllegalArgumentException("Parameter type must be value of constant: BATAVG, ONBASE, SLUGGING");
			}
		}
	}

	public ChartInfo getItemSorted(int type, int item) throws IllegalArgumentException
	{
		// Validate item parameter
		if (item < 0 || item >= getCountPlayers())
		{
			throw new IllegalArgumentException("Parameter item must be between 0 and" + getCountPlayers());
		}

		switch (type)
		{
			case KELStatsConstants.CHART_TYPE_BATAVG:   return batAvgSorted[item];
			case KELStatsConstants.CHART_TYPE_ONBASE:   return onBaseSorted[item];
			case KELStatsConstants.CHART_TYPE_SLUGGING: return sluggingSorted[item];
			default:
			{
				throw new IllegalArgumentException("Parameter type must be value of constant: BATAVG, ONBASE, SLUGGING");
			}
		}
	}
}