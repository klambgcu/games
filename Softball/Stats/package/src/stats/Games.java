package com.kelsoft.stats;

import jxl.*;

public class Games extends StatusUpdate
{
	private Workbook  workbook;
	private int       gamesPlayed;
	private Game[]    games;

	public Games()
	{
	}

	public Games(Workbook workbook)
	{
		this.workbook = workbook;
	}

	public void parse()
	{
		int sheetCount = workbook.getNumberOfSheets();
		setProgress(2);

		//Allocate number of games (total number of sheets to be safe)
		games = new Game[sheetCount];

		// Initialize each game object
		for (int i = 0; i <= KELStatsConstants.TAB_SUMMARY; i++)
		{
			updateStatus("Parsing tab: " + workbook.getSheetNames()[i] + "\n");
			games[i] = new Game(workbook.getSheet(i));
			setProgress(getProgress() + 1);
		}

		// Get the number of games played from the summary and only read that mean games.
		gamesPlayed = Integer.parseInt(games[KELStatsConstants.TAB_SUMMARY].getData(KELStatsConstants.BATTING_END, KELStatsConstants.SUMMARY_GAMES_PLAYEDX));
		updateStatus("Found Games Played: " + gamesPlayed + "\n");
		setProgress(getProgress() + 1);

		for (int i = 0; i < gamesPlayed; i++)
		{
			updateStatus("Parsing tab: " + workbook.getSheetNames()[KELStatsConstants.TAB_GAME1 + i] + "\n");
			games[KELStatsConstants.TAB_GAME1 + i] = new Game(workbook.getSheet(KELStatsConstants.TAB_GAME1 + i));
			setProgress(getProgress() + 1);
		}
		setProgress(40);
	}

	public Game getGame(int number)
	{
		return games[number];
	}

	public int getGamesPlayed()
	{
		// Exclude number of non-game sheets
		return (gamesPlayed);
	}
}