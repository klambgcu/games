package com.kelsoft.stats;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

// 	(Game game, int startPosY, int countPlayers, String title, String[] subtitles, int[] subtitlesSize, int summaryTotalOffset)

public class StatsFactory
{
	public static final Stats create(String type, Game game, StatsRosterInfo info)
	{
		Stats stats = null;

		if (KELStatsConstants.TYPE_CATCHING.equalsIgnoreCase(type))
		{
			stats = new TeamStatsCatching(game,
										  KELStatsConstants.CATCHING_START,
										  info.getCountCatchers(),
										  "Catchers",
										  new String[] {"Catching"},
										  new int[] {12},
										  KELStatsConstants.SUMMARY_TOTAL_OFFSET_CATCHING,
										  11 // Adjust Catching number/name offset to 11
										 );
		}
		else if (KELStatsConstants.TYPE_PITCHING.equalsIgnoreCase(type))
		{
			stats = new TeamStatsPitching(game,
										  KELStatsConstants.PITCHING_START,
										  info.getCountPitchers(),
										  "Pitchers",
										  new String[] {"Pitching", "Pitching Results"},
										  new int[] {24,8},
										  KELStatsConstants.SUMMARY_TOTAL_OFFSET_PITCHING,
										  0
										 );
		}
		else if (KELStatsConstants.TYPE_BATFIELD.equalsIgnoreCase(type))
		{
			stats = new TeamStatsBattingFielding(game,
												 2,
												 info.getCountPlayers(),
												 "Players",
												 new String[] {"Batting", "Batting Results", "Fielding"},
												 new int[] {17, 24, 5},
												 KELStatsConstants.SUMMARY_TOTAL_OFFSET_BATFIELD,
												 0
												);
		}
		return stats;
	}
}