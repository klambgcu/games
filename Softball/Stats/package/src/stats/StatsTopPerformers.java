package com.kelsoft.stats;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;

public class StatsTopPerformers
{
	private static String dashes = "------------------------------------------------------------";

	private StatsTopPerformer[] players;
	private String              category;
	private PrintWriter         report;

	public StatsTopPerformers()
	{
	}

	public StatsTopPerformers(PrintWriter report)
	{
		this.report = report;
	}

	public void setCategory(String category)
	{
		this.category = category;
	}

	public void setPlayers(StatsTopPerformer[] teamPlayers)
	{
		players = new StatsTopPerformer[teamPlayers.length];
		System.arraycopy(teamPlayers, 0, players, 0, teamPlayers.length);
		Arrays.sort(players);
	}

	public void setPlayers(StatsTopPerformer[] teamPlayers, String reverse)
	{
		players = new StatsTopPerformer[teamPlayers.length];
		System.arraycopy(teamPlayers, 0, players, 0, teamPlayers.length);
		Arrays.sort(players);
		Collections.reverse(Arrays.asList(players));
	}

	public void createReport()
	{
		rankPlayers();
		report.println(category);
		report.println(dashes.substring(0, category.length()));
		printRank(KELStatsConstants.TOP_PERFORMERS);
		report.println();
	}

	private void rankPlayers()
	{
		int    currRank = 1;
		int    nextRank = 1;
		double oldValue = players[0].getValue();
		players[0].setRank(currRank);

		for(StatsTopPerformer p : players)
		{
			if (p.getValue() == oldValue)
				p.setRank(currRank);
			else
			{
				currRank = nextRank;
				p.setRank(currRank);
				oldValue = p.getValue();
			}
			nextRank++;
		}
	}

	private void printRank()
	{
		for(StatsTopPerformer p : players)
			report.println(p.toString(1));
	}

	private void printRank(int maxLines)
	{
		int currPlay = 0;
		int currLine = 0;
		int currRank = 1;
		int count    = getRankCount(1);
		boolean ok   = true;

		maxLines = Math.min(maxLines, players.length);

		while(ok)
		{
			if ((count + currLine) > maxLines)
			{
				if (currLine < maxLines)
					report.println(players[currPlay].toStringTied(count));
				currLine++;
				ok = false;
				continue;
			}
			report.println(players[currPlay++].toString(1));
			currLine++;
			for (int i = 0; i < count-1; i++)
			{
				report.println(players[currPlay++].toString(0));
				currLine++;
			}
			if (currPlay >= players.length)
			{
				ok = false;
				continue;
			}
			currRank = players[currPlay].getRank();
			count = getRankCount(currRank);
		}
	}

	private int getRankCount(int rank)
	{
		int count = 0;
		for(StatsTopPerformer p : players)
			if (p.getRank() == rank)
				count++;
		return count;
	}
}