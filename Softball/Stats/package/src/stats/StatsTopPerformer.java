package com.kelsoft.stats;

import java.util.Arrays;

public class StatsTopPerformer implements Comparable
{
	private static enum Types {INT, DOUBLE};
	private static String periods = ".........................";

	private double value;
	private int    rank;
	private String name;
	private String number;
	private Types  type;


	public StatsTopPerformer()
	{
	}

	public StatsTopPerformer(String number, String name, double value)
	{
		setName(name);
		setNumber(number);
		setRank(0);
		setValue(value);
		type = Types.DOUBLE;
	}

	public StatsTopPerformer(String number, String name, int value)
	{
		setName(name);
		setNumber(number);
		setRank(0);
		setValue((double)value);
		type = Types.INT;
	}

	public StatsTopPerformer(String number, String name, double value, int rank)
	{
		setName(name);
		setNumber(number);
		setRank(rank);
		setValue(value);
		type = Types.DOUBLE;
	}

	public StatsTopPerformer(String number, String name, int value, int rank)
	{
		setName(name);
		setNumber(number);
		setRank(rank);
		setValue((double)value);
		type = Types.INT;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}

	public void setRank(int rank)
	{
		this.rank = rank;
	}

	public void setValue(double value)
	{
		this.value = value;
		type = Types.DOUBLE;
	}

	public void setValue(int value)
	{
		this.value = (double)value;
		type = Types.INT;
	}

	public String getName()
	{
		return this.name;
	}

	public String getNumber()
	{
		return this.number;
	}

	public int getRank()
	{
		return this.rank;
	}

	public double getValue()
	{
		return this.value;
	}

	public String toString()
	{
		if (type == Types.DOUBLE)
			return String.format("%2s %25s %05.3f", getNumber(), getName() + periods.substring(0, 25-getName().length()), getValue());
		else
			return String.format("%2s %25s %d",     getNumber(), getName() + periods.substring(0, 25-getName().length()), (int)getValue());
	}

	public String toString(int printRank)
	{
		if (printRank != 0)
			return String.format("%2d.  ", getRank()) + this.toString();
		else
			return "     " + this.toString();
	}

	public String toStringTied(int amount)
	{
		String tied1 = String.format("%d %s",amount, "players tied at");
		String tied2 = String.format("%2d.     %25s", getRank(), tied1 + periods.substring(0, 25 - tied1.length()));

		if (type == Types.DOUBLE)
			return tied2 + String.format(" %05.3f", getValue());
		else
			return tied2 + String.format(" %d",     (int)getValue());
	}

	public int compareTo(Object o)
	{
		int result = 0;

		// Order in descending value
		if (this.value > ((StatsTopPerformer) o).getValue())
		{
			result = -1;
		}
		else if (this.value < ((StatsTopPerformer) o).getValue())
		{
			result = 1;
		}
		else
		{
			// Values same, order by name ascending next
			result = this.name.compareTo(((StatsTopPerformer) o).getName());
		}
		return result;
	}
}