package com.kelsoft.stats;

import com.kelsoft.stats.Configuration;
import com.kelsoft.stats.KELStatsConstants;
import com.kelsoft.stats.StatsRosterInfo;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.colorchooser.*;

public class HTMLEditor extends JDialog implements ActionListener
{
	private Frame           mainFrame;
	private Configuration   config = new Configuration();
	private Configuration   origConfig;
	private Configuration   newConfiguration = null;
	private StatsRosterInfo rosterInfo;
	private JLabel          htmlLabel;
	private JButton         buttonOK;
	private JButton         buttonCancel;
	private JButton         buttonReset;
	private JColorChooser   colorChooser = new JColorChooser();

	public HTMLEditor(Frame owner, StatsRosterInfo rosterInfo, Configuration config)
	{
		super(owner, "HTML Configuration Editor", true);
		this.mainFrame  = owner;
		this.config.copy(config);
		this.origConfig = config;
		this.rosterInfo = rosterInfo;

		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setResizable(false);
		add(createPanel());
		pack();
		setLocationRelativeTo(mainFrame);
	}

	private JPanel createPanel()
	{
		JPanel previewPanel = new JPanel();
		previewPanel.setBorder(BorderFactory.createTitledBorder(" Preview: "));

		htmlLabel = new JLabel(getHtmlSampleIcon());
		htmlLabel.setBorder(LineBorder.createBlackLineBorder());
		previewPanel.add(htmlLabel);

		buttonCancel = new JButton("Cancel");
		buttonCancel.addActionListener(this);
		buttonCancel.setActionCommand("buttonCancel");

		buttonOK = new JButton("OK");
		buttonOK.addActionListener(this);
		buttonOK.setActionCommand("buttonOK");

		buttonReset = new JButton("Reset");
		buttonReset.addActionListener(this);
		buttonReset.setActionCommand("buttonReset");

		JPanel southPanel = new JPanel();
		southPanel.setLayout(new BorderLayout());
		southPanel.setBorder(BorderFactory.createTitledBorder(""));

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(6,5,5,5));
		buttonPanel.setBorder(BorderFactory.createTitledBorder(" Edit Colors: "));

		JLabel h;
		JButton b;
		h = new JLabel("Area:");       h.setHorizontalAlignment(SwingConstants.CENTER); buttonPanel.add(h);
		h = new JLabel("Background:"); h.setHorizontalAlignment(SwingConstants.CENTER); buttonPanel.add(h);
		h = new JLabel("Font:");       h.setHorizontalAlignment(SwingConstants.CENTER); buttonPanel.add(h);
		h = new JLabel("Visited:");    h.setHorizontalAlignment(SwingConstants.CENTER); buttonPanel.add(h);
		h = new JLabel("Hover:");      h.setHorizontalAlignment(SwingConstants.CENTER); buttonPanel.add(h);

		h = new JLabel("Players:"); h.setHorizontalAlignment(SwingConstants.CENTER); buttonPanel.add(h);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editP0"); buttonPanel.add(b);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editP1"); buttonPanel.add(b);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editP2"); buttonPanel.add(b);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editP3"); buttonPanel.add(b);

		h = new JLabel("Heading:"); h.setHorizontalAlignment(SwingConstants.CENTER); buttonPanel.add(h);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editH0"); buttonPanel.add(b);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editH1"); buttonPanel.add(b);
		buttonPanel.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPanel.add(Box.createRigidArea(new Dimension(10, 0)));

		h = new JLabel("Detail:");  h.setHorizontalAlignment(SwingConstants.CENTER); buttonPanel.add(h);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editD0"); buttonPanel.add(b);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editD1"); buttonPanel.add(b);
		buttonPanel.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPanel.add(Box.createRigidArea(new Dimension(10, 0)));

		h = new JLabel("Totals:");  h.setHorizontalAlignment(SwingConstants.CENTER); buttonPanel.add(h);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editT0"); buttonPanel.add(b);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editT1"); buttonPanel.add(b);
		buttonPanel.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPanel.add(Box.createRigidArea(new Dimension(10, 0)));

		h = new JLabel("Grayed:");  h.setHorizontalAlignment(SwingConstants.CENTER); buttonPanel.add(h);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editG0"); buttonPanel.add(b);
		b = new JButton("Edit..."); b.addActionListener(this); b.setActionCommand("editG1"); buttonPanel.add(b);
		buttonPanel.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPanel.add(Box.createRigidArea(new Dimension(10, 0)));

		JPanel actionPanel = new JPanel();
		actionPanel.setLayout(new GridLayout(1,3,5,5));
		actionPanel.setBorder(BorderFactory.createTitledBorder(" Confirm Action: "));
		actionPanel.add(buttonOK);
		actionPanel.add(buttonCancel);
		actionPanel.add(buttonReset);

		JPanel actionGrid = new JPanel();
		actionGrid.setLayout(new GridLayout(3,1,5,5));
		actionGrid.add(Box.createRigidArea(new Dimension(0, 20)));
		actionGrid.add(Box.createRigidArea(new Dimension(0, 20)));
		actionGrid.add(actionPanel);

		southPanel.add(buttonPanel, BorderLayout.WEST);
		southPanel.add(actionGrid,  BorderLayout.EAST);

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());

		panel.add(previewPanel, BorderLayout.NORTH);
		panel.add(southPanel,   BorderLayout.SOUTH);

		return panel;
	}

	private ImageIcon getHtmlSampleIcon()
	{
		HTMLImage     html       = null;
		BufferedImage sampleHtml = null;
		BufferedImage smallHtml  = null;

		try
		{
			html = new HTMLImage(rosterInfo, config);
			sampleHtml = (BufferedImage)html.getHtmlImage();
		}
		catch (Exception ignore)
		{
		}

		return new ImageIcon(sampleHtml);
	}

	public Configuration getConfiguration()
	{
		return newConfiguration;
	}

	public void actionPerformed(ActionEvent evt)
	{
		String action = evt.getActionCommand();
		String color = "#000000";
		String title = "Color";

		if ("buttonCancel".equals(action))
		{
			//newConfiguration = origConfiguration; - // leave null
			setVisible(false);
		}
		else if ("buttonOK".equals(action))
		{
			newConfiguration = config;
			setVisible(false);
		}
		else if ("buttonReset".equals(action))
		{
			config.copy(origConfig);
			htmlLabel.setIcon(getHtmlSampleIcon());
		}
		else
		{
			if ("editP0".equals(action))
			{
				color = config.getProperty(KELStatsConstants.PLAYER_COLOR_PROPERTY);
				title = "Choose Player Background Color";
			}
			else if ("editP1".equals(action))
			{
				color = config.getProperty(KELStatsConstants.PLAYER_COLOR_FONT_NORMAL_PROPERTY);
				title = "Choose Player Font Color";
			}
			else if ("editP2".equals(action))
			{
				color = config.getProperty(KELStatsConstants.PLAYER_COLOR_FONT_VISITED_PROPERTY);
				title = "Choose Player Font Visited Color";
			}
			else if ("editP3".equals(action))
			{
				color = config.getProperty(KELStatsConstants.PLAYER_COLOR_FONT_HOVER_PROPERTY);
				title = "Choose Player Font Hover Color";
			}
			else if ("editH0".equals(action))
			{
				color = config.getProperty(KELStatsConstants.HEADING_COLOR_PROPERTY);
				title = "Choose Heading Background Color";
			}
			else if ("editH1".equals(action))
			{
				color = config.getProperty(KELStatsConstants.HEADING_COLOR_FONT_PROPERTY);
				title = "Choose Heading Font Color";
			}
			else if ("editD0".equals(action))
			{
				color = config.getProperty(KELStatsConstants.DETAIL_COLOR_PROPERTY);
				title = "Choose Detail Background Color";
			}
			else if ("editD1".equals(action))
			{
				color = config.getProperty(KELStatsConstants.DETAIL_COLOR_FONT_PROPERTY);
				title = "Choose Detail Font Color";
			}
			else if ("editT0".equals(action))
			{
				color = config.getProperty(KELStatsConstants.TOTALS_COLOR_PROPERTY);
				title = "Choose Totals Background Color";
			}
			else if ("editT1".equals(action))
			{
				color = config.getProperty(KELStatsConstants.TOTALS_COLOR_FONT_PROPERTY);
				title = "Choose Totals Font Color";
			}
			else if ("editG0".equals(action))
			{
				color = config.getProperty(KELStatsConstants.DETAIL_GRAYED_PROPERTY);
				title = "Choose Grayed Area Background Color";
			}
			else if ("editG1".equals(action))
			{
				color = config.getProperty(KELStatsConstants.DETAIL_GRAYED_FONT_PROPERTY);
				title = "Choose Grayed Area Font Color";
			}

			Color newColor = colorChooser.showDialog(mainFrame, title, Color.decode(color));
			if (newColor != null)
			{
				String newValue = String.format("#%02X%02X%02X", newColor.getRed(), newColor.getGreen(), newColor.getBlue());

				if ("editP0".equals(action))
				{
					config.setProperty(KELStatsConstants.PLAYER_COLOR_PROPERTY, newValue);
				}
				else if ("editP1".equals(action))
				{
					config.setProperty(KELStatsConstants.PLAYER_COLOR_FONT_NORMAL_PROPERTY, newValue);
				}
				else if ("editP2".equals(action))
				{
					config.setProperty(KELStatsConstants.PLAYER_COLOR_FONT_VISITED_PROPERTY, newValue);
				}
				else if ("editP3".equals(action))
				{
					config.setProperty(KELStatsConstants.PLAYER_COLOR_FONT_HOVER_PROPERTY, newValue);
				}
				else if ("editH0".equals(action))
				{
					config.setProperty(KELStatsConstants.HEADING_COLOR_PROPERTY, newValue);
				}
				else if ("editH1".equals(action))
				{
					config.setProperty(KELStatsConstants.HEADING_COLOR_FONT_PROPERTY, newValue);
				}
				else if ("editD0".equals(action))
				{
					config.setProperty(KELStatsConstants.DETAIL_COLOR_PROPERTY, newValue);
				}
				else if ("editD1".equals(action))
				{
					config.setProperty(KELStatsConstants.DETAIL_COLOR_FONT_PROPERTY, newValue);
				}
				else if ("editT0".equals(action))
				{
					config.setProperty(KELStatsConstants.TOTALS_COLOR_PROPERTY, newValue);
				}
				else if ("editT1".equals(action))
				{
					config.setProperty(KELStatsConstants.TOTALS_COLOR_FONT_PROPERTY, newValue);
				}
				else if ("editG0".equals(action))
				{
					config.setProperty(KELStatsConstants.DETAIL_GRAYED_PROPERTY, newValue);
				}
				else if ("editG1".equals(action))
				{
					config.setProperty(KELStatsConstants.DETAIL_GRAYED_FONT_PROPERTY, newValue);
				}
				htmlLabel.setIcon(getHtmlSampleIcon());
			}
		}
	}
}

