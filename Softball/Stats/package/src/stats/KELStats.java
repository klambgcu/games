package com.kelsoft.stats;

import com.kelsoft.charts.Chart;
import com.kelsoft.charts.ChartsFactory;
import com.kelsoft.charts.ChartsWorker;
import com.kelsoft.charts.ChartEditor;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.beans.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.colorchooser.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;
import jxl.*;

public class KELStats extends JPanel implements ActionListener, PropertyChangeListener
{
	private ChartsWorker       summaryCharts;
	private Configuration      config = new Configuration();
	private Games              games;
	private JButton[]          buttonEdit = new JButton[3];
	private JColorChooser      colorChooser = new JColorChooser();
	private JFrame             mainFrame;
	private JLabel             chartLabel;
	private JLabel             htmlLabel;
	private JLabel             teamNameLabel;
	private JProgressBar       progressBar;
	private JTextArea          statusArea;
	private PlayerStats        reportPlayers;
	private PrintWriter        report;
	private StatsRosterInfo    rosterInfo;
	private StatsTopCategories reportTopCategories;
	private TeamStats          reportTeam;

	public KELStats()
	{
		super();
		config.loadProperties();
		config.saveProperties();
	}

	public JMenuBar createMenu(JFrame frm)
	{
		final JFrame frame = frm;
		mainFrame = frm;
		JMenuBar menuBar = new JMenuBar();

		/***********************/
		/* Menu Section - FILE */
		/***********************/
		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic('F');

		JMenuItem itemFileOpen = new JMenuItem("Open...");
		itemFileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		itemFileOpen.addActionListener(new FileOpenHandler(frame));
		menuFile.add(itemFileOpen);

		menuFile.addSeparator();

		JMenuItem itemFileSaveConfiguration = new JMenuItem("Save Configuration");
		itemFileSaveConfiguration.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		itemFileSaveConfiguration.setActionCommand("saveConfig");

		itemFileSaveConfiguration.addActionListener(this);
		menuFile.add(itemFileSaveConfiguration);

		menuFile.addSeparator();

		JMenuItem itemFileExit = new JMenuItem("Exit...");
		itemFileExit.setMnemonic('x');
		itemFileExit.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					System.exit(0);
				}
			}
		);
		menuFile.add(itemFileExit);

		menuBar.add(menuFile);

		/***********************/
		/* Menu Section - HELP */
		/***********************/
		JMenu menuHelp = new JMenu("Help");
		menuHelp.setMnemonic('H');

		JMenuItem itemHelpHelp = new JMenuItem("Help...");
		itemHelpHelp.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		itemHelpHelp.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					JOptionPane.showMessageDialog(frame,
												  "1. Configure Colors (if needed and save if desired)\n" +
												  "2. Open Stats Spreadsheet\n" +
												  "3. Spreadsheet will be validated for correct version\n" +
												  "4. A TeamStats.html file will be created\n" +
												  "5. A Charts directory will be created (if not already existing)\n" +
												  "6. A PlayerStats directory will be created (if not already existing)\n" +
												  "7. Batting Average, On-Base Percentage and Slugging Percentage charts will be created. \n" +
												  "8. Individual Player Stat files will be created and can be viewed thru the TeamStats.html file.\n" +
												  "9. The charts can also be viewed thru the TeamStats.html file by clicking on thumbnail images.\n" +
												  "-----------------------------------------------------------------------------------------------\n" +
												  "Troubleshooting steps\n" +
												  "1. Make sure you have java 1.6.0.18 or higher correctly installed.\n" +
												  "2. Make sure your Spreadsheet template is version " + KELStatsConstants.VERSION_ID_VALID + " - check the \"Instructions\" tab to confirm.\n" +
												  "3. Make sure that you have read/write privileges in the directory.\n" +
												  "4. SoftballStats.ini may be corrupted, delete the file and try opening the program again.\n",
												  "Help",
												  JOptionPane.INFORMATION_MESSAGE);
				}
			}
		);

		menuHelp.add(itemHelpHelp);

		menuHelp.addSeparator();

		JMenuItem itemHelpAbout = new JMenuItem("About...");
		itemHelpAbout.setMnemonic('A');
		itemHelpAbout.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					JOptionPane.showMessageDialog(frame,
												  "Softball Stats 4.0.0\n" +
												  "Date: 05/01/2007\n" +
												  "Copyright: 2007 - 2050 Kelly Lamb Software\n" +
												  "All Rights Reserved\n" +
												  "-------------------------------------------------\n" +
												  "Requires Java 1.6.0.18 or higher\n" +
												  "Requires KELStats Spreadsheet template version " + KELStatsConstants.VERSION_ID_VALID,
												  "About",
												  JOptionPane.INFORMATION_MESSAGE);
				}
			}
		);

		menuHelp.add(itemHelpAbout);

		menuBar.add(menuHelp);

		return menuBar;
	}

	private JPanel createPanel()
	{
		JPanel panelMain = new JPanel();
		panelMain.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		panelMain.setLayout(new BorderLayout());

		JPanel panelSouth = new JPanel();
		panelSouth.setBorder(BorderFactory.createEmptyBorder(0,5,5,5));
		panelSouth.setLayout(new BorderLayout());

		statusArea = new JTextArea(10, 64);
		//statusArea.setCaretPosition(statusArea.getDocument().getLength());
		//statusArea.moveCaretPosition(statusArea.getDocument().getLength());
		statusArea.setEditable(false);

		JScrollPane scrollPane = new JScrollPane(statusArea);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setBorder(BorderFactory.createTitledBorder("Status:"));

        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);

		panelSouth.add(scrollPane,  BorderLayout.CENTER);
		panelSouth.add(progressBar, BorderLayout.SOUTH);

		JPanel panelNorth = new JPanel();
		panelNorth.setBorder(BorderFactory.createEmptyBorder(5,5,0,5));
		panelNorth.setLayout(new BorderLayout());

		JLabel logoLabel = new JLabel(createImageIcon("images/Logo.png", "Softball Stats Logo"));
		logoLabel.setBorder(BorderFactory.createTitledBorder(""));
		logoLabel.setVerticalAlignment(SwingConstants.TOP);
		logoLabel.setIconTextGap(0);

		panelNorth.add(logoLabel,           BorderLayout.WEST);
		panelNorth.add(createConfigPanel(), BorderLayout.EAST);

		panelMain.add(panelSouth, BorderLayout.SOUTH);
		panelMain.add(panelNorth, BorderLayout.NORTH);

		return panelMain;
	}

	private JPanel createConfigPanel()
	{
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder(""));

		JPanel pane1 = new JPanel();
		JPanel pane2 = new JPanel();
		JPanel pane3 = new JPanel();

		pane1.setBorder(LineBorder.createGrayLineBorder());
		pane2.setBorder(LineBorder.createGrayLineBorder());
		pane3.setBorder(LineBorder.createGrayLineBorder());

		for (int i = 0; i < buttonEdit.length; i++)
		{
			buttonEdit[i] = new JButton("Edit...");
			buttonEdit[i].addActionListener(this);
			buttonEdit[i].setActionCommand("button" + i);
			buttonEdit[i].setAlignmentX(Component.CENTER_ALIGNMENT);
		}

		// Define pane1 - Chart Color Tab
		try
		{
			chartLabel = new JLabel(getChartSampleIcon());
			chartLabel.setBorder(LineBorder.createBlackLineBorder());
			pane1.add(chartLabel);
			pane1.add(buttonEdit[0]);
		}
		catch (Exception e)
		{
		}

		// Define pane2 - HTML Color Tab
		try
		{
			htmlLabel = new JLabel(getHtmlSampleIcon());
			htmlLabel.setBorder(LineBorder.createBlackLineBorder());
			pane2.add(htmlLabel);
			pane2.add(buttonEdit[1]);
		}
		catch (Exception e)
		{
		}

		// Define pane3 - Team Name Tab
		String name = config.getProperty(KELStatsConstants.TEAM_NAME_PROPERTY, KELStatsConstants.TEAM_NAME_DEFAULTS);
		teamNameLabel = new JLabel(" Team Name:   " + name + " ");
		teamNameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		teamNameLabel.setBorder(LineBorder.createBlackLineBorder());

		pane3.setLayout(new BoxLayout(pane3, BoxLayout.Y_AXIS));
		pane3.add(Box.createRigidArea(new Dimension(0,90)));
		pane3.add(teamNameLabel);
		pane3.add(Box.createRigidArea(new Dimension(0,79)));
		pane3.add(buttonEdit[2]);

		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.addTab("Chart Colors", null, pane1, "Configure Chart Colors");
		tabbedPane.addTab("HTML Colors",  null, pane2, "Configure HTML Colors");
		tabbedPane.addTab("Team Name",    null, pane3, "Configure Team Name");

		tabbedPane.setMinimumSize( new Dimension(400,250));
		tabbedPane.setPreferredSize( tabbedPane.getMinimumSize());

		panel.add(tabbedPane, BorderLayout.NORTH);

		return panel;
	}

	private ImageIcon getChartSampleIcon()
	{
		Chart         chart       = null;
		BufferedImage sampleChart = null;
		BufferedImage smallChart  = null;

		try
		{
			chart = ChartsFactory.create(KELStatsConstants.CHART_TYPE_BAR_CONFIG, rosterInfo, config);
			sampleChart = (BufferedImage)chart.doChart();
			int w = 390;
			int h = 175;

			smallChart = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = smallChart.createGraphics();
			g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
			g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC));
			g2d.drawImage(sampleChart, 0,0, w, h, null);
		}
		catch (Exception ignore)
		{
		}

		return new ImageIcon(smallChart);
	}

	private ImageIcon getHtmlSampleIcon()
	{
		HTMLImage     html       = null;
		BufferedImage sampleHtml = null;
		BufferedImage smallHtml  = null;

		try
		{
			html = new HTMLImage(rosterInfo, config);
			sampleHtml = (BufferedImage)html.getHtmlImage();
			int w = 390;
			int h = 175;

			smallHtml = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = smallHtml.createGraphics();
			g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
			g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC));
			g2d.drawImage(sampleHtml, 0,0, w, h, null);
		}
		catch (Exception ignore)
		{
		}

		return new ImageIcon(smallHtml);
	}

	/************ createReport ************/

	private void createReport(final String fileName) throws Exception
	{
		try
		{
			progressBar.setValue(0);
			statusArea.setText("Opening: " + fileName + "\n");
			Workbook workbook = Workbook.getWorkbook(new File(fileName));
			progressBar.setValue(1);

			statusArea.append("Validating Spreadsheet version...");
			checkSpreadsheetVersion(workbook);
			statusArea.append("Completed.\n");
			progressBar.setValue(2);

			statusArea.append("\nBeginning Section: Parsing Spreadsheet Data\n");
			games = new Games(workbook);
			games.addPropertyChangeListener(this);
			games.parse();
			statusArea.append("Completed Section: Parsing Spreadsheet Data\n");

			statusArea.append("Calculating Roster Data...");
			rosterInfo = new StatsRosterInfo(games);
			statusArea.append("Completed.\n\n");
			progressBar.setValue(progressBar.getValue() + 2);

			// Create Team Stats Section
			String outFile = "TeamStats.html";
			report = new PrintWriter (new FileWriter(outFile));

			reportTeam = new TeamStats(games, rosterInfo, report, config);
			reportTeam.addPropertyChangeListener(this);
			reportTeam.createReport();

			// Create Summary Charts Section
			summaryCharts = new ChartsWorker(rosterInfo, config);
			summaryCharts.addPropertyChangeListener(this);
			summaryCharts.makeCharts();

			// Create Player Stats Section
			reportPlayers = new PlayerStats(games, rosterInfo, report, config);
			reportPlayers.addPropertyChangeListener(this);
			reportPlayers.createReport();

			// Create Top Performers By Category
			reportTopCategories = new StatsTopCategories(games, rosterInfo, config);
			reportTopCategories.addPropertyChangeListener(this);
			reportTopCategories.createReport();

			statusArea.append("\nAttempting to launch the default browser to view the results in TeamStats.html file.\n");
			statusArea.setCaretPosition(statusArea.getDocument().getLength());
			BareBonesBrowserLaunch.openURL(outFile);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally
		{
			try
			{
				if (report != null)
					report.close();
			}
			catch (Exception ignore)
			{}
		}
	}

	private void checkSpreadsheetVersion(Workbook workbook) throws Exception
	{
		boolean valid = true;
		try
		{
			Sheet  v_tab = workbook.getSheet(KELStatsConstants.VERSION_ID_TAB);
			String v_str = v_tab.getCell(KELStatsConstants.VERSION_ID_LOCX,
										 KELStatsConstants.VERSION_ID_LOCY).getContents().trim();
			int    v_id  = Integer.parseInt(v_str);

			if (KELStatsConstants.VERSION_ID_VALID != v_id)
			{
				valid = false;
			}
		}
		catch (Exception e)
		{
			valid = false;
			e.printStackTrace();
		}

		if (! valid)
		{
			JOptionPane.showMessageDialog(null,
										  "An incorrect version of the statistics spreadsheet was selected.\n" +
										  "This program requires version " + KELStatsConstants.VERSION_ID_VALID +
										  ".\n Please select a different spreadsheet.",
										  "Spreadsheet Version Error",
										  JOptionPane.ERROR_MESSAGE);
			throw new Exception("Incorrect Spreadsheet version!");
		}
	}

	/**
	 * Invoked when task's progress property changes or status is updated.
	 */
	public void propertyChange(PropertyChangeEvent evt)
	{
		if ("progress" == evt.getPropertyName())
		{
			int progress = (Integer) evt.getNewValue();
			progressBar.setValue(progress);
		}
		else if ("updateStatus" == evt.getPropertyName())
		{
			String statusMsg = (String) evt.getNewValue();
			statusArea.append(statusMsg);
			statusArea.setCaretPosition(statusArea.getDocument().getLength());
			//statusArea.moveCaretPosition(statusArea.getDocument().getLength());
		}
	}

	public void actionPerformed(ActionEvent evt)
	{
		String action = evt.getActionCommand();
		String newValue = "#000000";

		if ("saveConfig".equals(action))
		{
			config.saveProperties();
		}
		else if ("button0".equals(action))
		{
			ChartEditor chartEditor = new ChartEditor(mainFrame, rosterInfo, config);
			chartEditor.setVisible(true);
			Configuration c = chartEditor.getConfiguration();
			if (c != null)
			{
				config.copy(c);
				chartLabel.setIcon(getChartSampleIcon());
			}
		}
		else if ("button1".equals(action))
		{
			HTMLEditor htmlEditor = new HTMLEditor(mainFrame, rosterInfo, config);
			htmlEditor.setVisible(true);
			Configuration c = htmlEditor.getConfiguration();
			if (c != null)
			{
				config.copy(c);
				htmlLabel.setIcon(getHtmlSampleIcon());
			}
		}
		else if ("button2".equals(action))
		{
			String name = config.getProperty(KELStatsConstants.TEAM_NAME_PROPERTY, KELStatsConstants.TEAM_NAME_DEFAULTS);
			String newName = (String)JOptionPane.showInputDialog(mainFrame,
																"Please Enter your team's name: ",
																"Team Name Configuration",
																JOptionPane.INFORMATION_MESSAGE,
																null,
																null,
																name);

			if ((newName != null) && (newName.length() > 0))
			{
				teamNameLabel.setText(" Team Name:   " + newName + " ");
				config.setProperty(KELStatsConstants.TEAM_NAME_PROPERTY, newName);
			}
		}
	}

	/** Returns an ImageIcon, or null if the path was invalid. */
	protected ImageIcon createImageIcon(String path, String description)
	{
		java.net.URL imgURL = getClass().getResource(path);
		if (imgURL != null)
		{
			return new ImageIcon(imgURL, description);
		}
		else
		{
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event dispatch thread.
	 */
	private void createAndShowGUI()
	{
		//Create and set up the window.
		final JFrame frame = new JFrame("Softball Stats");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setIconImage(createImageIcon("images/LogoIcon.png", "Softball Stats LogoIcon").getImage());
		//Add content to the window.
		frame.setJMenuBar(createMenu(frame));
		frame.getContentPane().add(createPanel(), BorderLayout.SOUTH);

		//Display the window.
		frame.pack();
		frame.setLocationRelativeTo(null); // Move to center of the screen
		frame.setVisible(true);
	}

	public static void main(String[] args)
	{
		//Schedule a job for the event dispatching thread:
		//creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				KELStats stats = new KELStats();
				try
				{
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
					System.exit(1);
				}
				stats.createAndShowGUI();
			}
		});
	}

	class FileOpenHandler implements ActionListener
	{
		JFrame frame;

		FileOpenHandler()
		{
			super();
		}

		public FileOpenHandler(JFrame frm)
		{
			super();
			frame = frm;
		}

		public void actionPerformed( ActionEvent event )
		{
			// Get the current directory
			File currentDirectory = new File (".");

			// Create a file chooser
			JFileChooser fc = new JFileChooser(currentDirectory);
			fc.setAcceptAllFileFilterUsed(true);
			fc.addChoosableFileFilter(new XLSFileFilter());

			// In response to a button click:
			int returnVal = fc.showOpenDialog(frame);
			if (returnVal == JFileChooser.APPROVE_OPTION)
			{
				final File file = fc.getSelectedFile();
				try
				{
					Thread a = new Thread()
					{
						public void run()
						{
							try
							{
								createReport(file.getAbsolutePath());
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
						}
					};
					a.start();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	} // end FileOpenHandler

	class XLSFileFilter extends FileFilter
	{
		public boolean accept(File f)
		{
			if (f.isDirectory())
			{
				return true;
			}
			if (getExtension(f).equals("xls"))
				return true;
			else
				return false;
		}

		// The description of this filter
		public String getDescription()
		{
			return "Excel Files (*.xls)";
		}

		// Get the extension of a file.
		private String getExtension(File f)
		{
			String ext = "";
			String s = f.getName();
			int i = s.lastIndexOf('.');
			if (i > 0 &&  i < s.length() - 1)
				ext = s.substring(i+1).toLowerCase();
			return ext;
		}
	}  // end XLSFileFilter
} // end KELStats

