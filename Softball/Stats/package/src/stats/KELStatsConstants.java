package com.kelsoft.stats;

public class KELStatsConstants
{
	public static final int VERSION_ID_LOCX  =  0;
	public static final int VERSION_ID_LOCY  = 10;
	public static final int VERSION_ID_TAB   =  0;
	public static final int VERSION_ID_VALID = 11;

	public static final int MAX_NUM_PLAYERS  = 16;
	public static final int MAX_NUM_PITCHERS =  8;
	public static final int MAX_NUM_CATCHERS =  8;

	// Pitching offsets
	public static final int PITCHING_START   = 42;
	public static final int PITCHING_END     = 50;
	public static final int PITCHING_START2  = 54;
	public static final int PITCHING_END2    = 62;
	public static final int PITCHINGX_END    = 25;
	public static final int PITCHINGX_END2   =  9;

	// Batting offsets
	public static final int BATTING_START    =  2;
	public static final int BATTING_END      = 18;
	public static final int BATTING_START2   = 22;
	public static final int BATTING_END2     = 38;
	public static final int BATTINGX1_END    = 24;
	public static final int BATTINGX2_END    = 19;

	// Fielding offsets
	public static final int FIELDINGX_START  = 21;
	public static final int FIELDINGX_END    = 25;
	public static final int FIELDING_START   = 22;

	// Catching offsets
	public static final int CATCHING_START   = 54;
	public static final int CATCHING_END     = 62;
	public static final int CATCHINGX_END    = 26;

	public static final int SUMMARY_GAMES_PLAYEDX =  2;
	public static final int SUMMARY_GAMES_PLAYEDY = 18;

	public static final int TAB_INSTRUCTIONS =  0;
	public static final int TAB_DEFINITIONS  =  1;
	public static final int TAB_ROSTER       =  2;
	public static final int TAB_SUMMARY      =  3;
	public static final int TAB_GAME1        =  4;

	public static final int PLAYER_NUM_POSX  =  0;
	public static final int PLAYER_NAME_POSX =  1;

	public static final int SUMMARY_TOTAL_OFFSET_BATFIELD = 16;
	public static final int SUMMARY_TOTAL_OFFSET_CATCHING = 8;
	public static final int SUMMARY_TOTAL_OFFSET_PITCHING = 8;

	public static final String TYPE_CATCHING = "Catcher";
	public static final String TYPE_BATFIELD = "BatterFielder";
	public static final String TYPE_PITCHING = "Pitcher";

	public static final String STATS_HINT     = "title='Individual Game Statistics'";
	public static final String DIRECTORY_NAME = "PlayerStats";
	public static final String STATS_LOCATION = DIRECTORY_NAME + "/player_";

	public static final String TOP_CATEGORIES = "category_leaders.html";
	public static final int TOP_PERFORMERS    = 5;

	// Configuration Section
	public static final String CONFIG_FILE    = "SoftballStats.ini";

	public static final String TEAM_NAME_PROPERTY = "TeamName";
	public static final String TEAM_NAME_DEFAULTS = "All Stars";

    public static final String DETAIL_COLOR_PROPERTY              = "DetailColor";
    public static final String DETAIL_COLOR_DEFAULTS              = "#FFFFFF";

    public static final String DETAIL_COLOR_FONT_PROPERTY         = "DetailFontColor";
    public static final String DETAIL_COLOR_FONT_DEFAULTS         = "#000000";

    public static final String DETAIL_GRAYED_PROPERTY             = "DetailGrayed";
    public static final String DETAIL_GRAYED_DEFAULTS             = "#404040";

    public static final String DETAIL_GRAYED_FONT_PROPERTY        = "DetailFontGrayed";
    public static final String DETAIL_GRAYED_FONT_DEFAULTS        = "#404040";

    public static final String TOTALS_COLOR_PROPERTY              = "TotalsColor";
    public static final String TOTALS_COLOR_DEFAULTS              = "#C0C0C0";

    public static final String TOTALS_COLOR_FONT_PROPERTY         = "TotalsFontColor";
    public static final String TOTALS_COLOR_FONT_DEFAULTS         = "#000000";

    public static final String HEADING_COLOR_PROPERTY             = "HeadingColor";
    public static final String HEADING_COLOR_DEFAULTS             = "#C0C0C0";

    public static final String HEADING_COLOR_FONT_PROPERTY        = "HeadingFontColor";
    public static final String HEADING_COLOR_FONT_DEFAULTS        = "#000000";

    public static final String PLAYER_COLOR_PROPERTY              = "PlayerColor";
    public static final String PLAYER_COLOR_DEFAULTS              = "#C0C0C0";

    public static final String PLAYER_COLOR_FONT_NORMAL_PROPERTY  = "PlayerFontColorNormal";
    public static final String PLAYER_COLOR_FONT_NORMAL_DEFAULTS  = "#000000";

    public static final String PLAYER_COLOR_FONT_VISITED_PROPERTY = "PlayerFontColorVisited";
    public static final String PLAYER_COLOR_FONT_VISITED_DEFAULTS = "#000000";

    public static final String PLAYER_COLOR_FONT_HOVER_PROPERTY   = "PlayerFontColorHover";
    public static final String PLAYER_COLOR_FONT_HOVER_DEFAULTS   = "#FFFFFF";

	public static final String CHART_BACKGROUND_COLOR_PROPERTY    = "ChartBackGroundColor";
	public static final String CHART_BACKGROUND_COLOR_DEFAULTS    = "#008080";

	public static final String CHART_BARTOP_COLOR_PROPERTY        = "ChartBarTopColor";
	public static final String CHART_BARTOP_COLOR_DEFAULTS        = "#00FFFF";

	// Charts Section
	public static final String CHART_TYPE_BAR_BATAVG = "Batting Average Chart";
	public static final String CHART_TYPE_BAR_ONBASE = "On-Base Percentage Chart";
	public static final String CHART_TYPE_BAR_SLUGPT = "Slugging Percentage Chart";
	public static final String CHART_TYPE_BAR_CONFIG = "Sample Configuration Chart";

	public static final int POSX_BATTING_AVERAGE     = 7;
	public static final int POSX_ONBASE_PERCENTAGE   = 8;
	public static final int POSX_SLUGGING_PERCENTAGE = 9;

	public static final int CHART_TYPE_BATAVG   = 0;
	public static final int CHART_TYPE_ONBASE   = 1;
	public static final int CHART_TYPE_SLUGGING = 2;

	public static final String CHART_DIRECTORY_NAME  = "Charts";
	public static final String CHART_BAR_BATAVG_FILE = "BATAVG.png";
	public static final String CHART_BAR_ONBASE_FILE = "ONBASE.png";
	public static final String CHART_BAR_SLUGPT_FILE = "SLUGGING.png";


	public static final String HTML_COMMENT_LINE = "<!-- Softball Stats By Kelly - (KELStats / KELSoft / Kelly Lamb Software) -->";
}