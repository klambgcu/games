package com.kelsoft.stats;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class StatusUpdate
{
	private final PropertyChangeSupport pcs = new PropertyChangeSupport( this );
	private static int progress = 0;

	public StatusUpdate()
	{
	}

	protected final void updateStatus( String msg )
	{
		this.pcs.firePropertyChange("updateStatus", "", msg);
	}

	protected final void setProgress( int aprogress )
	{
		aprogress = Math.min(100, Math.max(0, aprogress));
		if (this.progress == aprogress)
		{
			return;
		}
		int oldProgress = this.progress;
		this.progress = aprogress;
		this.pcs.firePropertyChange("progress", new Integer(oldProgress), new Integer(aprogress));
	}

	protected final int getProgress()
	{
		return progress;
	}

	public void addPropertyChangeListener( PropertyChangeListener listener )
	{
		this.pcs.addPropertyChangeListener( listener );
	}

	public void removePropertyChangeListener( PropertyChangeListener listener )
	{
		this.pcs.removePropertyChangeListener( listener );
	}
}