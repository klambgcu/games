package com.kelsoft.stats;

import java.awt.Toolkit;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;

public class StatsTopCategories extends StatusUpdate
{
	private Configuration       config;
	private Game                summary;
	private Games               games;
	private PrintWriter         report;
	private StatsRosterInfo     roster;
	private StatsTopPerformer[] players;
	private StatsTopPerformers  top;

	public StatsTopCategories()
	{
	}

	public StatsTopCategories(Games games, StatsRosterInfo roster, Configuration config)
	{
		this.games   = games;
		this.roster  = roster;
		this.config  = config;
		this.summary = games.getGame(KELStatsConstants.TAB_SUMMARY);
	}

	public void createReport() throws Exception
	{
		try
		{
			doInit();
			doBatting();
			doPitching();
			doCatching();
			doFielding();
			doConclude();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally
		{
			try
			{
				if (report != null)
					report.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw e;
			}
		}
	}

	private void loadPlayers(int startX, int startY)
	{
		int count = players.length;

		for(int i = 0; i < count; i++)
		{
			players[i] = new StatsTopPerformer();
			players[i].setNumber(summary.getData(startY + i, startX));
			players[i].setName(summary.getData(startY + i, startX+1));
		}
	}

	private void loadCategory(int startX, int startY, int type)
	{
		int count = players.length;
		if (type == 0) // Double
		{
			for(int i = 0; i < count; i++)
			{
				players[i].setValue(Double.parseDouble(summary.getData(startY + i, startX)));
			}
		}
		else if (type == 1) // Integer
		{
			for(int i = 0; i < count; i++)
			{
				players[i].setValue(Integer.parseInt(summary.getData(startY + i, startX)));
			}
		}
		else if (type == 2) // Integer - special case for Batting Walks
		{
			for(int i = 0; i < count; i++)
			{
				players[i].setValue(Integer.parseInt(summary.getData(startY + i, startX)) + Integer.parseInt(summary.getData(startY + i, startX+1)) );
			}
		}
		else if (type == 3) // Integer - special case for Batting Caught Stealing
		{
			for(int i = 0; i < count; i++)
			{
				players[i].setValue(Integer.parseInt(summary.getData(startY + i, startX)) - Integer.parseInt(summary.getData(startY + i, startX-1)) );
			}
		}
		else if (type == 4) // Integer - special case for Fielding Chances
		{
			for(int i = 0; i < count; i++)
			{
				players[i].setValue(Integer.parseInt(summary.getData(startY + i, startX))   +
									Integer.parseInt(summary.getData(startY + i, startX+1)) +
									Integer.parseInt(summary.getData(startY + i, startX+2)));
			}
		}
		else if (type == 5) // Double - special case for Pitching Win - Loss Percentage
		{
			for(int i = 0; i < count; i++)
			{
				int w = Integer.parseInt(summary.getData(startY + i, startX));
				int l = Integer.parseInt(summary.getData(startY + i, startX+1));
				int t = w + l;
				double p;
				if (t > 0)
					p = (double)w / (double)t;
				else
					p = 0.000;
				players[i].setValue(p);
			}
		}
	}

	private void doInit() throws Exception
	{
		try
		{
			updateStatus("\nBeginning Section: Top Performers By Category\n");
			setProgress(90);
			String outFile = KELStatsConstants.DIRECTORY_NAME + File.separator + KELStatsConstants.TOP_CATEGORIES;
			report = new PrintWriter (new FileWriter(outFile));
			top = new StatsTopPerformers(report);
			report.println("<html>\n<head>\n<title>Top Performers By Category</title>\n</head>\n<body>");
			report.println(KELStatsConstants.HTML_COMMENT_LINE);
			report.printf("<pre><center>Team: %s\nDate: %s</center></pre>\n", config.getProperty(KELStatsConstants.TEAM_NAME_PROPERTY, KELStatsConstants.TEAM_NAME_DEFAULTS), new Date());
			report.println("<center><h1>Top Performers By Category</h1></center>");
			report.println("<center>\n<table border=0>\n<tr><td>Batting Categories</td><td>Pitching Categories</td></tr>\n<tr>");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doBatting()
	{
		updateStatus("    Formatting Batting Categories...");
		players = new StatsTopPerformer[roster.getCountPlayers()];
		loadPlayers(0, 2);

		report.println("<td>\n<textarea cols=\"60\" rows=\"15\" readonly=\"readonly\">");

		loadCategory( 2,  2, 1); top.setCategory("Games Played");             top.setPlayers(players); top.createReport();
		loadCategory( 3,  2, 1); top.setCategory("Games Started");            top.setPlayers(players); top.createReport();
		loadCategory( 4,  2, 1); top.setCategory("Total Plate Appearances");  top.setPlayers(players); top.createReport();
		loadCategory( 2, 22, 1); top.setCategory("At Bats");                  top.setPlayers(players); top.createReport();
		loadCategory( 7, 22, 0); top.setCategory("Batting Average");          top.setPlayers(players); top.createReport();
		loadCategory( 8, 22, 0); top.setCategory("On Base Percentage");       top.setPlayers(players); top.createReport();
		loadCategory( 9, 22, 0); top.setCategory("Slugging Percentage");      top.setPlayers(players); top.createReport();
		loadCategory( 3, 22, 1); top.setCategory("Hits");                     top.setPlayers(players); top.createReport();
		loadCategory( 5,  2, 1); top.setCategory("Singles");                  top.setPlayers(players); top.createReport();
		loadCategory( 6,  2, 1); top.setCategory("Doubles");                  top.setPlayers(players); top.createReport();
		loadCategory( 7,  2, 1); top.setCategory("Triples");                  top.setPlayers(players); top.createReport();
		loadCategory( 8,  2, 1); top.setCategory("Home Runs");                top.setPlayers(players); top.createReport();
		loadCategory(19,  2, 1); top.setCategory("Runs Batted In");           top.setPlayers(players); top.createReport();
		loadCategory(20,  2, 1); top.setCategory("Runs Scored");              top.setPlayers(players); top.createReport();
		loadCategory( 4, 22, 1); top.setCategory("Total Bases");              top.setPlayers(players); top.createReport();
		loadCategory(13,  2, 1); top.setCategory("Sac Bunts");                top.setPlayers(players); top.createReport();
		loadCategory(12,  2, 1); top.setCategory("Sac Flies");                top.setPlayers(players); top.createReport();
		loadCategory(21,  2, 1); top.setCategory("Stolen Bases");             top.setPlayers(players); top.createReport();
		loadCategory(22,  2, 1); top.setCategory("Steal Attempts");           top.setPlayers(players); top.createReport();
		loadCategory(22,  2, 3); top.setCategory("Caught Stealing");          top.setPlayers(players); top.createReport();
		loadCategory(14,  2, 2); top.setCategory("Walks");                    top.setPlayers(players); top.createReport();
		loadCategory(18,  2, 1); top.setCategory("Strikeouts");               top.setPlayers(players); top.createReport();
		loadCategory(23,  2, 1); top.setCategory("Grounded into DP");         top.setPlayers(players); top.createReport();
		loadCategory(11,  2, 1); top.setCategory("Hit By Pitch");             top.setPlayers(players); top.createReport();

		report.println("\n</textarea>\n</td>");
		updateStatus("Completed.\n");
		setProgress( getProgress() + 2);
	}

	private void doPitching()
	{
		updateStatus("    Formatting Pitching Categories...");
		players = new StatsTopPerformer[roster.getCountPitchers()];
		loadPlayers(0, 42);

		report.println("<td>\n<textarea cols=\"60\" rows=\"15\" readonly=\"readonly\">");

		loadCategory( 5, 54, 0); top.setCategory("Earned Run Average");       top.setPlayers(players, "Asc"); top.createReport();
		loadCategory( 6, 54, 0); top.setCategory("Opposing Batting Average"); top.setPlayers(players, "Asc"); top.createReport();
		loadCategory( 3, 42, 5); top.setCategory("Win - Loss Percentage");    top.setPlayers(players); top.createReport();
		loadCategory( 3, 42, 1); top.setCategory("Wins");                     top.setPlayers(players); top.createReport();
		loadCategory( 4, 42, 1); top.setCategory("Losses");                   top.setPlayers(players); top.createReport();
		loadCategory( 5, 42, 1); top.setCategory("Saves");                    top.setPlayers(players); top.createReport();
		loadCategory( 8, 42, 1); top.setCategory("Shutouts");                 top.setPlayers(players); top.createReport();
		loadCategory( 9, 42, 1); top.setCategory("Combined Shutouts");        top.setPlayers(players); top.createReport();
		loadCategory(11, 42, 0); top.setCategory("Innings Pitched");          top.setPlayers(players); top.createReport();
		loadCategory(10, 42, 1); top.setCategory("Batters Faced");            top.setPlayers(players); top.createReport();
		loadCategory(22, 42, 1); top.setCategory("Batters Struck Out");       top.setPlayers(players); top.createReport();
		loadCategory( 2, 42, 1); top.setCategory("Appearances");              top.setPlayers(players); top.createReport();
		loadCategory( 6, 42, 1); top.setCategory("Games Started");            top.setPlayers(players); top.createReport();
		loadCategory( 7, 42, 1); top.setCategory("Complete Games");           top.setPlayers(players); top.createReport();
		loadCategory(23, 42, 1); top.setCategory("Wild Pitches");             top.setPlayers(players); top.createReport();
		loadCategory(21, 42, 1); top.setCategory("Hit Batters");              top.setPlayers(players); top.createReport();
		loadCategory(19, 42, 1); top.setCategory("Sac Bunts Allowed");        top.setPlayers(players); top.createReport();
		loadCategory(18, 42, 1); top.setCategory("Sac Flies Allowed");        top.setPlayers(players); top.createReport();
		loadCategory(14, 42, 1); top.setCategory("Hits Allowed");             top.setPlayers(players, "Asc"); top.createReport();
		loadCategory(24, 42, 1); top.setCategory("Runs Allowed");             top.setPlayers(players, "Asc"); top.createReport();
		loadCategory(25, 42, 1); top.setCategory("Earned Runs Allowed");      top.setPlayers(players, "Asc"); top.createReport();
		loadCategory(20, 42, 1); top.setCategory("Walks Allowed");            top.setPlayers(players, "Asc"); top.createReport();
		loadCategory(15, 42, 1); top.setCategory("Doubles Allowed");          top.setPlayers(players, "Asc"); top.createReport();
		loadCategory(16, 42, 1); top.setCategory("Triples Allowed");          top.setPlayers(players, "Asc"); top.createReport();
		loadCategory(17, 42, 1); top.setCategory("Home Runs Allowed");        top.setPlayers(players, "Asc"); top.createReport();

		report.println("\n</textarea>\n</td>\n</tr>");
		updateStatus("Completed.\n");
		setProgress( getProgress() + 2);
	}

	private void doCatching()
	{
		updateStatus("    Formatting Catching Categories...");
		players = new StatsTopPerformer[roster.getCountCatchers()];
		loadPlayers(11, 54);

		report.println("<tr><td>Catching Categories</td><td>Fielding Categories</td></tr>\n<tr>");

		report.println("<td>\n<textarea cols=\"60\" rows=\"15\" readonly=\"readonly\">");

		loadCategory(15, 54, 1); top.setCategory("Batters Caught Against");   top.setPlayers(players); top.createReport();
		loadCategory(16, 54, 0); top.setCategory("Innings Caught");           top.setPlayers(players); top.createReport();
		loadCategory(23, 54, 1); top.setCategory("Runners Caught Stealing");  top.setPlayers(players); top.createReport();
		loadCategory(25, 54, 1); top.setCategory("Stolen Bases Allowed");     top.setPlayers(players); top.createReport();
		loadCategory(20, 54, 1); top.setCategory("Passed Balls");             top.setPlayers(players); top.createReport();
		loadCategory(21, 54, 1); top.setCategory("Dropped 3rd Strike");       top.setPlayers(players); top.createReport();
		loadCategory(22, 54, 1); top.setCategory("Recovered Dropped 3rd");    top.setPlayers(players); top.createReport();

		report.println("\n</textarea>\n</td>");
		updateStatus("Completed.\n");
		setProgress( getProgress() + 2);
	}

	private void doFielding()
	{
		updateStatus("    Formatting Fielding Categories...");
		players = new StatsTopPerformer[roster.getCountPlayers()];
		loadPlayers(0, 2);

		report.println("<td>\n<textarea cols=\"60\" rows=\"15\" readonly=\"readonly\">");

		loadCategory(21, 22, 4); top.setCategory("Chances");                  top.setPlayers(players); top.createReport();
		loadCategory(21, 22, 1); top.setCategory("Putouts");                  top.setPlayers(players); top.createReport();
		loadCategory(22, 22, 1); top.setCategory("Assists");                  top.setPlayers(players); top.createReport();
		loadCategory(23, 22, 1); top.setCategory("Errors");                   top.setPlayers(players); top.createReport();
		loadCategory(24, 22, 0); top.setCategory("Fielding Percentage");      top.setPlayers(players); top.createReport();

		report.println("\n</textarea>\n</td>\n</tr>");
		updateStatus("Completed.\n");
		setProgress( getProgress() + 2);
	}

	private void doConclude() throws Exception
	{
		try
		{
			report.println("</table>\n</center>\n</body>\n</html>");
			updateStatus("Completed Section: Top Performers By Category\nDone!\n");
			setProgress(100);
			Toolkit.getDefaultToolkit().beep();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

}