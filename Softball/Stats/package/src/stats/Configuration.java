package com.kelsoft.stats;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

public class Configuration extends Properties
{
	public void saveProperties()
	{
		FileOutputStream output = null;

		try
		{
			output = new FileOutputStream(KELStatsConstants.CONFIG_FILE);
			store(output, "SoftballStats Configuration");
			output.close();
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (output != null)
					output.close();
			}
			catch (Exception ignore)
			{
			}
		}
	}

	public void loadProperties()
	{
		FileInputStream input = null;

		try
		{
			input = new FileInputStream(KELStatsConstants.CONFIG_FILE);
			load(input);
			input.close();
		}
		catch (FileNotFoundException ex)
		{
			loadDefaultSettings();
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (input != null)
					input.close();
			}
			catch (Exception ignore)
			{
			}
		}
	}

	public void copy(Configuration config)
	{
		clear();
		Enumeration e = config.propertyNames();
		while (e.hasMoreElements())
		{
			String key = (String)e.nextElement();
			setProperty(key, config.getProperty(key));
		}
	}

	private void loadDefaultSettings()
	{
		setProperty(KELStatsConstants.CHART_BACKGROUND_COLOR_PROPERTY,    KELStatsConstants.CHART_BACKGROUND_COLOR_DEFAULTS);
		setProperty(KELStatsConstants.CHART_BARTOP_COLOR_PROPERTY,        KELStatsConstants.CHART_BARTOP_COLOR_DEFAULTS);
		setProperty(KELStatsConstants.DETAIL_COLOR_FONT_PROPERTY,         KELStatsConstants.DETAIL_COLOR_FONT_DEFAULTS);
		setProperty(KELStatsConstants.DETAIL_COLOR_PROPERTY,              KELStatsConstants.DETAIL_COLOR_DEFAULTS);
		setProperty(KELStatsConstants.DETAIL_GRAYED_FONT_PROPERTY,        KELStatsConstants.DETAIL_GRAYED_FONT_DEFAULTS);
		setProperty(KELStatsConstants.DETAIL_GRAYED_PROPERTY,             KELStatsConstants.DETAIL_GRAYED_DEFAULTS);
		setProperty(KELStatsConstants.HEADING_COLOR_FONT_PROPERTY,        KELStatsConstants.HEADING_COLOR_FONT_DEFAULTS);
		setProperty(KELStatsConstants.HEADING_COLOR_PROPERTY,             KELStatsConstants.HEADING_COLOR_DEFAULTS);
		setProperty(KELStatsConstants.PLAYER_COLOR_FONT_HOVER_PROPERTY,   KELStatsConstants.PLAYER_COLOR_FONT_HOVER_DEFAULTS);
		setProperty(KELStatsConstants.PLAYER_COLOR_FONT_NORMAL_PROPERTY,  KELStatsConstants.PLAYER_COLOR_FONT_NORMAL_DEFAULTS);
		setProperty(KELStatsConstants.PLAYER_COLOR_FONT_VISITED_PROPERTY, KELStatsConstants.PLAYER_COLOR_FONT_VISITED_DEFAULTS);
		setProperty(KELStatsConstants.PLAYER_COLOR_PROPERTY,              KELStatsConstants.PLAYER_COLOR_DEFAULTS);
		setProperty(KELStatsConstants.TEAM_NAME_PROPERTY,                 KELStatsConstants.TEAM_NAME_DEFAULTS);
		setProperty(KELStatsConstants.TOTALS_COLOR_FONT_PROPERTY,         KELStatsConstants.TOTALS_COLOR_FONT_DEFAULTS);
		setProperty(KELStatsConstants.TOTALS_COLOR_PROPERTY,              KELStatsConstants.TOTALS_COLOR_DEFAULTS);
	}
} // end Configuration
