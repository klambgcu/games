package com.kelsoft.stats;

import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public abstract class AbstractTeamStats implements Stats
{
	protected Game         game;
	protected int          countPlayers;
	protected int          offsetNameNum;
	protected int          startPosY;
	protected int          summaryTotalOffset;
	protected int[]        subtitlesSize;
	protected String       title;
	protected String[]     subtitles;
	protected StringBuffer report = new StringBuffer(32 * 1024);

	public AbstractTeamStats(Game game, int startPosY, int countPlayers, String title, String[] subtitles, int[] subtitlesSize, int summaryTotalOffset, int numNameOffset)
	{
		this.game               = game;
		this.startPosY          = startPosY;
		this.countPlayers       = countPlayers;
		this.title              = title;
		this.subtitles          = subtitles;
		this.subtitlesSize      = subtitlesSize;
		this.summaryTotalOffset = summaryTotalOffset;
		this.offsetNameNum      = numNameOffset;
	}

	private void doInitHTML() throws Exception
	{
		try
		{
			report("<table cellpadding='0' cellspacing='0' style='margin:15px 0 0 0;'>");
			report("<tr>");
			report("<td valign='top'>");
			report("<table width='200' class='kelsoftballstats' border='1'>");
			report("<tr><th class='subhead' nowrap='nowrap'>" + title.trim() + "</th></tr>");
			report("<tr><th class='subhead' nowrap='nowrap'>&nbsp;</th></tr>");

			for (int y = this.startPosY; y < (this.startPosY + this.countPlayers); y++)
				report("<tr><th nowrap='nowrap' width='200' class='subhead'><a " +
								KELStatsConstants.STATS_HINT + " href='" +
								KELStatsConstants.STATS_LOCATION +
								getFormattedText(KELStatsConstants.PLAYER_NUM_POSX  + offsetNameNum, y)  + ".html'>" +
								getFormattedText(KELStatsConstants.PLAYER_NAME_POSX + offsetNameNum, y) +
								"</a></th></tr>");
			report("<tr><th class='subhead' nowrap='nowrap'><a " +
					KELStatsConstants.STATS_HINT + " href='" +
					KELStatsConstants.STATS_LOCATION + "Totals.html'>Totals</a></th></tr>");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doSummaryHeadingHTML1() throws Exception
	{
		try
		{
			report("</table>");
			report("</td>");
			report("<td valign='top'>");
			report("<div style='overflow-x:auto; overflow-y:hidden; width: 700px;' class='iepadding111'>");
			report("<table class='kelsoftballstats' border='1' width='98%'>");
			report("<thead>");
			report("<tr>");
			for (int loop = 0; loop < subtitles.length; loop++)
				report("<th colspan=" + subtitlesSize[loop] + " nowrap='nowrap'>" + subtitles[loop].trim() + "</th>");
			report("<th nowrap='nowrap' width=700px>&nbsp;</th>");
			report("</tr>");
			report("<tr>");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doSummaryHeadingHTML2() throws Exception
	{
		try
		{
			report("<th nowrap='nowrap' width=700px>&nbsp;</th>");
			report("</tr>");
			report("</thead>");
			report("<tbody>");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doEndHTML() throws Exception
	{
		try
		{
			report("</tbody>");
			report("</table>\n</div>\n</td>\n</tr>\n</table>\n<p>");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	protected void report(String msg) throws Exception
	{
		try
		{
			report.append(msg + "\n");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	public void setOffsetNameNum(int offset)
	{
		this.offsetNameNum = offset;
	}

	public StringBuffer doReport() throws Exception
	{
		try
		{
			doInitHTML();
			doSummaryHeadingHTML1();
			doSummaryHeadingHTML();
			doSummaryHeadingHTML2();
			for (int x=0; x < this.countPlayers; x++)
			{
				doSummaryValuesHTML(x, false);
			}
			doSummaryValuesHTML(this.summaryTotalOffset, true);
			doEndHTML();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		return report;
	}

	protected String getFormattedText(int col, int row) throws Exception
	{
		String result = null;

		try
		{
			result = game.getData(row, col);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	protected abstract void doSummaryHeadingHTML() throws Exception;
	protected abstract void doSummaryValuesHTML(int player, Boolean tots) throws Exception;
}