package com.kelsoft.stats;

import jxl.*;

public class Game
{
	private int        rowCount = 0;
	private String[][] data;

	public Game()
	{
	}

	public Game(Sheet s)
	{
		// Allocate array of rows
		rowCount = s.getRows();
		data = new String[rowCount][];

		Cell[] row = null;
		for (int i = 0; i < rowCount; i++)
		{
			row = s.getRow(i);
			if (row.length > 0)
			{
				// Allocate array of columns based upon this row's length
				data[i] = new String[row.length];

				// Store cell data
				for (int r = 0; r < row.length; r++)
				{
					data[i][r] = row[r].getContents().trim();
				}
			}
			else
			{
				// Allocate a single item in case of empty rows
				data[i] = new String[1];
				data[i][0] = "";
			}
		}
	}

	public String getData(int row, int column)
	{
		return data[row][column];
	}

	public int getRowCount()
	{
		return data.length;
	}

	public int getColumnCount(int row)
	{
		return data[row].length;
	}
}