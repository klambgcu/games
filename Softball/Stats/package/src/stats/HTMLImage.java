package com.kelsoft.stats;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.colorchooser.*;

public class HTMLImage
{
	private StatsRosterInfo rosterInfo;
	private Configuration   config;

	public HTMLImage()
	{
	}

	public HTMLImage(StatsRosterInfo rosterInfo, Configuration config)
	{
		this.rosterInfo = rosterInfo;
		this.config     = config;
	}

	public RenderedImage getHtmlImage()
	{
		int w = 604;
		int h = 204;
		int offsetX = 1;
		int offsetY = 1;

		BufferedImage htmlImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = htmlImage.createGraphics();
		g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
		g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC));
		Font f = g2d.getFont();
		g2d.setFont(f.deriveFont(Font.BOLD));

		// Fill the background
		g2d.setPaint(Color.WHITE);
		g2d.fill(new Rectangle(0, 0, w, h));

		// Draw Player background
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.PLAYER_COLOR_PROPERTY)));
		g2d.fill(new Rectangle(offsetX,offsetY,100,200));

		// Draw Heading background
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.HEADING_COLOR_PROPERTY)));
		g2d.fill(new Rectangle(offsetX+100,offsetY,500,50));

		// Draw Total background
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.TOTALS_COLOR_PROPERTY)));
		g2d.fill(new Rectangle(offsetX+100,offsetY+175,300,25));

		// Draw Grayed Area background
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.DETAIL_GRAYED_PROPERTY)));
		g2d.fill(new Rectangle(offsetX+400,offsetY+50,200,150));

		// Draw Detail background
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.DETAIL_COLOR_PROPERTY)));
		g2d.fill(new Rectangle(offsetX+100,offsetY+50,300,125));

		// Draw Player text
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.PLAYER_COLOR_FONT_NORMAL_PROPERTY)));
		int posY = offsetY+18;
		g2d.drawString("Players",        offsetX+30,posY); posY += 25;
		g2d.drawString(" ",              offsetX+10,posY); posY += 25;
		g2d.drawString("Player"       ,  offsetX+10,posY); posY += 25;
		g2d.drawString("Player Normal",  offsetX+10,posY); posY += 25;
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.PLAYER_COLOR_FONT_VISITED_PROPERTY)));
		g2d.drawString("Player Visited", offsetX+10,posY); posY += 25;
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.PLAYER_COLOR_FONT_HOVER_PROPERTY)));
		g2d.drawString("Player Hover",   offsetX+10,posY); posY += 25;
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.PLAYER_COLOR_FONT_NORMAL_PROPERTY)));
		g2d.drawString("Player",         offsetX+10,posY); posY += 25;
		g2d.drawString("Totals",         offsetX+30,posY);

		// Draw Heading text (Font Color)
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.HEADING_COLOR_FONT_PROPERTY)));
		g2d.drawString("Heading Category", offsetX+200,offsetY+18);
		for (int i = 0; i < 5; i++)
			g2d.drawString("Head"+(i+1),   offsetX+110+i*60,  offsetY+43);

		// Draw Detail text (Font Color)
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.DETAIL_COLOR_FONT_PROPERTY)));
		for (int i = 0; i < 5; i++)
		{
			g2d.drawString("Detail"+(i+1), offsetX+110+i*60, offsetY+68);
			g2d.drawString("Detail"+(i+1), offsetX+110+i*60, offsetY+93);
			g2d.drawString("Detail"+(i+1), offsetX+110+i*60, offsetY+118);
			g2d.drawString("Detail"+(i+1), offsetX+110+i*60, offsetY+143);
			g2d.drawString("Detail"+(i+1), offsetX+110+i*60, offsetY+168);
		}

		// Draw Total text (Font Color)
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.TOTALS_COLOR_FONT_PROPERTY)));
		for (int i = 0; i < 5; i++)
			g2d.drawString("Total"+(i+1),  offsetX+110+i*60, offsetY+193);

		// Draw Grayed Area text (Font Color)
		g2d.setPaint(Color.decode(config.getProperty(KELStatsConstants.DETAIL_GRAYED_FONT_PROPERTY)));
		for (int i = 0; i < 6; i++)
			g2d.drawString("Grayed Area", offsetX+450, offsetY+68+i*25);

		// Draw Grid Lines
		// Draw horizontal lines
		g2d.setPaint(Color.BLACK);
		for (int i =0; i < 9; i++)
			g2d.drawLine(offsetX, offsetY+i*25, offsetX+600, offsetY+i*25);

		// Draw vertical end lines
		g2d.drawLine(offsetX,     offsetY, offsetX,     offsetY+200);
		g2d.drawLine(offsetX+100, offsetY, offsetX+100, offsetY+200);
		g2d.drawLine(offsetX+400, offsetY, offsetX+400, offsetY+200);
		g2d.drawLine(offsetX+600, offsetY, offsetX+600, offsetY+200);

		// Draw vertical detail lines
		for (int i = 0; i < 5; i++)
			g2d.drawLine(offsetX+160+i*60, offsetY+25, offsetX+160+i*60, offsetY+200);

		return htmlImage;
	}
}