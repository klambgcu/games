package com.kelsoft.stats;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Date;

public class TeamStats extends StatusUpdate
{
	private Configuration   config;
	private Game            game;
	private Games           games;
	private PrintWriter     report;
	private Stats           batfielder;
	private Stats           catcher;
	private Stats           pitcher;
	private StatsRosterInfo rosterInfo;

	public TeamStats()
	{
	}

	public TeamStats(Games games, StatsRosterInfo info, PrintWriter report, Configuration config) throws Exception
	{
		this.games      = games;
		this.rosterInfo = info;
		this.report     = report;
		this.config     = config;
	}

	public void createReport() throws Exception
	{
		try
		{
			updateStatus("Beginning Section: Team Stats Summary:\n");
			setProgress(40);
			game = games.getGame(KELStatsConstants.TAB_SUMMARY);

			batfielder = StatsFactory.create(KELStatsConstants.TYPE_BATFIELD, game, rosterInfo);
			pitcher    = StatsFactory.create(KELStatsConstants.TYPE_PITCHING, game, rosterInfo);
			catcher    = StatsFactory.create(KELStatsConstants.TYPE_CATCHING, game, rosterInfo);

			report.println("<style>");
			report.println(".iepadding111{padding-bottom:17px;}");
			report.println(".kelsoftballstats            {clear:both;border: 1px solid #000;border-collapse:collapse;}");
			report.println(".kelsoftballstats td         {height: 20px;text-align:center;font:normal 11px Verdana, Arial, Helvetica, sans-serif;border-collapse:collapse;border: 1px solid #000;padding: 0 3px; " +
							"color: "      + config.getProperty(KELStatsConstants.DETAIL_COLOR_FONT_PROPERTY, KELStatsConstants.DETAIL_COLOR_FONT_DEFAULTS) + "; " +
							"background: " + config.getProperty(KELStatsConstants.DETAIL_COLOR_PROPERTY, KELStatsConstants.DETAIL_COLOR_DEFAULTS) + ";}");

			report.println(".kelsoftballstats td.gray    {height: 20px;text-align:center;font:normal 11px Verdana, Arial, Helvetica, sans-serif;border-collapse:collapse;border: 1px solid #000;padding: 0 3px; " +
							"color: "      + config.getProperty(KELStatsConstants.DETAIL_GRAYED_FONT_PROPERTY, KELStatsConstants.DETAIL_GRAYED_FONT_DEFAULTS) + "; " +
							"background: " + config.getProperty(KELStatsConstants.DETAIL_GRAYED_PROPERTY, KELStatsConstants.DETAIL_GRAYED_DEFAULTS) + ";}");

			report.println(".kelsoftballstats td.tot     {height: 20px;text-align:center;font:normal 11px Verdana, Arial, Helvetica, sans-serif;border-collapse:collapse;border: 1px solid #000;padding: 0 3px; " +
							"color: "      + config.getProperty(KELStatsConstants.TOTALS_COLOR_FONT_PROPERTY, KELStatsConstants.TOTALS_COLOR_FONT_DEFAULTS) + "; " +
							"background: " + config.getProperty(KELStatsConstants.TOTALS_COLOR_PROPERTY, KELStatsConstants.TOTALS_COLOR_DEFAULTS) + ";}");

			report.println(".kelsoftballstats th         {height: 20px;text-align:center;font:bold   11px Verdana, Arial, Helvetica, sans-serif;border-collapse:collapse;border: 1px solid #000;padding: 0 3px;  " +
							"color: "      + config.getProperty(KELStatsConstants.HEADING_COLOR_FONT_PROPERTY, KELStatsConstants.HEADING_COLOR_FONT_DEFAULTS) + "; " +
							"background: " + config.getProperty(KELStatsConstants.HEADING_COLOR_PROPERTY, KELStatsConstants.HEADING_COLOR_DEFAULTS) + ";}");

			report.println(".kelsoftballstats th.subhead {height: 20px;text-align:right;font:bold 11px Verdana, Arial, Helvetica, sans-serif;text-transform:capitalize; " +
							"color: "      + config.getProperty(KELStatsConstants.PLAYER_COLOR_FONT_NORMAL_PROPERTY, KELStatsConstants.PLAYER_COLOR_FONT_NORMAL_DEFAULTS) + "; " +
							"background: " + config.getProperty(KELStatsConstants.PLAYER_COLOR_PROPERTY, KELStatsConstants.PLAYER_COLOR_DEFAULTS) + ";}");

			report.println(".kelsoftballstats th.subhead a			{height: 20px;text-decoration: none; color: " + config.getProperty(KELStatsConstants.PLAYER_COLOR_FONT_NORMAL_PROPERTY,  KELStatsConstants.PLAYER_COLOR_FONT_NORMAL_DEFAULTS)  + ";}");
			report.println(".kelsoftballstats th.subhead a:visited	{height: 20px;text-decoration: none; color: " + config.getProperty(KELStatsConstants.PLAYER_COLOR_FONT_VISITED_PROPERTY, KELStatsConstants.PLAYER_COLOR_FONT_VISITED_DEFAULTS) + ";}");
			report.println(".kelsoftballstats th.subhead a:hover	{height: 20px;text-decoration: none; color: " + config.getProperty(KELStatsConstants.PLAYER_COLOR_FONT_HOVER_PROPERTY,   KELStatsConstants.PLAYER_COLOR_FONT_HOVER_DEFAULTS)   + ";}");

			report.println(".kelsoftballcharts th				{height: 20px;text-align:center;font:normal 11px Verdana, Arial, Helvetica, sans-serif;}");
			report.println(".kelsoftballcharts td a img			{text-decoration: none; color: #000000; border:1px solid #FFFFFF;}");
			report.println(".kelsoftballcharts td a:visited img	{text-decoration: none; color: #000000; border:1px solid #FFFFFF;}");
			report.println(".kelsoftballcharts td a:hover img	{text-decoration: none; color: #FFFFFF; border:1px solid #000000;}");
			report.println("</style>");
			report.println(KELStatsConstants.HTML_COMMENT_LINE);

			updateStatus("  Formatting: ");
			updateStatus("Batting, Fielding");
			setProgress(getProgress() + 4);
			report.println(batfielder.doReport());

			updateStatus(", Pitching");
			setProgress(getProgress() + 7);
			report.println(pitcher.doReport());

			updateStatus(", Catching");
			setProgress(getProgress() + 10);
			report.println(catcher.doReport());

			report.println("<p>\n<table class=\"kelsoftballcharts\" border=\"0\" width=\"860\">");
			report.println("<tbody>");
			report.println("<tr>");
			report.println("  <th colspan=3><a href=\"" +
							KELStatsConstants.DIRECTORY_NAME + "\\" + KELStatsConstants.TOP_CATEGORIES + "\">Top Performers By Category</a></th>");
			report.println("</tr>");
			report.println("<tr>");
			report.println("  <th colspan=3>&nbsp;</th>");
			report.println("</tr>");
			report.println("<tr>");
			report.println("  <th>&nbsp;</th>");
			report.println("  <th>On Base Percentage Chart</th>");
			report.println("  <th>&nbsp;</th>");
			report.println("</tr>");
			report.println("<tr>");
			report.println("  <td width=\"217px\">&nbsp;</td>");
			report.println("  <td><a href=\"" +
							KELStatsConstants.CHART_DIRECTORY_NAME + "\\" + KELStatsConstants.CHART_BAR_ONBASE_FILE + "\"><img src=\"" +
							KELStatsConstants.CHART_DIRECTORY_NAME + "\\" + KELStatsConstants.CHART_BAR_ONBASE_FILE + "\" width=\"426px\" height=\"213px\" /></a></td>");
			report.println("  <td width=\"217px\">&nbsp;</td>");
			report.println("</tr>");
			report.println("</tbody>");
			report.println("</table>");

			report.println("<table class=\"kelsoftballcharts\" border=\"0\" width=\"860\">");
			report.println("<tbody>");
			report.println("<tr>");
			report.println("  <th>Batting Average Chart</th>");
			report.println("  <th>Slugging Percentage Chart</th>");
			report.println("</tr>");
			report.println("<tr>");
			report.println("  <td><a href=\"" +
							KELStatsConstants.CHART_DIRECTORY_NAME + "\\" + KELStatsConstants.CHART_BAR_BATAVG_FILE + "\"><img src=\"" +
							KELStatsConstants.CHART_DIRECTORY_NAME + "\\" + KELStatsConstants.CHART_BAR_BATAVG_FILE + "\" width=\"426px\" height=\"213px\" /></a></td>");
			report.println("  <td><a href=\"" +
							KELStatsConstants.CHART_DIRECTORY_NAME + "\\" + KELStatsConstants.CHART_BAR_SLUGPT_FILE + "\"><img src=\"" +
							KELStatsConstants.CHART_DIRECTORY_NAME + "\\" + KELStatsConstants.CHART_BAR_SLUGPT_FILE + "\" width=\"426px\" height=\"213px\" /></a></td>");
			report.println("</tr>");
			report.println("</tbody>");
			report.println("</table>");
			report.println("<p>");
			report.println(String.format("<pre>Last Updated: %1$tb %1$te, %1$tY %1$tH:%1$tM:%1$tS</pre>\n<p>\n", new Date()));

			updateStatus("...Completed\n");
			updateStatus("Completed Section: Team Stats Summary\n\n");
			setProgress(50);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}
}
