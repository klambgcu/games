package com.kelsoft.stats;

import java.awt.Color;
import java.io.File;

public class TeamStatsCatching extends AbstractTeamStats
{
	public TeamStatsCatching(Game game, int startPosY, int countPlayers, String title, String[] subtitles, int[] subtitlesSize, int summaryTotalOffset, int numNameOffset)
	{
		super(game, startPosY, countPlayers, title, subtitles, subtitlesSize, summaryTotalOffset, numNameOffset);
	}

	protected void doSummaryHeadingHTML() throws Exception
	{
		try
		{
			for (int z = 15; z <= KELStatsConstants.CATCHINGX_END; z++)
			{
				String s = getFormattedText(z, (KELStatsConstants.CATCHING_START - 1));
				if ((s == null) || (s.trim().equals("0"))) s = "&nbsp;";
				report("<th nowrap='nowrap'>" + s + "</th>");
			}
		}
		catch (Exception e)
		{
			report(e.getMessage());
			throw e;
		}
	}

	protected void doSummaryValuesHTML(int player, Boolean tots) throws Exception
	{
		try
		{
			report("<tr>");
			for (int z = 15; z <= KELStatsConstants.CATCHINGX_END; z++)
			{
				String s = getFormattedText(z, (player + KELStatsConstants.CATCHING_START));
				if ((s == null) || (s.trim().equals("0")) || (s.trim().equals("0.0")) || (s.trim().equals("0.00")) || (s.trim().equals("0.000")) ) s = "&nbsp;";
				if (tots)
					report("<td class=tot>" + s + "</td>");
				else
					report("<td>" + s + "</td>");
			}
			report("<td class=gray>&nbsp;</td>");
			report("</tr>");
		}
		catch (Exception e)
		{
			report(e.getMessage());
			throw e;
		}
	}
}