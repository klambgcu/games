package com.kelsoft.stats;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Date;

public class PlayerStats extends StatusUpdate
{
	private Configuration   config;
	private Games           games;
	private int             gameCount = 0;
	private PrintWriter     report;
	private StatsRosterInfo rosterInfo;

	public PlayerStats()
	{
	}

	public PlayerStats(Games games, StatsRosterInfo info, PrintWriter report, Configuration config) throws Exception
	{
		this.games      = games;
		this.rosterInfo = info;
		this.report     = report;
		this.config     = config;
		this.gameCount  = getGameCount();
	}

	private int getGameCount() throws Exception
	{
		try
		{
			return Integer.parseInt( getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_END, 2) ); // GP at column 2
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void initDirectory() throws Exception
	{
		try
		{
			File dir = new File(KELStatsConstants.DIRECTORY_NAME);
			if (! dir.exists() || ! dir.isDirectory())
				dir.mkdir();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doHTMLHeading(String name, String number) throws Exception
	{
		try
		{
			String title = name + " #" + number + " Stats";
			report.printf("<html>\n<head>\n<title>%s</title>\n</head>\n<body>\n", title);
			report.println(KELStatsConstants.HTML_COMMENT_LINE);
			report.printf("<pre>\nTeam: %s\nDate: %s\n\nPlayer Statistics\nName:   %s\nNumber: %s\n\n",
							config.getProperty(KELStatsConstants.TEAM_NAME_PROPERTY, KELStatsConstants.TEAM_NAME_DEFAULTS),
							new Date(),
							name,
							number);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doHTMLBattingDetail(int player) throws Exception
	{
		try
		{
			report.printf("%s\n","B A T T I N G");
			report.println("Game     AVG GP-GS    AB   R   H  2B  3B  HR RBI  TB  SLG%    BB HBP  SO GDP   OB%    SF  SH  SB-ATT");
			report.println("----   ----- -----   --- --- --- --- --- --- --- --- -----   --- --- --- --- -----   --- --- -------");

			for (int game = 1; game <= gameCount; game++)
			{
				int bb = Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START + player, 14)) +
						 Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START + player, 15));

				String sbatt = String.format("%3s-%-3s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START + player, 21),
							   							 getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START + player, 22));

				String gp_gs = String.format("%2s-%-2s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START + player, 2),
							   							 getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START + player, 3));

				report.printf("%4d", game);
				report.printf("%8s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START2 + player,  7)); // Batting Average
				report.printf("%6s", gp_gs);																								 // Games Played/Started
				report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START2 + player,  2)); // At-Bats
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START  + player, 20)); // Runs
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START2 + player,  3)); // Hits
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START  + player,  6)); // 2B
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START  + player,  7)); // 3B
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START  + player,  8)); // HR
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START  + player, 19)); // RBIs
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START2 + player,  4)); // TB
				report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START2 + player,  9)); // SLG%
				report.printf("%6d", bb);																									 // BB
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START  + player, 11)); // HBP
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START  + player, 18)); // SO
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START  + player, 23)); // GDP
				report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START2 + player,  8)); // OB%
				report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START  + player, 12)); // SF
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.BATTING_START  + player, 13)); // SH (Bunts)
				report.printf("%8s", sbatt);																								 // SB-ATT
				report.println();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doHTMLBattingSummary(int player) throws Exception
	{
		try
		{
			int bb = Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START + player, 14)) +
					 Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START + player, 15));

			String sbatt = String.format("%3s-%-3s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START + player, 21),
						   							 getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START + player, 22));

			String gp_gs = String.format("%2s-%-2s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START + player, 2),
						   							 getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START + player, 3));

			report.println("----   ----- -----   --- --- --- --- --- --- --- --- -----   --- --- --- --- -----   --- --- -------");
			report.printf("%4s", "Tot.");
			report.printf("%8s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START2 + player,  7)); // Batting Average
			report.printf("%6s", gp_gs);																						  // Games Played/Started
			report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START2 + player,  2)); // At-Bats
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START  + player, 20)); // Runs
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START2 + player,  3)); // Hits
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START  + player,  6)); // 2B
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START  + player,  7)); // 3B
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START  + player,  8)); // HR
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START  + player, 19)); // RBIs
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START2 + player,  4)); // TB
			report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START2 + player,  9)); // SLG%
			report.printf("%6d", bb);																							  // BB
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START  + player, 11)); // HBP
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START  + player, 18)); // SO
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START  + player, 23)); // GDP
			report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START2 + player,  8)); // OB%
			report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START  + player, 12)); // SF
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.BATTING_START  + player, 13)); // SH (Bunts)
			report.printf("%8s", sbatt);																						  // SB-ATT
			report.println("\n");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doHTMLPitchingDetail(int player) throws Exception
	{
		try
		{
			report.printf("\n%s\n","P I T C H I N G");
			report.println("Game     ERA  W-L  APP  GS  CG SHO/CBO  SV       IP   H   R  ER  BB  SO  2B  3B  HR   AB B/AVG    WP HBP SFA SHA     BF   TP    S%");
			report.println("----   ----- ----- --- --- --- ------- ---   ------ --- --- --- --- --- --- --- --- ---- -----   --- --- --- ---   ---- ---- -----");

			for (int game = 1; game <= gameCount; game++)
			{
				String wl = String.format("%2s-%-2s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START + player, 3),
													  getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START + player, 4));

				String sh = String.format("%3s/%-3s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START + player,  8),
													  getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START + player,  9));

				double ip = Double.parseDouble(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START + player, 11));

				int    ab = Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START + player, 10)) - // Batters Faced
							Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START + player, 18)) - // SFA
							Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START + player, 19)) - // SHA
							Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START + player, 20)) - // BB's
							Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START + player, 21));  // HBP

				report.printf("%4d", game);
				report.printf("%8s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START2 + player,  5)); // ERA
				report.printf("%6s", wl);																									  // W-L
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player,  2)); // APP
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player,  6)); // GS
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player,  7)); // CG
				report.printf("%8s", sh);																									  // SHO/CBO
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player,  5)); // SV
				report.printf("%9.2f", ip);																									  // IP
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 14)); // H
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 24)); // R
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 25)); // ER
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 20)); // BB
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 22)); // SO

				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 15)); // 2B
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 16)); // 3B
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 17)); // HR

				report.printf("%5d", ab);																									  // AB
				report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START2 + player,  6)); // B/AVG
				report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 23)); // WP
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 21)); // HBP

				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 18)); // SFA
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 19)); // SHA

				report.printf("%7s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START  + player, 10)); // BF
				report.printf("%5s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START2 + player,  2)); // TP
				report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.PITCHING_START2 + player,  7)); // S%
				report.println();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doHTMLPitchingSummary(int player, boolean teamTotal) throws Exception
	{
		try
		{
			String wl = String.format("%2s-%-2s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START + player, 3),
												  getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START + player, 4));

			int    sho = Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START + player,  8));
			int    cbo = Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START + player,  9));
			String sh  = String.format("%3d/%-3d", sho, cbo);

			// Team Total Summary for SHO/CBO differs from individual/details: SHO = (SHO+CBO)/CBO
			if (teamTotal)
			{
				sh  = String.format("%3d/%-3d", (sho + cbo), cbo);
			}

			double ip = Double.parseDouble(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START + player, 11));

			int    ab = Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START + player, 10)) - // Batters Faced
						Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START + player, 18)) - // SFA
						Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START + player, 19)) - // SHA
						Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START + player, 20)) - // BB's
						Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START + player, 21));  // HBP

			report.println("----   ----- ----- --- --- --- ------- ---   ------ --- --- --- --- --- --- --- --- ---- -----   --- --- --- ---   ---- ---- -----");
			report.printf("%4s", "Tot.");
			report.printf("%8s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START2 + player,  5)); // ERA
			report.printf("%6s", wl);																							   // W-L
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player,  2)); // APP
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player,  6)); // GS
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player,  7)); // CG
			report.printf("%8s", sh);																							   // SHO/CBO
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player,  5)); // SV
			report.printf("%9.2f", ip);																							   // IP
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 14)); // H
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 24)); // R
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 25)); // ER
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 20)); // BB
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 22)); // SO

			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 15)); // 2B
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 16)); // 3B
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 17)); // HR

			report.printf("%5d", ab);																							   // AB
			report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START2 + player,  6)); // B/AVG
			report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 23)); // WP
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 21)); // HBP

			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 18)); // SFA
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 19)); // SHA

			report.printf("%7s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START  + player, 10)); // BF
			report.printf("%5s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START2 + player,  2)); // TP
			report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START2 + player,  7)); // S%
			report.println("\n");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doHTMLCatchingDetail(int player) throws Exception
	{
		try
		{
			report.printf("\n%s\n","C A T C H I N G");
			report.println("Game    BF     IC   TP  DB  WP  PB  D3-D3R  PO-ATT  SB");
			report.println("----   --- ------ ---- --- --- --- ------- ------- ---");

			for (int game = 1; game <= gameCount; game++)
			{
				double ic = Double.parseDouble(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.CATCHING_START + player, 16));

				String d3 = String.format("%3s-%-3s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.CATCHING_START + player,  21),
													  getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.CATCHING_START + player,  22));

				String po = String.format("%3s-%-3s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.CATCHING_START + player, 23),
													  getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.CATCHING_START + player, 24));

				report.printf("%4d", game);
				report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.CATCHING_START + player, 15)); // BF
				report.printf("%7.2f", ic);																									 // IC
				report.printf("%5s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.CATCHING_START + player, 17)); // TP
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.CATCHING_START + player, 18)); // DB
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.CATCHING_START + player, 19)); // WP
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.CATCHING_START + player, 20)); // PB
				report.printf("%8s", d3);																									 // D3-D3R
				report.printf("%8s", po);																									 // PO-ATT
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.CATCHING_START + player, 25)); // SB
				report.println();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doHTMLCatchingSummary(int player) throws Exception
	{
		try
		{
			double ic = Double.parseDouble(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + player, 16));

			String d3 = String.format("%3s-%-3s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + player, 21),
												  getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + player, 22));

			String po = String.format("%3s-%-3s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + player, 23),
												  getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + player, 24));

			report.println("----   --- ------ ---- --- --- --- ------- ------- ---");
			report.printf("%4s", "Tot.");
			report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + player, 15)); // BF
			report.printf("%7.2f", ic);																							  // IC
			report.printf("%5s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + player, 17)); // TP
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + player, 18)); // DB
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + player, 19)); // WP
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + player, 20)); // PB
			report.printf("%8s", d3);																							  // D3-D3R
			report.printf("%8s", po);																							  // PO-ATT
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + player, 25)); // SB
			report.println("\n");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doHTMLFieldingDetail(int player) throws Exception
	{
		try
		{
			report.printf("\n%s\n","F I E L D I N G");
			report.println("Game      C   PO    A   E  FLD%");
			report.println("----   ---- ---- ---- --- -----");

			for (int game = 1; game <= gameCount; game++)
			{
				int    ch = Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.FIELDING_START + player, 21)) + // PO
							Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.FIELDING_START + player, 22)) + // A
							Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.FIELDING_START + player, 23));  // E

				report.printf("%4d", game);
				report.printf("%7d", ch);																									 // Chances
				report.printf("%5s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.FIELDING_START + player, 21)); // PO
				report.printf("%5s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.FIELDING_START + player, 22)); // A
				report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.FIELDING_START + player, 23)); // E
				report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY + game, KELStatsConstants.FIELDING_START + player, 24)); // FLD%
				report.println();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doHTMLFieldingSummary(int player) throws Exception
	{
		try
		{
			int    ch = Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.FIELDING_START + player, 21)) + // PO
						Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.FIELDING_START + player, 22)) + // A
						Integer.parseInt(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.FIELDING_START + player, 23));  // E

			report.println("----   ---- ---- ---- --- -----");
			report.printf("%4s", "Tot.");
			report.printf("%7d", ch);																							  // Chances
			report.printf("%5s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.FIELDING_START + player, 21)); // PO
			report.printf("%5s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.FIELDING_START + player, 22)); // A
			report.printf("%4s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.FIELDING_START + player, 23)); // E
			report.printf("%6s", getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.FIELDING_START + player, 24)); // FLD%
			report.println("\n");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void doHTMLFooting() throws Exception
	{
		try
		{
			report.println("\n</pre>\n</body>\n</html>");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	private int isPitcher(String number) throws Exception
	{
		int result = -1;

		try
		{
			for (int p = 0; p < rosterInfo.getCountPitchers(); p++)
			{
				if (number.equals(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.PITCHING_START + p, 0)))
				{
					result = p;
					break;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	private int isCatcher(String number) throws Exception
	{
		int result = -1;

		try
		{
			for (int p = 0; p < rosterInfo.getCountCatchers(); p++)
			{
				if (number.equals(getFormattedText(KELStatsConstants.TAB_SUMMARY, KELStatsConstants.CATCHING_START + p, 11)))
				{
					result = p;
					break;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}

		return result;
	}

	private void createPlayerFiles() throws Exception
	{
		int player = -1;

		try
		{
			updateStatus("  Formatting: Player Game Files\n");
			setProgress(getProgress() + 1);

			for (int p = 0; p < rosterInfo.getCountPlayers(); p++)
			{
				String number = getFormattedText(KELStatsConstants.TAB_SUMMARY, p + KELStatsConstants.BATTING_START, 0);
				String name   = getFormattedText(KELStatsConstants.TAB_SUMMARY, p + KELStatsConstants.BATTING_START, 1);

				String outFile = KELStatsConstants.DIRECTORY_NAME + File.separator + "player_" + number + ".html";
				report = new PrintWriter (new FileWriter(outFile));

				updateStatus("    Formatting Player: " + name + " - ");
				setProgress(getProgress() + 1);
				doHTMLHeading(name, number);

				updateStatus("Batting");
				setProgress(getProgress() + 1);
				doHTMLBattingDetail(p);
				doHTMLBattingSummary(p);

				if ((player = isPitcher(number)) >= 0)
				{
					updateStatus(", Pitching");
					setProgress(getProgress() + 1);
					doHTMLPitchingDetail(player);
					doHTMLPitchingSummary(player, false);
				}

				if ((player = isCatcher(number)) >= 0)
				{
					updateStatus(", Catching");
					setProgress(getProgress() + 1);
					doHTMLCatchingDetail(player);
					doHTMLCatchingSummary(player);
				}

				updateStatus(", Fielding");
				setProgress(getProgress() + 1);
				doHTMLFieldingDetail(p);
				doHTMLFieldingSummary(p);

				doHTMLFooting();
				report.close();
				updateStatus("...Completed\n");
				setProgress(getProgress() + 1);
			}
			updateStatus("  Completed: Player Game Files\n\n");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally
		{
			try
			{
				if (report != null)
					report.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw e;
			}
		}
	}

	private void createTeamTotalsFile() throws Exception
	{
		try
		{
			updateStatus("  Beginning: Team Game Totals File\n");
			setProgress(getProgress() + 1);
			String outFile = KELStatsConstants.DIRECTORY_NAME + File.separator + "player_Totals.html";
			report = new PrintWriter (new FileWriter(outFile));

			doHTMLHeading("Team Game Totals", "Team Game Totals");

			updateStatus("    Formatting: Batting");
			setProgress(getProgress() + 1);
			doHTMLBattingDetail(KELStatsConstants.SUMMARY_TOTAL_OFFSET_BATFIELD);
			doHTMLBattingSummary(KELStatsConstants.SUMMARY_TOTAL_OFFSET_BATFIELD);

			updateStatus(", Pitching");
			setProgress(getProgress() + 1);
			doHTMLPitchingDetail(KELStatsConstants.SUMMARY_TOTAL_OFFSET_PITCHING);
			doHTMLPitchingSummary(KELStatsConstants.SUMMARY_TOTAL_OFFSET_PITCHING, true);

			updateStatus(", Catching");
			setProgress(getProgress() + 1);
			doHTMLCatchingDetail(KELStatsConstants.SUMMARY_TOTAL_OFFSET_CATCHING);
			doHTMLCatchingSummary(KELStatsConstants.SUMMARY_TOTAL_OFFSET_CATCHING);

			updateStatus(", Fielding");
			setProgress(getProgress() + 1);
			doHTMLFieldingDetail(KELStatsConstants.SUMMARY_TOTAL_OFFSET_BATFIELD);
			doHTMLFieldingSummary(KELStatsConstants.SUMMARY_TOTAL_OFFSET_BATFIELD);
			updateStatus("...Completed\n");

			doHTMLFooting();
			report.close();
			updateStatus("  Completed: Team Game Totals File\n");
			setProgress(90);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally
		{
			try
			{
				if (report != null)
					report.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw e;
			}
		}
	}

	private String getFormattedText(int sheetNumber, int row, int col) throws Exception
	{
		String result = null;

		try
		{
			result = games.getGame(sheetNumber).getData(row, col);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	public void createReport() throws Exception
	{
		try
		{
			updateStatus("Beginning Section: Player Stats\n");
			setProgress(60);
			initDirectory();
			createPlayerFiles();
			createTeamTotalsFile();
			updateStatus("Completed Section: Player Stats\n");
			setProgress(90);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}
}