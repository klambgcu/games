package com.kelsoft.stats;

import java.awt.Color;
import java.io.File;

public class TeamStatsBattingFielding extends AbstractTeamStats
{
	public TeamStatsBattingFielding(Game game, int startPosY, int countPlayers, String title, String[] subtitles, int[] subtitlesSize, int summaryTotalOffset, int numNameOffset)
	{
		super(game, startPosY, countPlayers, title, subtitles, subtitlesSize, summaryTotalOffset, numNameOffset);
	}

	protected void doSummaryHeadingHTML() throws Exception
	{
		try
		{
			for (int z = 2; z <= KELStatsConstants.BATTINGX1_END; z++)
			{
				String s = getFormattedText(z, KELStatsConstants.BATTING_START - 1);
				if ((s == null) || (s.trim().equals("0"))) s = "&nbsp;";
				report("<th nowrap='nowrap'>" + s + "</th>");
			}

			for (int z = 2; z <= KELStatsConstants.BATTINGX2_END - 10; z++)
			{
				String s = getFormattedText(z, KELStatsConstants.BATTING_START2 - 1);
				if ((s == null) || (s.trim().equals("0"))) s = "&nbsp;";
				report("<th nowrap='nowrap'>" + s + "</th>");
			}

			// Offset heading by -1 for Rank fields
			for (int z = KELStatsConstants.BATTINGX2_END - 9; z <= KELStatsConstants.BATTINGX2_END; z++)
			{
				String s = getFormattedText(z, KELStatsConstants.BATTING_START2 - 2);
				if ((s == null) || (s.trim().equals("0"))) s = "&nbsp;";
				report("<th nowrap='nowrap'>" + s + "</th>");
			}

			for (int z = KELStatsConstants.FIELDINGX_START; z <= KELStatsConstants.FIELDINGX_END - 1; z++)
			{
				String s = getFormattedText(z, KELStatsConstants.FIELDING_START - 1);
				if ((s == null) || (s.trim().equals("0"))) s = "&nbsp;";
				report("<th nowrap='nowrap'>" + s + "</th>");
			}
			report("<th nowrap='nowrap'>FLD Rank</th>");
		}
		catch (Exception e)
		{
			report(e.getMessage());
			throw e;
		}
	}

	protected void doSummaryValuesHTML(int player, Boolean tots) throws Exception
	{
		try
		{
			report("<tr>");
			for (int z = 2; z <= KELStatsConstants.BATTINGX1_END; z++)
			{
				String s = getFormattedText(z, player + (KELStatsConstants.BATTING_START));
				if ((s == null) || (s.trim().equals("0")) || (s.trim().equals("0.0")) || (s.trim().equals("0.00")) || (s.trim().equals("0.000")) ) s = "&nbsp;";
				if (tots)
					report("<td class=tot>" + s + "</td>");
				else
					report("<td>" + s + "</td>");
			}

			for (int z = 2; z <= KELStatsConstants.BATTINGX2_END; z++)
			{
				String s = getFormattedText(z, player + (KELStatsConstants.BATTING_START2));
				if ((s == null) || (s.trim().equals("0")) || (s.trim().equals("0.0")) || (s.trim().equals("0.00")) || (s.trim().equals("0.000")) ) s = "&nbsp;";
				if (tots)
					report("<td class=tot>" + s + "</td>");
				else
					report("<td>" + s + "</td>");
			}

			for (int z = KELStatsConstants.FIELDINGX_START; z <= KELStatsConstants.FIELDINGX_END; z++)
			{
				String s = getFormattedText(z, player + (KELStatsConstants.FIELDING_START));
				if ((s == null) || (s.trim().equals("0")) || (s.trim().equals("0.0")) || (s.trim().equals("0.00")) || (s.trim().equals("0.000")) ) s = "&nbsp;";
				if (tots)
					report("<td class=tot>" + s + "</td>");
				else
					report("<td>" + s + "</td>");
			}
			report("<td class=gray>&nbsp;</td>");
			report("</tr>");
		}
		catch (Exception e)
		{
			report(e.getMessage());
			throw e;
		}
	}
}