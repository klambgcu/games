package com.kelsoft.charts;

import com.kelsoft.stats.Configuration;
import com.kelsoft.stats.KELStatsConstants;
import com.kelsoft.stats.StatsRosterInfo;
import com.kelsoft.stats.StatusUpdate;
import java.io.File;

public class ChartsWorker extends StatusUpdate
{
	private Configuration   config;
	private StatsRosterInfo rosterInfo;

	public ChartsWorker()
	{
	}

	public ChartsWorker(StatsRosterInfo info, Configuration config) throws Exception
	{
		this.rosterInfo      = info;
		this.config          = config;
	}

	public void makeCharts() throws Exception
	{
		Chart   chart;

		try
		{
			updateStatus("Beginning Section: Chart Creation\n");
			setProgress(50);

			// Initialize directory, if necessary
			File dir = new File(KELStatsConstants.CHART_DIRECTORY_NAME);
			if (! dir.exists() || ! dir.isDirectory())
			{
				updateStatus("  Creating Charts directory\n");
				dir.mkdir();
			}

			// Perform Chart creation section
			updateStatus("  Creating Charts:\n");
			updateStatus("    Batting Average...");
			chart = ChartsFactory.create(KELStatsConstants.CHART_TYPE_BAR_BATAVG, rosterInfo, config);
			chart.saveChartFile(KELStatsConstants.CHART_BAR_BATAVG_FILE, chart.doChart());
			updateStatus("Completed\n");
			setProgress(53);

			updateStatus("    On Base Percentage...");
			chart = ChartsFactory.create(KELStatsConstants.CHART_TYPE_BAR_ONBASE, rosterInfo, config);
			chart.saveChartFile(KELStatsConstants.CHART_BAR_ONBASE_FILE, chart.doChart());
			updateStatus("Completed\n");
			setProgress(56);

			updateStatus("    Slugging Percentage...");
			chart = ChartsFactory.create(KELStatsConstants.CHART_TYPE_BAR_SLUGPT, rosterInfo, config);
			chart.saveChartFile(KELStatsConstants.CHART_BAR_SLUGPT_FILE, chart.doChart());
			updateStatus("Completed\n");
			setProgress(59);

			updateStatus("Completed Section: Chart Creation\n\n");
			setProgress(60);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}
}