package com.kelsoft.charts;

import java.awt.Color;
import com.kelsoft.stats.StatsRosterInfo;
import com.kelsoft.stats.Configuration;
import com.kelsoft.stats.KELStatsConstants;

public class SampleConfigChart extends AbstractBarChart
{
	public SampleConfigChart()
	{
	}

	public SampleConfigChart(StatsRosterInfo roster, Configuration config)
	{
		super();

		String name = config.getProperty(KELStatsConstants.TEAM_NAME_PROPERTY, KELStatsConstants.TEAM_NAME_DEFAULTS);
		setChartScalingValue(500.000);
		setChartAxisIncrement(0.100);
		setChartAxisMaximum(1.000);
		setChartTitle(name + " - " + KELStatsConstants.CHART_TYPE_BAR_CONFIG);
		setChartSubTitle("(Summary: Games ...)");
		setChartBgColor(    Color.decode( config.getProperty(KELStatsConstants.CHART_BACKGROUND_COLOR_PROPERTY, KELStatsConstants.CHART_BACKGROUND_COLOR_DEFAULTS)));
		setChartBarTopColor(Color.decode( config.getProperty(KELStatsConstants.CHART_BARTOP_COLOR_PROPERTY,     KELStatsConstants.CHART_BARTOP_COLOR_DEFAULTS)));

/*
		ChartInfo[] sampleData = new ChartInfo[KELStatsConstants.MAX_NUM_PLAYERS];
		for (int i = 0; i < KELStatsConstants.MAX_NUM_PLAYERS; i++)
		{
			int player = (KELStatsConstants.MAX_NUM_PLAYERS-1)-i;
			sampleData[player] = new ChartInfo("Player " + (player+1), 1.0 / (i+1));
		}
*/
		ChartInfo[] sampleData = new ChartInfo[6];
		for (int i = 0; i < 6; i++)
		{
			int player = (6-1)-i;
			sampleData[player] = new ChartInfo("Player " + (player+1), 1.0 / (i+1));
		}

		setChartInfo(sampleData);
	}
}
