package com.kelsoft.charts;

import java.awt.image.RenderedImage;

public interface Chart
{
	public RenderedImage doChart() throws Exception;
	public void saveChartFile(String fileName, RenderedImage image) throws Exception;

}