package com.kelsoft.charts;

import com.kelsoft.stats.StatsRosterInfo;
import com.kelsoft.stats.Configuration;
import com.kelsoft.stats.KELStatsConstants;

public class ChartsFactory
{
	public static final Chart create(String type, StatsRosterInfo info, Configuration config)
	{
		Chart chart = null;

		try
		{
			if (KELStatsConstants.CHART_TYPE_BAR_BATAVG.equalsIgnoreCase(type))
			{
				chart = new BattingAverageChart(info, config);
			}
			else if (KELStatsConstants.CHART_TYPE_BAR_ONBASE.equalsIgnoreCase(type))
			{
				chart = new OnBasePercentageChart(info, config);
			}
			else if (KELStatsConstants.CHART_TYPE_BAR_SLUGPT.equalsIgnoreCase(type))
			{
				chart = new SluggingPercentageChart(info, config);
			}
			else if (KELStatsConstants.CHART_TYPE_BAR_CONFIG.equalsIgnoreCase(type))
			{
				chart = new SampleConfigChart(info, config);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return chart;
	}
}
