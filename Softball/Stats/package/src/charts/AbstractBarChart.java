package com.kelsoft.charts;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.awt.image.RenderedImage;
import java.awt.MultipleGradientPaint.CycleMethod;
import java.io.*;
import java.util.Arrays;
import java.util.Random;
import java.util.TreeSet;
import javax.imageio.*;
import javax.swing.*;
import com.kelsoft.stats.KELStatsConstants;


public abstract class AbstractBarChart implements Chart
{
	private double          chartScalingValue  = 500.0;
	private double          chartAxisIncrement =   0.1;
	private double          chartAxisMaximum   =   1.0;
	private String          chartTitle;
	private String          chartSubTitle;
	private Color           chartBgColor;
	private Color           chartBarTopColor;
	private ChartInfo[]     info;

	public AbstractBarChart()
	{
		super();
	}

	// ACCESSOR GET METHODS
	protected double getChartScalingValue()
	{
		return chartScalingValue;
	}

	protected double getChartAxisIncrement()
	{
		return chartAxisIncrement;
	}

	protected double getChartAxisMaximum()
	{
		return chartAxisMaximum;
	}

	protected String getChartTitle()
	{
		return chartTitle;
	}

	protected String getChartSubTitle()
	{
		return chartSubTitle;
	}

	protected Color getChartBgColor()
	{
		return chartBgColor;
	}

	protected Color getChartBarTopColor()
	{
		return chartBarTopColor;
	}

	protected ChartInfo[] getChartInfo()
	{
		return info;
	}

	// ACCESSOR SET METHODS
	protected void setChartScalingValue(double value)
	{
		this.chartScalingValue = value;
	}

	protected void setChartAxisIncrement(double value)
	{
		this.chartAxisIncrement = value;
	}

	protected void setChartAxisMaximum(double value)
	{
		this.chartAxisMaximum = value;
	}

	protected void setChartTitle(String title)
	{
		this.chartTitle = title;
	}

	protected void setChartSubTitle(String subTitle)
	{
		this.chartSubTitle = subTitle;
	}

	protected void setChartBgColor(Color color)
	{
		this.chartBgColor = color;
	}

	protected void setChartBarTopColor(Color color)
	{
		this.chartBarTopColor = color;
	}

	protected void setChartInfo(ChartInfo[] info)
	{
		this.info = info;
	}

	// Returns a generated .
	public RenderedImage doChart() throws Exception
	{
		BufferedImage graph;

		try
		{
			int awidth  = 80 * info.length; // 1040;
			int aheight = 500;

			int width   = 160 + awidth; //1200;
			int height  = 625;

			int bgSize  = 150;
			int bgRatio = 75;

			double value = chartAxisMaximum;
			Rectangle2D rect;

			int offsetX = 125;

			int baseX1 = offsetX-50+15+24;			// BL
			int baseX2 = offsetX-50+15+24+18;		// TL
			int baseX3 = offsetX-50+15+24+32+18;	// TR
			int baseX4 = offsetX-50+15+24+32;		// BR

			int baseY1 = 50+aheight+25-9;			// Bar bottom
			int baseY2 = 50+aheight+25-9-9;			// Bar top
			int baseY3 = 50+aheight+25-9-9-9;		// Player bar values (average, percentages)
			int baseY4 = 50+aheight+25+20;			// Player names (1st 12 chars)
			int baseY5 = 50+aheight+25+35;			// Player names (remaining chars -if necessary)

			AffineTransform at = new AffineTransform();
			at.setToIdentity();

			// Create a buffered image in which to draw
			// Create a graphics contents on the buffered image
			graph = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = graph.createGraphics();
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			//g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC));
			FontMetrics fm = g2d.getFontMetrics(g2d.getFont());

			BufferedImage graphBackground = new BufferedImage(bgSize, bgSize, BufferedImage.TYPE_INT_RGB);
			Graphics2D gBG = graphBackground.createGraphics();
			gBG.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			//gBG.addRenderingHints(new RenderingHints(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC));

			BufferedImage graphBarFace = new BufferedImage( (baseX4-baseX1), (baseX4-baseX1), BufferedImage.TYPE_INT_RGB);
			Graphics2D gBF = graphBarFace.createGraphics();
			gBF.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			//gBF.addRenderingHints(new RenderingHints(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC));

			// Draw chart borders
			g2d.setColor(Color.WHITE);
			g2d.fillRect(0, 0, width, height);

			g2d.setColor(Color.BLACK);
			g2d.drawRect(0, 0, width-1, height-1);

			g2d.setColor(new Color(0xA0A0A0));
			g2d.fillRect(7, 7, width-14, height-14);

			g2d.setColor(Color.BLACK);
			g2d.drawRect(7, 7, width-15, height-15);

			// Draw graph title - centered
			rect = fm.getStringBounds(chartTitle, g2d);
			int titleX = (width - (int)(rect.getWidth())) / 2;
			g2d.drawString(chartTitle, titleX, 22);

			// Draw graph subtitle - centered
			rect = fm.getStringBounds(chartSubTitle, g2d);
			int subTitleX = (width - (int)(rect.getWidth())) / 2;
			g2d.drawString(chartSubTitle, subTitleX, 35);

			// Create Background gradient
			Point2D start = new Point2D.Float(0, 0);
			Point2D end   = new Point2D.Float(bgRatio, bgRatio);
			float[] dist = {0.0f, 0.98f};
			Color[] colors = {new Color(0x101010), chartBgColor};
			LinearGradientPaint p = new LinearGradientPaint(start, end, dist, colors, MultipleGradientPaint.CycleMethod.REFLECT);
			gBG.setPaint(p);
			gBG.fill(new Rectangle(0,0,bgSize, bgSize));

			// Create Bar Face gradient
			start = new Point2D.Float(0,0);
			end   = new Point2D.Float((baseX4-baseX1)/2, 0);
			float[] dist1 = {0.0f, 0.78f};
			Color[] colors1 = {chartBarTopColor.darker().darker().darker().darker(), chartBarTopColor};
			p = new LinearGradientPaint(start, end, dist1, colors1, MultipleGradientPaint.CycleMethod.REFLECT);
			gBF.setPaint(p);
			gBF.fill(new Rectangle(0,0,(baseX4-baseX1), (baseX4-baseX1)));

			// Draw graph background
			g2d.setTransform(at);
			g2d.drawImage(graphBackground,offsetX,50, awidth, aheight, null);

			// Draw graph background lines
			g2d.setColor(Color.BLACK);
			for (int i = 0; i < 11; i++)
			{
				g2d.drawLine(offsetX, i*50+50, offsetX + awidth, i*50+50);
			}
			g2d.draw(new Rectangle(offsetX,50,awidth, aheight));

			// Draw side of graph
			g2d.shear(0.0, -0.5);
			g2d.translate(0.0, 50+13.0);
			g2d.drawImage(graphBackground, offsetX-50, 50, 50, aheight,  null);
			g2d.setTransform(at);

			// Draw bottom of graph
			Polygon pBottom = new Polygon();
			pBottom.addPoint(offsetX-50, aheight+50+25);
			pBottom.addPoint(offsetX, aheight+50);
			pBottom.addPoint(offsetX + awidth, aheight+50);
			pBottom.addPoint(offsetX-50 + awidth, aheight+50+25);

			g2d.setPaint(Color.GRAY);
			g2d.fillPolygon(pBottom);
			g2d.setPaint(Color.BLACK);
			g2d.drawPolygon(pBottom);

			// Draw diagonal lines on graph side
			g2d.setPaint(Color.BLACK);
			for (int i = 0; i < 11; i++)
			{
				g2d.drawLine(offsetX-50, i*50+50+25, offsetX, i*50+50);

				// Draw vertical axis values and indicators
				g2d.drawLine(offsetX-60, i*50+50+25, offsetX-50, i*50+50+25);
				g2d.drawString( String.format("%4.3f", value), offsetX-95, i*50+50+30);
				value = value - chartAxisIncrement;
			}

			// Draw horizontal axis indicators
			for (int i = 0; i < info.length+ 1; i++)
			{
				g2d.drawLine(offsetX-50+i*80, aheight+50+25, offsetX-50+i*80, aheight+50+35);
				g2d.drawLine(offsetX-50+i*80, aheight+50+25, offsetX-50+i*80+50, aheight+50+25-25);
			}

			// Loop thru players - limit to 13 for now.... :)
			for (int i = 0; i < Math.min(KELStatsConstants.MAX_NUM_PLAYERS, info.length); i++)
			{
				int x = i*80;
				int y = (int)Math.ceil(info[i].getValue() * chartScalingValue);

    			//g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,0.95f));

				if (y > 0)
				{
					// Draw bar face
					Polygon pBarFace = new Polygon();
					pBarFace.addPoint(baseX1+x, baseY1);
					pBarFace.addPoint(baseX1+x, baseY1-y);
					pBarFace.addPoint(baseX4+x, baseY1-y);
					pBarFace.addPoint(baseX4+x, baseY1);
					//g2d.setPaint(chartBarTopColor.darker());
					//g2d.fillPolygon(pBarFace);
					g2d.drawImage(graphBarFace, baseX1+x, baseY1-y, (baseX4-baseX1), y, null);

					// Draw bar side
					Polygon pBarSide = new Polygon();
					pBarSide.addPoint(baseX4+x, baseY1);
					pBarSide.addPoint(baseX4+x, baseY1-y);
					pBarSide.addPoint(baseX3+x, baseY2-y);
					pBarSide.addPoint(baseX3+x, baseY2);

					g2d.setPaint(chartBarTopColor.darker().darker());
					g2d.fillPolygon(pBarSide);

					g2d.setPaint(Color.BLACK);
					g2d.drawPolygon(pBarFace);
					g2d.drawPolygon(pBarSide);
				}

				// Draw bar top
				Polygon pBarTop = new Polygon();
				pBarTop.addPoint(baseX1+x, baseY1-y);
				pBarTop.addPoint(baseX2+x, baseY2-y);
				pBarTop.addPoint(baseX3+x, baseY2-y);
				pBarTop.addPoint(baseX4+x, baseY1-y);

				g2d.setPaint(chartBarTopColor);
				g2d.fillPolygon(pBarTop);
				g2d.setPaint(Color.BLACK);
				g2d.drawPolygon(pBarTop);

    			//g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1.f));

				// Draw bar values
				g2d.setPaint(Color.WHITE);
				g2d.drawString( String.format("%4.3f", info[i].getValue()), baseX2+x, baseY3-y);

				// Draw player names
				g2d.setPaint(Color.BLACK);
				String pName = info[i].getName().trim();
				String pName1;

				// Split large names at 10 char, print remainder on 2nd line - if necessary
				if (pName.length() > 10)
				{
					// Check for space in name and, if so, split on space
					int pos = pName.indexOf(' ');
					if (pos > 0 && pos <= 10)
					{
						pName1 = pName.substring(pos).trim();
						pName  = pName.substring(0,pos).trim();
					}
					else
					{
						pName1 = pName.substring(10).trim();
						pName  = pName.substring(0,10).trim();
					}
					rect = fm.getStringBounds(pName1, g2d);
					int xo = (80 - (int)(rect.getWidth())) / 2;
					g2d.drawString(pName1, offsetX-50+80*i+xo, baseY5);
				}
				rect = fm.getStringBounds(pName, g2d);
				int xo = (80 - (int)(rect.getWidth())) / 2;
				g2d.drawString(pName, offsetX-50+80*i+xo, baseY4);
			}

			// Graphics context no longer needed so dispose it
			gBG.dispose();
			g2d.dispose();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		return graph;
	}

	public void saveChartFile(String fileName, RenderedImage image) throws Exception
	{
		try
		{
			// Save as PNG
			File file = new File(KELStatsConstants.CHART_DIRECTORY_NAME + "\\" + fileName);
			boolean found = ImageIO.write(image, "png", file);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			throw e;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}
}
