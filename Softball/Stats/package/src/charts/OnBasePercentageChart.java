package com.kelsoft.charts;

import java.awt.Color;
import com.kelsoft.stats.StatsRosterInfo;
import com.kelsoft.stats.Configuration;
import com.kelsoft.stats.KELStatsConstants;

public class OnBasePercentageChart extends AbstractBarChart
{
	public OnBasePercentageChart()
	{
	}

	public OnBasePercentageChart(StatsRosterInfo roster, Configuration config)
	{
		super();

		String name = config.getProperty(KELStatsConstants.TEAM_NAME_PROPERTY, KELStatsConstants.TEAM_NAME_DEFAULTS);
		setChartScalingValue(500.000);
		setChartAxisIncrement(0.100);
		setChartAxisMaximum(1.000);
		setChartTitle(name + " - " + KELStatsConstants.CHART_TYPE_BAR_ONBASE);
		int gp = roster.getGamesPlayed();
		setChartSubTitle(String.format("(Summary: Game%s)", (gp > 1 ? "s 1 - " + gp : " " + gp)));
		setChartBgColor(    Color.decode( config.getProperty(KELStatsConstants.CHART_BACKGROUND_COLOR_PROPERTY, KELStatsConstants.CHART_BACKGROUND_COLOR_DEFAULTS)));
		setChartBarTopColor(Color.decode( config.getProperty(KELStatsConstants.CHART_BARTOP_COLOR_PROPERTY,     KELStatsConstants.CHART_BARTOP_COLOR_DEFAULTS)));
		setChartInfo(roster.getAllSorted(KELStatsConstants.CHART_TYPE_ONBASE));
	}
}