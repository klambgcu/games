package com.kelsoft.charts;

import java.awt.Color;
import com.kelsoft.stats.StatsRosterInfo;
import com.kelsoft.stats.Configuration;
import com.kelsoft.stats.KELStatsConstants;

public class SluggingPercentageChart extends AbstractBarChart
{
	public SluggingPercentageChart()
	{
	}

	public SluggingPercentageChart(StatsRosterInfo roster, Configuration config)
	{
		super();

		String name = config.getProperty(KELStatsConstants.TEAM_NAME_PROPERTY, KELStatsConstants.TEAM_NAME_DEFAULTS);
		setChartScalingValue(getChartScalingValue() / 4.0);
		setChartAxisIncrement(0.400);
		setChartAxisMaximum(4.000);
		setChartTitle(name + " - " + KELStatsConstants.CHART_TYPE_BAR_SLUGPT);
		int gp = roster.getGamesPlayed();
		setChartSubTitle(String.format("(Summary: Game%s)", (gp > 1 ? "s 1 - " + gp : " " + gp)));
		setChartBgColor(    Color.decode( config.getProperty(KELStatsConstants.CHART_BACKGROUND_COLOR_PROPERTY, KELStatsConstants.CHART_BACKGROUND_COLOR_DEFAULTS)));
		setChartBarTopColor(Color.decode( config.getProperty(KELStatsConstants.CHART_BARTOP_COLOR_PROPERTY,     KELStatsConstants.CHART_BARTOP_COLOR_DEFAULTS)));
		setChartInfo(roster.getAllSorted(KELStatsConstants.CHART_TYPE_SLUGGING));
	}
}
