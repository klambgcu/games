package com.kelsoft.charts;

import com.kelsoft.charts.Chart;
import com.kelsoft.charts.ChartsFactory;
import com.kelsoft.stats.Configuration;
import com.kelsoft.stats.KELStatsConstants;
import com.kelsoft.stats.StatsRosterInfo;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.colorchooser.*;

public class ChartEditor extends JDialog implements ActionListener
{
	private Frame           mainFrame;
	private Configuration   config = new Configuration();
	private Configuration   origConfig;
	private Configuration   newConfiguration = null;
	private StatsRosterInfo rosterInfo;
	private JLabel          chartLabel;
	private JButton         buttonBGColor;
	private JButton         buttonBTColor;
	private JButton         buttonOK;
	private JButton         buttonCancel;
	private JButton         buttonReset;
	private JColorChooser   colorChooser = new JColorChooser();


	public ChartEditor(Frame owner, StatsRosterInfo rosterInfo, Configuration config)
	{
		super(owner, "Chart Configuration Editor", true);
		this.mainFrame  = owner;
		this.config.copy(config);
		this.origConfig = config;
		this.rosterInfo = rosterInfo;

		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setResizable(false);
		add(createPanel());
		pack();
		setLocationRelativeTo(mainFrame);
	}

	private JPanel createPanel()
	{
		JPanel previewPanel = new JPanel();
		previewPanel.setBorder(BorderFactory.createTitledBorder(" Preview: "));

		chartLabel = new JLabel(getChartSampleIcon());
		chartLabel.setBorder(LineBorder.createBlackLineBorder());
		previewPanel.add(chartLabel);

		buttonBGColor = new JButton("Background...");
		buttonBGColor.addActionListener(this);
		buttonBGColor.setActionCommand("buttonBGColor");

		buttonBTColor = new JButton("Bar Top...");
		buttonBTColor.addActionListener(this);
		buttonBTColor.setActionCommand("buttonBTColor");

		buttonCancel = new JButton("Cancel");
		buttonCancel.addActionListener(this);
		buttonCancel.setActionCommand("buttonCancel");

		buttonOK = new JButton("OK");
		buttonOK.addActionListener(this);
		buttonOK.setActionCommand("buttonOK");

		buttonReset = new JButton("Reset");
		buttonReset.addActionListener(this);
		buttonReset.setActionCommand("buttonReset");

		JPanel southPanel = new JPanel();
		southPanel.setLayout(new BorderLayout());
		southPanel.setBorder(BorderFactory.createTitledBorder(""));

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1,2,5,5));
		buttonPanel.setBorder(BorderFactory.createTitledBorder(" Edit Colors: "));
		buttonPanel.add(buttonBGColor);
		buttonPanel.add(buttonBTColor);

		JPanel actionPanel = new JPanel();
		actionPanel.setLayout(new GridLayout(1,3,5,5));
		actionPanel.setBorder(BorderFactory.createTitledBorder(" Confirm Action: "));
		actionPanel.add(buttonOK);
		actionPanel.add(buttonCancel);
		actionPanel.add(buttonReset);

		southPanel.add(buttonPanel, BorderLayout.WEST);
		southPanel.add(actionPanel, BorderLayout.EAST);

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());

		panel.add(previewPanel, BorderLayout.NORTH);
		panel.add(southPanel,   BorderLayout.SOUTH);

		return panel;
	}

	private ImageIcon getChartSampleIcon()
	{
		Chart         chart       = null;
		BufferedImage sampleChart = null;
		BufferedImage smallChart  = null;

		try
		{
			chart = ChartsFactory.create(KELStatsConstants.CHART_TYPE_BAR_CONFIG, rosterInfo, config);
			sampleChart = (BufferedImage)chart.doChart();
			int w = sampleChart.getWidth();
			int h = sampleChart.getHeight() / 2;

			smallChart = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = smallChart.createGraphics();
			g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
			g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC));
			g2d.drawImage(sampleChart, 0,0, w, h, null);
		}
		catch (Exception ignore)
		{
		}

		return new ImageIcon(smallChart);
	}

	public Configuration getConfiguration()
	{
		return newConfiguration;
	}

	public void actionPerformed(ActionEvent evt)
	{
		String action = evt.getActionCommand();
		String color = "#000000";
		String title = "Color";

		if ("buttonCancel".equals(action))
		{
			//newConfiguration = origConfiguration; - // leave null
			setVisible(false);
		}
		else if ("buttonOK".equals(action))
		{
			newConfiguration = config;
			setVisible(false);
		}
		else if ("buttonReset".equals(action))
		{
			config.copy(origConfig);
			chartLabel.setIcon(getChartSampleIcon());
		}
		else
		{
			if ("buttonBGColor".equals(action))
			{
				color = config.getProperty(KELStatsConstants.CHART_BACKGROUND_COLOR_PROPERTY, KELStatsConstants.CHART_BACKGROUND_COLOR_DEFAULTS);
				title = "Choose Background Color";
			}
			else if ("buttonBTColor".equals(action))
			{
				color = config.getProperty(KELStatsConstants.CHART_BARTOP_COLOR_PROPERTY, KELStatsConstants.CHART_BARTOP_COLOR_DEFAULTS);
				title = "Choose Bar Top Color";
			}

			Color newColor = colorChooser.showDialog(mainFrame, title, Color.decode(color));
			if (newColor != null)
			{
				String newValue = String.format("#%02X%02X%02X", newColor.getRed(), newColor.getGreen(), newColor.getBlue());

				if ("buttonBGColor".equals(action))
				{
					config.setProperty(KELStatsConstants.CHART_BACKGROUND_COLOR_PROPERTY, newValue);
				}
				else if ("buttonBTColor".equals(action))
				{
					config.setProperty(KELStatsConstants.CHART_BARTOP_COLOR_PROPERTY, newValue);
				}
				chartLabel.setIcon(getChartSampleIcon());
			}
		}
	}
}