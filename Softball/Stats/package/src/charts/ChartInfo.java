package com.kelsoft.charts;

public class ChartInfo implements Comparable
{
	private String name;
	private double value;

	public ChartInfo()
	{
	}

	public ChartInfo(String name, double value)
	{
		this.name = name;
		this.value = value;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setValue(double value)
	{
		this.value = value;
	}

	public String getName()
	{
		return this.name;
	}

	public double getValue()
	{
		return this.value;
	}

	public int compareTo(Object o)
	{
		int result = 0;

		if (this.value < ((ChartInfo) o).getValue())
		{
			result = -1;
		}
		else if (this.value > ((ChartInfo) o).getValue())
		{
			result = 1;
		}
		else
		{
			// Values same, order by name next
			result = this.name.compareTo(((ChartInfo) o).getName());
		}
		return result;
	}
}