cls
cd ..
javac -cp .;.\build\jxl.jar -d build src\stats\*.java src\charts\*.java
cd build

del SoftballStats.jar
copy jxl.jar SoftballStats.jar
jar -uvfm SoftballStats.jar meta-inf/MANIFEST.MF com
java -cp .;.\jxl.jar com.kelsoft.stats.KELStats
