TEAMNAMEV	Cerritos Impact	RED
TEAMNAMEH	San Dimas	PURPLE
#	Pos.	Num.	Name
#
SLV	9	2	Rachel	L
SLV	8	27	Vicky
SLV	4	00	Luci
SLV	1	48	Nicole
SLV	5	7	Haley
SLV	10	20	Briana
SLV	2	99	Ashley
SLV	6	29	Kaitlyn
SLV	3	22	Courtney
SLV	7	15	Sabrina
SLV		32	Justina
SLV		21	Christabel
#
SLH		7	Karina Cardenas
SLH	6	22	Gabrielle Celeya
SLH	1	6	Rebecca Amploye
SLH	4	5	Erin Rode
SLH	5	88	Kat Boneman
SLH	8	8	Aileen Olivarez
SLH	7	18	Alondra Lima
SLH	2	2	Krystal Vigoa
SLH		75	Renee Rodriguez
SLH	3	12	Alondra Reynoso
SLH	9	15	Marina Garcia
#
INN	T	1
#
2	b,c,b,b,b,BB
27	bwp,2	>2B,b,b,2	S3B,b,BB
00	FO6
48	f,b,27	S2B,b,f,s,D3,MSG:Bases Loaded
7	f,b,b,b,b,BB,RBI1,SCORE#2,27	>3B;48	>2B
20	b,b,FO3
99	b,b,f,bwp,27	>HB;48	>3B,7	>2B,b,MSG:Bases Loaded
PR	99	20
29	b,c,b,f,b,f,f,b,BB,RBI1,SCORE#48,7	>3B;20	>2B,MSG:Bases Loaded
22	f,LD81B,RBI2,SCORE#7#20,29	>2B
15	b,f,s,f,bwp,29	>3B;22	>2B,b,b,BB,MSG:Bases Loaded
SH	1	6	12
32	b,ROEGM5,SCORE#29,22	>3B;15	>2B
21	b,c,b,c,LO3,LOB#22#15#32
#
INN	B	1
#
7	b,b,c,LD81B
22	s,7	PO2B2-6,b,LD81B
6	FO9
5	f,f,LD81B
88	s,f,s,K,LOB#22#5
#
INN	T	2
#
2	b,b,b,c,b,BB
27	s,2	S2B,LD9HR,RBI2,SCORE#2#27
00	f,b,LD81B,00	>2BOTE8
48	f,b,F61B
7	FO5
20	b,ROEGM4,00	>3B;48	>2B,MSG:Bases Loaded
99	s,s,s,K
29	b,f,f,b,ROEGM5,SCORE#00#48,20	>3B;29	>2B
22	c,GO6-3,LOB#20#29
#
INN	B	2
#
8	f,b,GO4-3
18	f,f,LD81B
2	b,s,GO4-3,18	>2B
75	bwp,18	>3B,FO9,LOB#18
#
INN	T	3
#
15	b,b,b,FO1
32	f,c,c,K
21	b,c,b,b,c,f,c,K
#
INN	B	3
#
SV	3	22	2
SV	4	00	15
SV	5	7	22
SV	7	15	21
SV	8	27	32
SV	9	2	20
12	c,c,s,K
15	c,GO1-3
7	b,b,LD81B
22	b,LD81B,7	>2B
6	c,RFCG6-2B6,7	TO2B6-6,LOB#22#6
#
INN	T	4
#
2	b,b,b,FO7
27	b,b,b,b,BB
00	c,27	S2B,GO5-3,27	>3B
48	b,c,b,b,b,BB
7	LD81B,RBI1,SCORE#27,48	>3B;7	>2B
20	b,b,c,f,F91B,RBI1,SCORE#48,7	>3B
99	c,20	S2B,c,b,b,f,f,b,LD71B,RBI1,SCORE#7,20	>3B
29	b,bwp,20	>HB;00	>3B,b,c,b,BB
22	f,b,f,s,K,LOB#99#29
#
INN	B	4
#
5	b,G92B
88	s,F91B,5	TOHB4-2,88	>2B
8	b,b,f,s,b,GO4-3,88	>3B
18	LD81B,RBI1,SCORE#88
2	RFCG6-2B4,18	TO2B6-4,LOB#2
#
GAMEENDS.
