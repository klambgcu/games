Impact All Stars - Cerritos Softball 2011 : Western National Tournament Game 3

Thursday, August 4, 2011 8:00 AM

Visitors:     Cerritos Impact
Home:         Twin Falls Idaho Ice

Winning Pitcher: #7  A. Williams
Losing Pitcher:  #48 Nicole,
                 #7  Haley

Final Score: Cerritos Impact 3, Twin Falls Idaho Ice 6

Team Name                       1  2  3  4  5  6  7    R  H  E  L
------------------------------ -- -- -- -- -- -- --   -- -- -- --
Cerritos Impact                 1  0  1  0  1  0  x    3  0  5  5
Twin Falls Idaho Ice            4  0  0  0  0  2  x    6  7  5  6

Starting Lineups:
Cerritos Impact                         Twin Falls Idaho Ice                    
Pos Num Name                            Pos Num Name                            
--- --- ------------------------------  --- --- ------------------------------  
RF. #2  Rachel                          CF. #23 K. Watts                      
CF. #27 Vicky                           3B. #3  H. Ackerman                   
    #20 Briana                          P.  #7  A. Williams                   
C.  #99 Ashley                          1B. #11 M. Hinojos                    
P.  #48 Nicole                          2B. #5  L. Parker                     
3B. #32 Justina                             #15 K. Baumert                    
SS. #29 Kaitlyn                         C.  #9  T. Johnson                    
2B. #7  Haley                           SS. #10 S. Fleming                    
1B. #22 Courtney                        RF. #27 M. Ford                       
LF. #15 Sabrina                         LF. #13 B. Johnson                    
    #21 Christabel                          #30 C. Cooper                     
    #00 Luci                                #34 S. Cooper                     
Inning 1
	Top: Cerritos Impact at bat.
		Play 1: #2 Rachel strikes out swinging (csbs). Out 1.
		Play 2: #27 Vicky reaches base on error as grounder to 2B #5 L. Parker is mishandled. (cc).
		Play 3: #20 Briana at bat,  #27 Vicky advances to 2B.
		Play 4: #20 Briana hits a ground out to Pitcher #7 A. Williams and is thrown out at 1B #11 M. Hinojos (b). Out 2. #27 Vicky advances to 3B.
		Play 5: #99 Ashley reaches base on error as grounder to 3B #3 H. Ackerman is thrown wildly to 1B #11 M. Hinojos . (bb). #27 Vicky scores.
		Substitution: Cerritos Impact #21 Christabel to pinch run for #99 Ashley 
		Play 6: #48 Nicole strikes out looking (csc). Out 3. #21 Christabel is left on base.
		Summary: Runs: 1, Hits: 0, Errors: 2, LOB: 1.

	Bottom: Twin Falls Idaho Ice at bat.
		Play 7: #23 K. Watts walks (bbfbff).
		Play 8: #3 H. Ackerman hits a grounder towards Pitcher #48 Nicole for a single (). #23 K. Watts advances to 2B.
		Play 9: #7 A. Williams hits a line drive towards CF #27 Vicky for a single (cfb). #23 K. Watts advances to 3B. #3 H. Ackerman advances to 2B. (Bases Loaded) 
		Play 10: #11 M. Hinojos strikes out swinging (cfbbs). Out 1.
		Play 11: #5 L. Parker hits a grounder towards RF #2 Rachel for a single (cbbfb). 2 RBIs, #23 K. Watts, #3 H. Ackerman all score. #7 A. Williams advances to 3B.
		Play 12: #15 K. Baumert at bat,  #5 L. Parker steals 2B.
		Play 13: #15 K. Baumert hits a line drive towards LF #15 Sabrina for a single (c). 1 RBI, #7 A. Williams scores. #5 L. Parker advances to 3B.
		Play 14: #9 T. Johnson hits a line drive towards CF #27 Vicky for a single (f). #15 K. Baumert is thrown out at 2B from CF #27 Vicky to 2B #7 Haley Out 2. 1 RBI, #5 L. Parker scores.
		Substitution: Twin Falls Idaho Ice #34 S. Cooper to pinch run for #9 T. Johnson 
		Play 15: #10 S. Fleming hits a fly out to 2B #7 Haley (fbsb). Out 3. #34 S. Cooper is left on base.
		Summary: Runs: 4, Hits: 5, Errors: 0, LOB: 1.

Inning 2
	Top: Cerritos Impact at bat.
		Play 16: #32 Justina hits a fly out to SS #10 S. Fleming (bbb). Out 1.
		Play 17: #29 Kaitlyn hits a ground out to 3B #3 H. Ackerman and is thrown out at 1B #11 M. Hinojos (bbcsfff). Out 2.
		Play 18: #7 Haley hits a ground out to 3B #3 H. Ackerman and is thrown out at 1B #11 M. Hinojos (bc). Out 3.
		Summary: Runs: 0, Hits: 0, Errors: 0, LOB: 0.

	Bottom: Twin Falls Idaho Ice at bat.
		Substitution: Cerritos Impact #7 Haley to Pitcher for #48 Nicole 
		Substitution: Cerritos Impact #48 Nicole to 2B for #7 Haley 
		Play 19: #27 M. Ford hits a ground out to Pitcher #7 Haley and is thrown out at 1B #22 Courtney (cbf). Out 1.
		Play 20: #23 K. Watts reaches base on error as grounder to 3B #32 Justina is mishandled. (fbff).
		Play 21: #3 H. Ackerman reaches base on fielder's choice as grounder to SS #29 Kaitlyn is thrown to 2B #48 Nicole (). #23 K. Watts is thrown out at 2B from SS #29 Kaitlyn to 2B #48 Nicole Out 2.
		Play 22: #7 A. Williams reaches base on error as fly ball to 2B #48 Nicole is mishandled. (bbfbs). #3 H. Ackerman advances to 3B. #7 A. Williams is thrown out at 1B from 1B #22 Courtney to 1B #22 Courtney Out 3. #3 H. Ackerman is left on base. (Hesitation on #7) 
		Summary: Runs: 0, Hits: 0, Errors: 2, LOB: 1.

Inning 3
	Top: Cerritos Impact at bat.
		Play 23: #22 Courtney hits a line drive out to 2B #5 L. Parker (c). Out 1.
		Play 24: #2 Rachel walks (bbbb).
		Play 25: #27 Vicky walks (cbbbfb). #2 Rachel steals 3B. #27 Vicky steals 2B.
		Play 26: #20 Briana hits a ground out to 2B #5 L. Parker and is thrown out at 1B #11 M. Hinojos (b). Out 2. 1 RBI, #2 Rachel scores. #27 Vicky advances to 3B.
		Play 27: #99 Ashley walks (bbbcb).
		Substitution: Cerritos Impact #21 Christabel to pinch run for #99 Ashley 
		Play 28: #48 Nicole at bat,  #21 Christabel steals 2B.
		Play 29: #48 Nicole strikes out looking (ccc). Out 3. #27 Vicky, #21 Christabel are left on base.
		Summary: Runs: 1, Hits: 0, Errors: 0, LOB: 2.

	Bottom: Twin Falls Idaho Ice at bat.
		Play 30: #11 M. Hinojos walks (cbbbsb).
		Play 31: #5 L. Parker hits a fly out to Pitcher #7 Haley (cb). Out 1. #11 M. Hinojos is thrown out at 1B from Pitcher #7 Haley to 1B #22 Courtney Out 2. (Double Play) 
		Play 32: #15 K. Baumert walks (bbbcb).
		Play 33: #9 T. Johnson hits a fly out to LF #15 Sabrina (cbcb). Out 3. #15 K. Baumert is left on base.
		Summary: Runs: 0, Hits: 0, Errors: 0, LOB: 1.

Inning 4
	Top: Cerritos Impact at bat.
		Play 34: #32 Justina hits a ground out to Pitcher #7 A. Williams and is thrown out at 1B #11 M. Hinojos (). Out 1.
		Play 35: #29 Kaitlyn reaches base on error as fly ball to RF #27 M. Ford is mishandled. (b).
		Play 36: #7 Haley hits a fly out to CF #23 K. Watts (bbf). Out 2.
		Play 37: #22 Courtney hits a ground out to 2B #5 L. Parker and is thrown out at 1B #11 M. Hinojos (). Out 3. #29 Kaitlyn is left on base.
		Summary: Runs: 0, Hits: 0, Errors: 1, LOB: 1.

	Bottom: Twin Falls Idaho Ice at bat.
		Play 38: #10 S. Fleming strikes out swinging (bscs). Out 1.
		Play 39: #27 M. Ford reaches base on error as grounder to Catcher #99 Ashley is mishandled. ().
		Play 40: #23 K. Watts at bat,  #27 M. Ford steals 2B.
		Play 41: #23 K. Watts at bat,  #27 M. Ford steals 3B.
		Play 42: #23 K. Watts strikes out swinging (cbbbcffs). Out 2.
		Play 43: #3 H. Ackerman hits a fly out to SS #29 Kaitlyn (). Out 3. #27 M. Ford is left on base.
		Summary: Runs: 0, Hits: 0, Errors: 1, LOB: 1.

Inning 5
	Top: Cerritos Impact at bat.
		Play 44: #2 Rachel strikes out looking (bbcfbc). Out 1.
		Play 45: #27 Vicky reaches base on error as grounder to 3B #3 H. Ackerman is thrown wildly to 1B #11 M. Hinojos . (bc). #27 Vicky advances to 2B on throwing error by 3B #3 H. Ackerman .
		Play 46: #20 Briana at bat,  #27 Vicky advances to 3B.
		Play 47: #20 Briana walks (ccfbffbfbb).
		Play 48: #99 Ashley at bat,  #20 Briana steals 2B.
		Play 49: #99 Ashley hits a ground out to SS #10 S. Fleming and is thrown out at 1B #11 M. Hinojos (cc). Out 2. 1 RBI, #27 Vicky scores. #20 Briana advances to 3B.
		Play 50: #48 Nicole hits a line drive out to 2B #5 L. Parker (bb). Out 3. #20 Briana is left on base.
		Summary: Runs: 1, Hits: 0, Errors: 2, LOB: 1.

	Bottom: Twin Falls Idaho Ice at bat.
		Play 51: #7 A. Williams strikes out looking (bbbcfc). Out 1.
		Play 52: #11 M. Hinojos strikes out swinging (sbbss). Out 2.
		Play 53: #5 L. Parker reaches base on error as grounder to 3B #32 Justina is thrown wildly to 1B #22 Courtney . (cbbbsf).
		Play 54: #15 K. Baumert hits a line drive towards RF #2 Rachel for a single (cb). #5 L. Parker advances to 2B.
		Substitution: Twin Falls Idaho Ice #13 B. Johnson to pinch run for #15 K. Baumert 
		Play 55: #9 T. Johnson hits a fly out to LF #15 Sabrina (bbc). Out 3. #5 L. Parker, #13 B. Johnson are left on base.
		Summary: Runs: 0, Hits: 1, Errors: 1, LOB: 2.

Inning 6
	Top: Cerritos Impact at bat.
		Play 56: #32 Justina hits a line drive out to SS #10 S. Fleming (sbs). Out 1.
		Play 57: #29 Kaitlyn hits a ground out to SS #10 S. Fleming and is thrown out at 1B #11 M. Hinojos (bsbf). Out 2.
		Play 58: #7 Haley hits a fly out to CF #23 K. Watts (b). Out 3.
		Summary: Runs: 0, Hits: 0, Errors: 0, LOB: 0.

	Bottom: Twin Falls Idaho Ice at bat.
		Play 59: #10 S. Fleming strikes out swinging (bssfs). Out 1.
		Play 60: #27 M. Ford hits a fly out to 1B #22 Courtney (bsf). Out 2.
		Play 61: #23 K. Watts walks (bbfbb).
		Play 62: #3 H. Ackerman reaches base on error as fly ball to 2B #48 Nicole is mishandled. (). #23 K. Watts advances to 3B. #3 H. Ackerman advances to 2B.
		Play 63: #7 A. Williams hits a line drive towards CF #27 Vicky for a double (). 2 RBIs, #23 K. Watts, #3 H. Ackerman all score.
		Play 64: #34 S. Cooper  (Time Limit Reached) 
		Summary: Runs: 2, Hits: 1, Errors: 1, LOB: 0.

Game Ends.
