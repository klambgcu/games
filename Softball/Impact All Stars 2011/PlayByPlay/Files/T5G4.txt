TEAMNAMEV	Cerritos Impact	RED
TEAMNAMEH	Santa Monica	BLUE
#	Pos.	Num.	Name
#
SLV	9	2	Rachel	L
SLV	8	27	Vicky
SLV	5	32	Justina
SLV	1	7	Haley
SLV	10	20	Briana
SLV	4	48	Nicole
SLV	2	99	Ashley
SLV	6	29	Kaitlyn
SLV	3	22	Courtney
SLV	7	15	Sabrina
SLV		21	Christabel
SLV		00	Luci
#
SLH	2	17	Southerbind
SLH	3	00	Condon
SLH	4	10	Miller
SLH	5	12	Hardy
SLH	6	22	Yzaguirre
SLH	1	31	Kalbrosky
SLH	7	2	Breuer
SLH	8	23	Saltzman
SLH	9	88	Reynoso
SLH		13	Costas
SLH		14	Cousins
#
INN	T	1
#
2	b,c,LD81B
27	b,2	S2B,c,b,SACB5-1B4,2	>3B
32	b,f,bwp,2	>HB,c,G71B
7	c,b,F81B,32	>2B
20	b,b,32	S3B;7	S2B;32	>HBOTE2,F41B,RBI1,SCORE#7,20	>2B
48	FO7
99	f,f,b,20	>3B,G71B,RBI1,SCORE#20
PR	99	21
29	c,b,21	S2B,b,f,LD61B,RBI1,SCORE#21,29	>2B
22	LD81B,29	>3B
2	b,s,22	S3B,FO4,LOB#29#22
#
INN	B	1
#
17	c,b,FO4
00	b,b,F71B
10	c,b,b,b,b,BB,00	>2B
12	c,f,bwp,00	>3B;10	>2B,GO5-3
22	b,b,b,b,BB,MSG:Bases Loaded
31	c,s,f,ROEGM1,00	>HB;10	>3B;22	>2B
2	c,s,GO4-3,LOB#10#22#31
#
INN	T	2
#
27	b,f,b,s,K
32	b,b,b,f,c,FO7
7	c,c,LO6
#
INN	B	2
#
23	c,b,c,b,s,K
88	b,s,b,b,s,b,BB
17	G81B,88	>2B
00	G51B,88	>3B;17	>2B
10	b,b,b,c,b,BB,RBI1,SCORE#88,17	>3B;00	>2B
12	LO8
22	c,b,FO4,LOB#17#00#10
#
INN	T	3
#
20	c,LD71B
48	b,c,RFCG7-2B4,20	TO2B7-4
99	b,b,48	>2BOTE2,c,f,b,f,f,FO1
29	GO5-3,LOB#48
#
INN	B	3
#
31	f,c,f,b,LD71B,31	>1BOCE3
2	f,b,b,bwp,31	>2B,b,BB
23	f,b,c,GO3u
88	b,FO5
17	b,bwp,ROEGT6-1B3,RBI2,SCORE#31#2
00	FO9,LOB#17
#
INN	T	4
#
22	GO4-3
2	b,c,FO6
27	G71B
32	f,b,27	S2B,G71B,27	>3B;32	>2B
7	LO8,LOB#27#32
#
INN	B	4
#
10	c,b,f,LO6
12	b,b,f,ROEFM7
22	HBP,12	>2B
SV	1	7	48
SV	4	48	7
31	bwp,12	>3B;22	>2B,b,GO6-3,RBI1,SCORE#12,22	>3B
2	b,LD81B,RBI1,SCORE#22
23	b,FO8,LOB#2
#
INN	T	5
#
SH	1	31	14
20	b,c,b,F81B
48	FO3
99	c,b,c,s,K
29	b,GO5-3,LOB#20
#
GAMEENDS.
