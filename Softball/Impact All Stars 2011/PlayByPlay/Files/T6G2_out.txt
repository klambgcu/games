Impact All Stars - Cerritos Softball 2011 : California State Tournament Game 2

Friday, July 8, 2011 3:00 PM

Visitors:     Cerritos Impact
Home:         West Valley 

Losing Pitcher:  #11 J. Weiss
Winning Pitcher: #7  Haley

Final Score: Cerritos Impact 5, West Valley 3

Team Name                       1  2  3  4  5  6  7    R  H  E  L
------------------------------ -- -- -- -- -- -- --   -- -- -- --
Cerritos Impact                 0  5  0  0  0  x  x    5  3  3  6
West Valley                     2  0  0  0  1  x  x    3  5  0  4

Starting Lineups:
Cerritos Impact                         West Valley                             
Pos Num Name                            Pos Num Name                            
--- --- ------------------------------  --- --- ------------------------------  
RF. #2  Rachel                          SS. #7  C. Coughlin                   
CF. #27 Vicky                           2B. #16 A. Morongell                  
3B. #32 Justina                         P.  #11 J. Weiss                      
P.  #7  Haley                           1B. #8  P. Green                      
SS. #29 Kaitlyn                         3B. #18 H. Heath                      
2B. #48 Nicole                          LF. #2  S. Feldman                    
C.  #99 Ashley                          RF. #6  H. Polakov                    
    #20 Briana                          CF. #17 A. Ramirez                    
1B. #22 Courtney                        C.  #5  L. O'Shea                     
LF. #15 Sabrina                             #27 V. Herrera                    
    #21 Christabel                          #9  H. Gold                       
    #00 Luci                                #1  R. Russell                    

Inning 1
	Top: Cerritos Impact at bat.
		Play 1: #2 Rachel is hit by the pitch (bb).
		Play 2: #27 Vicky strikes out swinging (ffs). Out 1.
		Play 3: #32 Justina at bat,  #2 Rachel steals 2B.
		Play 4: #32 Justina at bat,  #2 Rachel advances to 3B.
		Play 5: #32 Justina strikes out looking (bfsbbc). Out 2.
		Play 6: #7 Haley walks (bbcbcb).
		Play 7: #29 Kaitlyn hits a line drive out to SS #7 C. Coughlin (cb). Out 3. #2 Rachel, #7 Haley are left on base.
		Summary: Runs: 0, Hits: 0, Errors: 0, LOB: 2.

	Bottom: West Valley at bat.
		Play 8: #7 C. Coughlin hits a grounder towards RF #2 Rachel for a single (bcbcfb).
		Play 9: #16 A. Morongell at bat,  #7 C. Coughlin steals 2B.
		Play 10: #16 A. Morongell walks (bffbbb).
		Play 11: #11 J. Weiss hits a line drive towards CF #27 Vicky for a single (cff). 1 RBI, #7 C. Coughlin scores. #16 A. Morongell advances to 3B.
		Play 12: #5 L. O'Shea hits a ground out to Pitcher #7 Haley and is thrown out at 1B #22 Courtney (cb). Out 1. 1 RBI, #16 A. Morongell scores. #11 J. Weiss advances to 3B.
		Play 13: #18 H. Heath strikes out looking (cbsc). Out 2.
		Play 14: #2 S. Feldman strikes out looking (cfc). Out 3. #11 J. Weiss is left on base.
		Summary: Runs: 2, Hits: 2, Errors: 0, LOB: 1.

Inning 2
	Top: Cerritos Impact at bat.
		Play 15: #48 Nicole strikes out swinging (bccfs). Out 1.
		Play 16: #99 Ashley is hit by the pitch (cbbfb).
		Substitution: Cerritos Impact #21 Christabel to pinch run for #99 Ashley 
		Play 17: #20 Briana at bat,  #21 Christabel steals 2B.
		Play 18: #20 Briana at bat,  #21 Christabel steals 3B.
		Play 19: #20 Briana walks (bbbb).
		Play 20: #22 Courtney at bat,  #20 Briana steals 2B.
		Play 21: #22 Courtney walks (bcbbb). (Bases Loaded) 
		Play 22: #2 Rachel walks (cbbbb). 1 RBI, #21 Christabel scores. #20 Briana advances to 3B. #22 Courtney advances to 2B.
		Substitution: West Valley #1 R. Russell to Pitcher for #11 J. Weiss 
		Substitution: West Valley #11 J. Weiss to LF for #2 S. Feldman 
		Play 23: #27 Vicky at bat,  #20 Briana advances to home and scores. #22 Courtney advances to 3B. #2 Rachel advances to 2B.
		Play 24: #27 Vicky walks (bbbb).
		Play 25: #32 Justina hits a line drive towards LF #11 J. Weiss for a single (bc). 1 RBI, #22 Courtney scores. #2 Rachel advances to 3B. #27 Vicky advances to 2B.
		Play 26: #7 Haley hits a grounder towards RF #6 H. Polakov for a single (bbcb). 2 RBIs, #2 Rachel, #27 Vicky all score. #32 Justina advances to 3B. #7 Haley advances to 2B.
		Play 27: #29 Kaitlyn hits a fly out to 3B #18 H. Heath (b). Out 2.
		Play 28: #48 Nicole hits a ground out to SS #7 C. Coughlin and is thrown out at 1B #8 P. Green (cb). Out 3. #32 Justina, #7 Haley are left on base.
		Summary: Runs: 5, Hits: 2, Errors: 0, LOB: 2.

	Bottom: West Valley at bat.
		Play 29: #6 H. Polakov strikes out looking (ccbbc). Out 1.
		Play 30: #17 A. Ramirez hits a grounder towards SS #29 Kaitlyn for a single (bc).
		Play 31: #5 L. O'Shea strikes out swinging (cfbs). Out 2.
		Play 32: #7 C. Coughlin walks (bbbb).
		Play 33: #16 A. Morongell reaches base on double error after a dropped 3rd strike from Catcher #99 Ashley and a wild throw to 1B #22 Courtney (bcbfbs).
		Play 34: #11 J. Weiss hits a line drive out to SS #29 Kaitlyn (ccbbb). Out 3. #17 A. Ramirez, #7 C. Coughlin are left on base.
		Summary: Runs: 0, Hits: 1, Errors: 2, LOB: 2.

Inning 3
	Top: Cerritos Impact at bat.
		Play 35: #99 Ashley hits a line drive towards LF #11 J. Weiss for a single (bcbfbffffff).
		Substitution: Cerritos Impact #21 Christabel to pinch run for #99 Ashley 
		Substitution: West Valley #6 H. Polakov to Pitcher for #11 J. Weiss 
		Substitution: West Valley #11 J. Weiss to RF for #6 H. Polakov 
		Play 36: #20 Briana hits a sacrifice bunt to Pitcher #6 H. Polakov  (). Out 1. #21 Christabel advances to 2B.
		Play 37: #22 Courtney hits a ground out to 2B #16 A. Morongell and is thrown out at 1B #8 P. Green (bfb). Out 2. #21 Christabel advances to 3B.
		Play 38: #2 Rachel strikes out swinging (cbss). Out 3. #21 Christabel is left on base.
		Summary: Runs: 0, Hits: 1, Errors: 0, LOB: 1.

	Bottom: West Valley at bat.
		Play 39: #8 P. Green hits a fly out to 3B #32 Justina (b). Out 1.
		Play 40: #18 H. Heath strikes out swinging (css). Out 2.
		Play 41: #1 R. Russell strikes out looking (cfc). Out 3.
		Summary: Runs: 0, Hits: 0, Errors: 0, LOB: 0.

Inning 4
	Top: Cerritos Impact at bat.
		Play 42: #27 Vicky walks (bbbcb).
		Play 43: #32 Justina hits a fly out to 2B #16 A. Morongell (). Out 1.
		Play 44: #7 Haley at bat,  #27 Vicky advances to 2B.
		Play 45: #7 Haley strikes out looking (cfbc). Out 2.
		Play 46: #29 Kaitlyn at bat,  #27 Vicky steals 3B.
		Play 47: #29 Kaitlyn strikes out swinging (scffffs). Out 3. #27 Vicky is left on base.
		Summary: Runs: 0, Hits: 0, Errors: 0, LOB: 1.

	Bottom: West Valley at bat.
		Play 48: #6 H. Polakov hits a ground out to 3B #32 Justina and is thrown out at 1B #22 Courtney (c). Out 1.
		Play 49: #9 H. Gold hits a ground out to Pitcher #7 Haley and is thrown out at 1B #22 Courtney (cbf). Out 2.
		Play 50: #5 L. O'Shea strikes out looking (cbbfbc). Out 3.
		Summary: Runs: 0, Hits: 0, Errors: 0, LOB: 0.

Inning 5
	Top: Cerritos Impact at bat.
		Play 51: #48 Nicole hits a ground out to 2B #16 A. Morongell and is thrown out at 1B #8 P. Green (c). Out 1.
		Play 52: #99 Ashley hits a line drive out to Pitcher #6 H. Polakov (cf). Out 2.
		Play 53: #20 Briana hits a fly out to CF #17 A. Ramirez (cbbbf). Out 3.
		Summary: Runs: 0, Hits: 0, Errors: 0, LOB: 0.

	Bottom: West Valley at bat.
		Play 54: #7 C. Coughlin hits a fly ball towards RF #2 Rachel for a double (bb). #7 C. Coughlin advances to 3B on throwing error by RF #2 Rachel .
		Play 55: #16 A. Morongell at bat,  #7 C. Coughlin advances to home and scores.
		Play 56: #16 A. Morongell hits a grounder towards RF #2 Rachel for a single (cbfbb).
		Play 57: #11 J. Weiss walks (bbbcb). #16 A. Morongell advances to 2B.
		Play 58: #8 P. Green hits a line drive out to 2B #48 Nicole (b). Out 1. #16 A. Morongell is thrown out at 2B from 2B #48 Nicole to SS #29 Kaitlyn Out 2. (Double Play! #8 nearly thrown out at 1B for a Triple Play!) 
		Play 59: #27 V. Herrera strikes out swinging (ccs). Out 3. #11 J. Weiss is left on base.
		Summary: Runs: 1, Hits: 2, Errors: 1, LOB: 1.

Game Ends.
