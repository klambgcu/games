TEAMNAMEH	Cerritos Impact	RED
TEAMNAMEV	West Grove	PURPLE
#	Pos.	Num.	Name
#
SLH	9	2	Rachel	L
SLH	8	27	Vicky
SLH	5	32	Justina
SLH	3	48	Nicole
SLH		7	Haley
SLH	10	20	Briana
SLH	2	99	Ashley
SLH	6	29	Kaitlyn
SLH	1	22	Courtney
SLH	4	00	Luci
SLH	7	15	Sabrina
SLH		21	Christabel
#
SLV	2	22	Brooke Durand
SLV	3	1	Marissa Madrid
SLV	4	41	Jamie Gilbert
SLV	1	6	Dom. Espinoza
SLV	5	5	Briana Covarrubias
SLV	6	28	Mia Palomino
SLV	7	99	Anna Navarro
SLV	8	16	Jessica Reeser
SLV	9	00	Katelyn Christenson
SLV		03	Natalie Reeser
SLV		10	Cece Arzaga
SLV		25	Mady Christensen
#
INN	T	1
#
22	c,c,bw,b,F81B
1	f,f,bwp,22	>2B,b,LD81B,RBI1,SCORE#22,1	>2B
41	c,c,f,G81B,RBI1,SCORE#1
6	b,41	S2B,b,c,b,b,BB
5	BUNT11B,41	>3B;6	>2B,MSG:Bases Loaded
28	c,f,b,f,b,LD8HR,RBI4,SCORE#41#6#5#28,MSG:Grand Slam Home Run
99	b,GO4-3
16	c,GO1-3
00	FO9
#
INN	B	1
#
2	c,s,c,K
27	c,c,LD91B
32	c,F81B,27	>3B;32	>2B
48	c,RFCG1-HB2,27	TOHB1-2,32	>3B;48	>2B
7	c,c,b,b,FO8,LOB#32#48
#
INN	T	2
#
03	FO9
10	c,f,bw,c,K
25	LD61B
22	F81B,25	>3B
1	c,22	S2B,c,GO4-3,LOB#25#22
#
INN	B	2
#
20	f,s,b,b,b,b,BB
99	b,c,c,b,f,RFCG5-2B4,20	TO2B5-4
29	b,f,f,LD71B,99	>2B
22	f,RFCG6-2B4,99	TO2B6-4,29	>HB;22	>3BOTE6
00	b,FO1,LOB#22
#
INN	T	3
#
SH	1	22	48
SH	3	48	7
41	b,b,f,FO4
6	c,ROEGM6
5	b,6	S2B;6	>3BOTE2,LD81B,RBI1,SCORE#6
28	c,5	S2B,b,bw,5	>3B,GO1-3
99	b,GO1-3,LOB#5
#
INN	B	3
#
15	b,b,c,f,ROEGM6
21	c,b,f,f,GO4-3,15	>2B
2	c,b,GO4-3,15	>3B
27	b,G81B,RBI1,SCORE#15
32	c,27	PO2B2-6
#
INN	T	4
#
SH	3	7	22
16	c,LD71B
00	s,f,GO1-3,16	>2B
03	b,f,s,G81B,16	>3B,SCORE#16,03	>2B
18	GO1-3,03	>3B
25	b,GO1-3,LOB#03
#
INN	B	4
#
32	f,b,b,s,GO5-3
48	GO1-3
7	b,b,G61B
20	GO4-3,LOB#7
#
INN	T	5
#
SH	4	00	15
SH	5	32	7
SH	7	15	21
SH	8	27	32
22	b,LD81B
1	b,22	PO2B2-6,GO1-3
41	c,b,c,b,G81B
6	b,41	PO2B2-6
#
INN	B	5
#
99	c,b,f,b,F61B
29	f,RFCG5-2B4,99	TO2B5-4
22	FO4
00	b,c,s,FO3,LOB#29
#
INN	T	6
#
SH	4	15	20
SH	6	29	2
SH	9	2	27
6	c,LD82B
5	b,6	S3B,GO6-3,RBI1,SCORE#6
28	c,GO4-3
99	LD81B
16	c,LO4,LOB#99
#
INN	B	6
#
15	c,c,GO1-3
21	b,b,c,c,b,GO3u
2	b,b,b,b,BB
27	c,FO5,LOB#2
#
INN	T	7
#
SH	1	48	29
SH	3	22	2
SH	6	2	27
SH	8	32	15
SH	9	27	32
00	c,b,GO1-3
03	b,GO6-3
10	c,F91B
25	b,f,f,f,b,b,f,GO6-3,LOB#10
#
INN	B	7
#
32	c,s,FO3
48	c,LO4
7	b,s,c,f,b,GO6-3
#
GAMEENDS.
