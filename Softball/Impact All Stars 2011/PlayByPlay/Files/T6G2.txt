TEAMNAMEV	Cerritos Impact	RED
TEAMNAMEH	West Valley	BLACK
#	Pos.	Num.	Name
#
SLV	9	2	Rachel	L
SLV	8	27	Vicky
SLV	5	32	Justina
SLV	1	7	Haley
SLV	6	29	Kaitlyn
SLV	4	48	Nicole
SLV	2	99	Ashley
SLV	10	20	Briana
SLV	3	22	Courtney
SLV	7	15	Sabrina
SLV		21	Christabel
SLV		00	Luci
#
SLH	6	7	C. Coughlin
SLH	4	16	A. Morongell
SLH	1	11	J. Weiss
SLH	3	8	P. Green
SLH	5	18	H. Heath
SLH	7	2	S. Feldman
SLH	9	6	H. Polakov
SLH	8	17	A. Ramirez
SLH	2	5	L. O'Shea
SLH		27	V. Herrera
SLH		9	H. Gold
SLH		1	R. Russell
#
INN	T	1
#
2	b,HBP
27	f,f,s,K
32	b,2	S2B,f,s,b,bwp,2	>3B,c,K
7	b,b,c,b,c,b,BB
29	c,b,LO6,LOB#2#7
#
INN	B	1
#
7	b,c,b,c,f,b,G91B
16	b,7	S2B,f,f,b,b,b,BB
11	c,f,f,LD81B,RBI1,SCORE#7,16	>3B
5	c,b,GO1-3,RBI1,SCORE#16,11	>3B
18	c,b,s,c,K
2	c,f,c,K,LOB#11
#
INN	T	2
#
48	b,c,c,f,s,K
99	c,b,b,f,HBP
PR	99	21
20	b,21	S2B,b,b,21	S3B,b,BB
22	b,c,20	S2B,b,b,b,BB,MSG:Bases Loaded
2	c,b,b,b,b,BB,RBI1,SCORE#21,20	>3B;22	>2B
SH	1	11	1
SH	7	2	11
27	bwp,20	>HB;22	>3B;2	>2B,b,b,b,BB
32	b,c,LD71B,RBI1,SCORE#22,2	>3B;27	>2B
7	b,b,c,b,G91B,RBI2,SCORE#2#27,32	>3B;7	>2B
29	b,FO5
48	c,b,GO6-3,LOB#32#7
#
INN	B	2
#
6	c,c,b,b,c,K
17	b,c,G61B
5	c,f,b,s,K
7	b,b,b,b,BB
16	b,c,b,f,b,s,D3T3
11	c,c,b,b,b,LO6,LOB#17#7
#
INN	T	3
#
99	b,c,b,f,b,f,f,f,f,f,f,LD71B
PR	99	21
SH	1	11	6
SH	9	6	11
20	SACB1,21	>2B
22	b,f,b,GO4-3,21	>3B
2	c,b,s,s,K,LOB#21
#
INN	B	3
#
8	b,FO5
18	c,s,s,K
1	c,f,c,K
#
INN	T	4
#
27	b,b,b,c,b,BB
32	FO4
7	c,f,bwp,27	>2B,c,K
29	s,27	S3B,c,f,f,f,f,s,K,LOB#27
#
INN	B	4
#
6	c,GO5-3
9	c,b,f,GO1-3
5	c,b,b,f,b,c,K
#
INN	T	5
#
48	c,GO4-3
99	c,f,LO1
20	c,b,b,b,f,FO8
#
INN	B	5
#
7	b,b,F92B,7	>3BOTE9
16	c,bwp,7	>HB,f,b,b,G91B
11	b,b,b,c,b,BB,16	>2B
8	b,LO4,16	TO2B4-6,MSG:Double Play! #8 nearly thrown out at 1B for a Triple Play!
27	c,c,s,K,LOB#11
GAMEENDS.
