Impact All Stars - Cerritos Softball 2011 : Chino Hills Tournament Game 5 (Championships)

Sunday, June 19, 2011 5:45 PM

Visitors:     Cerritos Impact
Home:         West Grove

Losing Pitcher:  #22 Courtney
Winning Pitcher: #6  Dom. Espinoza

Final Score: West Grove 9, Cerritos Impact 2

Team Name                       1  2  3  4  5  6  7    R  H  E  L
------------------------------ -- -- -- -- -- -- --   -- -- -- --
West Grove                      6  0  1  1  0  1  0    9 15  2  6
Cerritos Impact                 0  1  1  0  0  0  0    2  6  2  6

Starting Lineups:
West Grove                              Cerritos Impact                         
Pos Num Name                            Pos Num Name                            
--- --- ------------------------------  --- --- ------------------------------  
C.  #22 Brooke Durand                   RF. #2  Rachel                        
1B. #1  Marissa Madrid                  CF. #27 Vicky                         
2B. #41 Jamie Gilbert                   3B. #32 Justina                       
P.  #6  Dom. Espinoza                   1B. #48 Nicole                        
3B. #5  Briana Covarrubias                  #7  Haley                         
SS. #28 Mia Palomino                        #20 Briana                        
LF. #99 Anna Navarro                    C.  #99 Ashley                        
CF. #16 Jessica Reeser                  SS. #29 Kaitlyn                       
RF. #00 Katelyn Christenson             P.  #22 Courtney                      
    #03 Natalie Reeser                  2B. #00 Luci                          
    #10 Cece Arzaga                     LF. #15 Sabrina                       
    #25 Mady Christensen                    #21 Christabel                    
Inning 1
	Top: West Grove at bat.
		Play 1: #22 Brooke Durand hits a fly ball towards CF #27 Vicky for a single (ccbb).
		Play 2: #1 Marissa Madrid at bat,  #22 Brooke Durand advances to 2B.
		Play 3: #1 Marissa Madrid hits a line drive towards CF #27 Vicky for a single (ffbb). 1 RBI, #22 Brooke Durand scores. #1 Marissa Madrid advances to 2B.
		Play 4: #41 Jamie Gilbert hits a grounder towards CF #27 Vicky for a single (ccf). 1 RBI, #1 Marissa Madrid scores.
		Play 5: #6 Dom. Espinoza at bat,  #41 Jamie Gilbert steals 2B.
		Play 6: #6 Dom. Espinoza walks (bbcbb).
		Play 7: #5 Briana Covarrubias hits a bunt towards Pitcher #22 Courtney for a single (). #41 Jamie Gilbert advances to 3B. #6 Dom. Espinoza advances to 2B. (Bases Loaded) 
		Play 8: #28 Mia Palomino hits a line drive towards CF #27 Vicky for a home run (cfbfb). 4 RBIs, #41 Jamie Gilbert, #6 Dom. Espinoza, #5 Briana Covarrubias, #28 Mia Palomino all score. (Grand Slam Home Run) 
		Play 9: #99 Anna Navarro hits a ground out to 2B #00 Luci and is thrown out at 1B #48 Nicole (b). Out 1.
		Play 10: #16 Jessica Reeser hits a ground out to Pitcher #22 Courtney and is thrown out at 1B #48 Nicole (c). Out 2.
		Play 11: #00 Katelyn Christenson hits a fly out to RF #2 Rachel (). Out 3.
		Summary: Runs: 6, Hits: 5, Errors: 0, LOB: 0.

	Bottom: Cerritos Impact at bat.
		Play 12: #2 Rachel strikes out looking (csc). Out 1.
		Play 13: #27 Vicky hits a line drive towards RF #00 Katelyn Christenson for a single (cc).
		Play 14: #32 Justina hits a fly ball towards CF #16 Jessica Reeser for a single (c). #27 Vicky advances to 3B. #32 Justina advances to 2B.
		Play 15: #48 Nicole reaches base on fielder's choice as grounder to Pitcher #6 Dom. Espinoza is thrown to home #22 Brooke Durand (c). #27 Vicky is thrown out at home from Pitcher #6 Dom. Espinoza to Catcher #22 Brooke Durand Out 2. #32 Justina advances to 3B. #48 Nicole advances to 2B.
		Play 16: #7 Haley hits a fly out to CF #16 Jessica Reeser (ccbb). Out 3. #32 Justina, #48 Nicole are left on base.
		Summary: Runs: 0, Hits: 2, Errors: 0, LOB: 2.

Inning 2
	Top: West Grove at bat.
		Play 17: #03 Natalie Reeser hits a fly out to RF #2 Rachel (). Out 1.
		Play 18: #10 Cece Arzaga strikes out looking (cfbc). Out 2.
		Play 19: #25 Mady Christensen hits a line drive towards SS #29 Kaitlyn for a single ().
		Play 20: #22 Brooke Durand hits a fly ball towards CF #27 Vicky for a single (). #25 Mady Christensen advances to 3B.
		Play 21: #1 Marissa Madrid at bat,  #22 Brooke Durand steals 2B.
		Play 22: #1 Marissa Madrid hits a ground out to 2B #00 Luci and is thrown out at 1B #48 Nicole (cc). Out 3. #25 Mady Christensen, #22 Brooke Durand are left on base.
		Summary: Runs: 0, Hits: 2, Errors: 0, LOB: 2.

	Bottom: Cerritos Impact at bat.
		Play 23: #20 Briana walks (fsbbbb).
		Play 24: #99 Ashley reaches base on fielder's choice as grounder to 3B #5 Briana Covarrubias is thrown to 2B #41 Jamie Gilbert (bccbf). #20 Briana is thrown out at 2B from 3B #5 Briana Covarrubias to 2B #41 Jamie Gilbert Out 1.
		Play 25: #29 Kaitlyn hits a line drive towards LF #99 Anna Navarro for a single (bff). #99 Ashley advances to 2B.
		Play 26: #22 Courtney reaches base on fielder's choice as grounder to SS #28 Mia Palomino is thrown to 2B #41 Jamie Gilbert (f). #99 Ashley is thrown out at 2B from SS #28 Mia Palomino to 2B #41 Jamie Gilbert Out 2. #29 Kaitlyn advances to home and scores. #22 Courtney advances to 3B on throwing error by SS #28 Mia Palomino .
		Play 27: #00 Luci hits a fly out to Pitcher #6 Dom. Espinoza (b). Out 3. #22 Courtney is left on base.
		Summary: Runs: 1, Hits: 1, Errors: 1, LOB: 1.

Inning 3
	Top: West Grove at bat.
		Substitution: Cerritos Impact #48 Nicole to Pitcher for #22 Courtney 
		Substitution: Cerritos Impact #7 Haley to 1B for #48 Nicole 
		Play 28: #41 Jamie Gilbert hits a fly out to 2B #00 Luci (bbf). Out 1.
		Play 29: #6 Dom. Espinoza reaches base on error as grounder to SS #29 Kaitlyn is mishandled. (c).
		Play 30: #5 Briana Covarrubias at bat,  #6 Dom. Espinoza steals 2B. #6 Dom. Espinoza advances to 3B on throwing error by Catcher #99 Ashley .
		Play 31: #5 Briana Covarrubias hits a line drive towards CF #27 Vicky for a single (b). 1 RBI, #6 Dom. Espinoza scores.
		Play 32: #28 Mia Palomino at bat,  #5 Briana Covarrubias steals 2B.
		Play 33: #28 Mia Palomino at bat,  #5 Briana Covarrubias advances to 3B.
		Play 34: #28 Mia Palomino hits a ground out to Pitcher #48 Nicole and is thrown out at 1B #7 Haley (cbb). Out 2.
		Play 35: #99 Anna Navarro hits a ground out to Pitcher #48 Nicole and is thrown out at 1B #7 Haley (b). Out 3. #5 Briana Covarrubias is left on base.
		Summary: Runs: 1, Hits: 1, Errors: 2, LOB: 1.

	Bottom: Cerritos Impact at bat.
		Play 36: #15 Sabrina reaches base on error as grounder to SS #28 Mia Palomino is mishandled. (bbcf).
		Play 37: #21 Christabel hits a ground out to 2B #41 Jamie Gilbert and is thrown out at 1B #1 Marissa Madrid (cbff). Out 1. #15 Sabrina advances to 2B.
		Play 38: #2 Rachel hits a ground out to 2B #41 Jamie Gilbert and is thrown out at 1B #1 Marissa Madrid (cb). Out 2. #15 Sabrina advances to 3B.
		Play 39: #27 Vicky hits a grounder towards CF #16 Jessica Reeser for a single (b). 1 RBI, #15 Sabrina scores.
		Play 40: #32 Justina at bat,  #27 Vicky is picked off at 2B on throw from Catcher #22 Brooke Durand to SS #28 Mia Palomino Out 3.
		Summary: Runs: 1, Hits: 1, Errors: 1, LOB: 0.

Inning 4
	Top: West Grove at bat.
		Substitution: Cerritos Impact #22 Courtney to 1B for #7 Haley 
		Play 41: #16 Jessica Reeser hits a line drive towards LF #15 Sabrina for a single (c).
		Play 42: #00 Katelyn Christenson hits a ground out to Pitcher #48 Nicole and is thrown out at 1B #22 Courtney (sf). Out 1. #16 Jessica Reeser advances to 2B.
		Play 43: #03 Natalie Reeser hits a grounder towards CF #27 Vicky for a single (bfs). #16 Jessica Reeser advances to 3B. #16 Jessica Reeser scores. #03 Natalie Reeser advances to 2B.
		Play 44: hits a ground out to Pitcher #48 Nicole and is thrown out at 1B #22 Courtney (). Out 2. #03 Natalie Reeser advances to 3B.
		Play 45: #25 Mady Christensen hits a ground out to Pitcher #48 Nicole and is thrown out at 1B #22 Courtney (b). Out 3. #03 Natalie Reeser is left on base.
		Summary: Runs: 1, Hits: 2, Errors: 0, LOB: 1.

	Bottom: Cerritos Impact at bat.
		Play 46: #32 Justina hits a ground out to 3B #5 Briana Covarrubias and is thrown out at 1B #1 Marissa Madrid (fbbs). Out 1.
		Play 47: #48 Nicole hits a ground out to Pitcher #6 Dom. Espinoza and is thrown out at 1B #1 Marissa Madrid (). Out 2.
		Play 48: #7 Haley hits a grounder towards SS #28 Mia Palomino for a single (bb).
		Play 49: #20 Briana hits a ground out to 2B #41 Jamie Gilbert and is thrown out at 1B #1 Marissa Madrid (). Out 3. #7 Haley is left on base.
		Summary: Runs: 0, Hits: 1, Errors: 0, LOB: 1.

Inning 5
	Top: West Grove at bat.
		Substitution: Cerritos Impact #15 Sabrina to 2B for #00 Luci 
		Substitution: Cerritos Impact #7 Haley to 3B for #32 Justina 
		Substitution: Cerritos Impact #21 Christabel to LF for #15 Sabrina 
		Substitution: Cerritos Impact #32 Justina to CF for #27 Vicky 
		Play 50: #22 Brooke Durand hits a line drive towards CF #32 Justina for a single (b).
		Play 51: #1 Marissa Madrid at bat,  #22 Brooke Durand is picked off at 2B on throw from Catcher #99 Ashley to SS #29 Kaitlyn Out 1.
		Play 52: #1 Marissa Madrid hits a ground out to Pitcher #48 Nicole and is thrown out at 1B #22 Courtney (b). Out 2.
		Play 53: #41 Jamie Gilbert hits a grounder towards CF #32 Justina for a single (cbcb).
		Play 54: #6 Dom. Espinoza at bat,  #41 Jamie Gilbert is picked off at 2B on throw from Catcher #99 Ashley to SS #29 Kaitlyn Out 3.
		Summary: Runs: 0, Hits: 2, Errors: 0, LOB: 0.

	Bottom: Cerritos Impact at bat.
		Play 55: #99 Ashley hits a fly ball towards SS #28 Mia Palomino for a single (cbfb).
		Play 56: #29 Kaitlyn reaches base on fielder's choice as grounder to 3B #5 Briana Covarrubias is thrown to 2B #41 Jamie Gilbert (f). #99 Ashley is thrown out at 2B from 3B #5 Briana Covarrubias to 2B #41 Jamie Gilbert Out 1.
		Play 57: #22 Courtney hits a fly out to 2B #41 Jamie Gilbert (). Out 2.
		Play 58: #00 Luci hits a fly out to 1B #1 Marissa Madrid (bcs). Out 3. #29 Kaitlyn is left on base.
		Summary: Runs: 0, Hits: 1, Errors: 0, LOB: 1.

Inning 6
	Top: West Grove at bat.
		Substitution: Cerritos Impact #20 Briana to 2B for #15 Sabrina 
		Substitution: Cerritos Impact #2 Rachel to SS for #29 Kaitlyn 
		Substitution: Cerritos Impact #27 Vicky to RF for #2 Rachel 
		Play 59: #6 Dom. Espinoza hits a line drive towards CF #32 Justina for a double (c).
		Play 60: #5 Briana Covarrubias at bat,  #6 Dom. Espinoza steals 3B.
		Play 61: #5 Briana Covarrubias hits a ground out to SS #2 Rachel and is thrown out at 1B #22 Courtney (b). Out 1. 1 RBI, #6 Dom. Espinoza scores.
		Play 62: #28 Mia Palomino hits a ground out to 2B #20 Briana and is thrown out at 1B #22 Courtney (c). Out 2.
		Play 63: #99 Anna Navarro hits a line drive towards CF #32 Justina for a single ().
		Play 64: #16 Jessica Reeser hits a line drive out to 2B #20 Briana (c). Out 3. #99 Anna Navarro is left on base.
		Summary: Runs: 1, Hits: 2, Errors: 0, LOB: 1.

	Bottom: Cerritos Impact at bat.
		Play 65: #15 Sabrina hits a ground out to Pitcher #6 Dom. Espinoza and is thrown out at 1B #1 Marissa Madrid (cc). Out 1.
		Play 66: #21 Christabel hits a ground out to 1B #1 Marissa Madrid (bbccb). Out 2.
		Play 67: #2 Rachel walks (bbbb).
		Play 68: #27 Vicky hits a fly out to 3B #5 Briana Covarrubias (c). Out 3. #2 Rachel is left on base.
		Summary: Runs: 0, Hits: 0, Errors: 0, LOB: 1.

Inning 7
	Top: West Grove at bat.
		Substitution: Cerritos Impact #29 Kaitlyn to Pitcher for #48 Nicole 
		Substitution: Cerritos Impact #2 Rachel to 1B for #22 Courtney 
		Substitution: Cerritos Impact #27 Vicky to SS for #2 Rachel 
		Substitution: Cerritos Impact #15 Sabrina to CF for #32 Justina 
		Substitution: Cerritos Impact #32 Justina to RF for #27 Vicky 
		Play 69: #00 Katelyn Christenson hits a ground out to Pitcher #29 Kaitlyn and is thrown out at 1B #2 Rachel (cb). Out 1.
		Play 70: #03 Natalie Reeser hits a ground out to SS #27 Vicky and is thrown out at 1B #2 Rachel (b). Out 2.
		Play 71: #10 Cece Arzaga hits a fly ball towards RF #32 Justina for a single (c).
		Play 72: #25 Mady Christensen hits a ground out to SS #27 Vicky and is thrown out at 1B #2 Rachel (bfffbbf). Out 3. #10 Cece Arzaga is left on base.
		Summary: Runs: 0, Hits: 1, Errors: 0, LOB: 1.

	Bottom: Cerritos Impact at bat.
		Play 73: #32 Justina hits a fly out to 1B #1 Marissa Madrid (cs). Out 1.
		Play 74: #48 Nicole hits a line drive out to 2B #41 Jamie Gilbert (c). Out 2.
		Play 75: #7 Haley hits a ground out to SS #28 Mia Palomino and is thrown out at 1B #1 Marissa Madrid (bscfb). Out 3.
		Summary: Runs: 0, Hits: 0, Errors: 0, LOB: 0.

Game Ends.
