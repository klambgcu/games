TEAMNAMEV	Cerritos Impact	RED
TEAMNAMEH	Twin Falls Idaho Ice	CYAN
#	Pos.	Num.	Name
#
SLV	9	2	Rachel	L
SLV	8	27	Vicky
SLV	10	20	Briana
SLV	2	99	Ashley
SLV	1	48	Nicole
SLV	5	32	Justina
SLV	6	29	Kaitlyn
SLV	4	7	Haley
SLV	3	22	Courtney
SLV	7	15	Sabrina
SLV		21	Christabel
SLV		00	Luci
#
SLH	8	23	K. Watts
SLH	5	3	H. Ackerman
SLH	1	7	A. Williams
SLH	3	11	M. Hinojos
SLH	4	5	L. Parker
SLH		15	K. Baumert
SLH	2	9	T. Johnson
SLH	6	10	S. Fleming
SLH	9	27	M. Ford
SLH	7	13	B. Johnson
SLH		30	C. Cooper
SLH		34	S. Cooper
#
INN	T	1
#
2	c,s,b,s,K
27	c,c,ROEGM4
20	bwp,27	>2B,GO1-3,27	>3B
99	b,b,ROEGT5-1B3,SCORE#27
PR	99	21
48	c,s,c,K,LOB#21
#
INN	B	1
#
23	b,b,f,b,f,f,BB
3	G11B,23	>2B
7	c,f,b,LD81B,23	>3B;3	>2B,MSG:Bases Loaded
11	c,f,b,b,s,K
5	c,b,b,f,b,G91B,RBI2,SCORE#23#3,7	>3B
15	c,5	S2B,LD71B,RBI1,SCORE#7,5	>3B
9	f,LD81B,15	TO2B8-4,RBI1,SCORE#5
PR	9	34
10	f,b,s,b,FO4,LOB#34
#
INN	T	2
#
32	b,b,b,FO6
29	b,b,c,s,f,f,f,GO5-3
7	b,c,GO5-3
#
INN	B	2
#
SV	1	48	7
SV	4	7	48
27	c,b,f,GO1-3
23	f,b,f,f,ROEGM5
3	RFCG6-2B4,23	TO2B6-4
7	b,b,f,b,s,ROEFM4,3	>3B,7	TO1B3-3,LOB#3,MSG:Hesitation on #7
#
INN	T	3
#
22	c,LO4
2	b,b,b,b,BB
27	c,b,b,b,f,b,BB,2	S3B;27	S2B
20	b,GO4-3,RBI1,SCORE#2,27	>3B
99	b,b,b,c,b,BB
PR	99	21
48	c,21	S2B,c,c,K,LOB#27#21
#
INN	B	3
#
11	c,b,b,b,s,b,BB
5	c,b,FO1,11	TO1B1-3,MSG:Double Play
15	b,b,b,c,b,BB
9	c,b,c,b,FO7,LOB#15
#
INN	T	4
#
32	GO1-3
29	b,ROEFM9
7	b,b,f,FO8
22	GO4-3,LOB#29
#
INN	B	4
#
10	b,s,c,s,K
27	ROEGM2
23	c,27	S2B,b,27	S3B,b,b,c,f,f,s,K
3	FO6,LOB#27
#
INN	T	5
#
2	b,b,c,f,b,c,K
27	b,c,ROEGT5-1B3,27	>2BOTE5
20	c,c,f,bp,27	>3B,f,f,b,f,b,b,BB
99	c,20	S2B,c,GO6-3,RBI1,SCORE#27,20	>3B
48	b,b,LO4,LOB#20
#
INN	B	5
#
7	b,b,b,c,f,c,K
11	s,b,b,s,s,K
5	c,b,b,b,s,f,ROEGT5-1B3
15	c,b,LD91B,5	>2B
PR	15	13
9	b,b,c,FO7,LOB#5#13
#
INN	T	6
#
32	s,b,s,LO6
29	b,s,b,f,GO6-3
7	b,FO8
#
INN	B	6
#
10	b,s,s,f,s,K
27	b,s,f,FO3
23	b,b,f,b,b,BB
3	ROEFM4,23	>3B;3	>2B
7	LD82B,RBI2,SCORE#23#3
34	b,f,b,f,b,MSG:Time Limit Reached
GAMEENDS.
