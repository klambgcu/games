TEAMNAMEV	Cerritos Impact	RED
TEAMNAMEH	Chino	CYAN
#	Pos.	Num.	Name
#
SLV	9	2	Rachel	L
SLV	8	27	Vicky
SLV	5	32	Justina
SLV		48	Nicole
SLV	1	7	Haley
SLV	10	20	Briana
SLV	2	99	Ashley
SLV	6	29	Kaitlyn
SLV	3	22	Courtney
SLV	4	00	Luci
SLV	7	15	Sabrina
SLV		21	Christabel
#
SLH	3	99	Alia Jimenez
SLH	4	21	Hailey Olmedo
SLH	5	9	Rachelle Huerta
SLH	1	33	Ashley Romero
SLH	6	14	Miranda	Gardy
SLH	7	4	Victoria Diaz
SLH	8	00	Tori Mendoza
SLH	2	11	Evelyn Lopez
SLH	9	5	Saige Lopez
SLH		17	Brandy Lara
SLH		01	Victoriana Avila
SLH		95	Selena Villabescencio
SLH		78	Ruth Schlisse
#
INN	T	1
#
2	b,b,c,f,b,F73B
27	c,SACF8,RBI1,SCORE#2
32	G71B
48	LO9,32	>2BOTE9
7	b,c,b,FO3,LOB#32
#
INN	B	1
#
99	b,c,f,GO1-3
21	b,c,f,bw,G81B
9	b,f,LD92B,RBI1,SCORE#21
33	c,b,LD72B,RBI1,SCORE#9
14	b,b,s,b,c,G71B,33	>3B;14	>2B
4	LD81B,RBI2,SCORE#33#14,4	>2B
00	c,c,4	>3B,f,LO1
11	c,f,s,K,LOB#4
#
INN	T	2
#
20	b,s,c,b,b,f,b,BB
99	b,b,b,c,b,BB,20	>2B
29	FO3
22	ROEGT4-2B6,99	>3B;22	>2B,MSG:Bases Loaded
00	b,FO4
PR	99	00
15	c,RFCG4-2B6,00	TO2B4-6,LOB#15#22#20
#
INN	B	2
#
5	b,c,s,b,s,K
17	c,b,c,b,bw,GO5-3
01	GO4-3
#
INN	T	3
#
21	b,c,s,f,LD81B
2	b,21	S2B;21	PO3B8-5,b,FO8
27	b,b,f,LO7
#
INN	B	3
#
95	b,c,f,f,GO6-3
78	b,b,c,b,f,f,b,BB
99	RFCG4-2B4,78	TO2B4-4,99	TO1B4-3,MSG:Double Play!
#
INN	T	4
#
32	b,c,LD81B
48	b,f,b,32	S2B,LD81B,32	>3B
7	b,48	S2B,LD71B,RBI1,SCORE#32,48	>3B
20	b,7	S2B,b,LD81B,RBI1,SCORE#48,7	>3B
PR	7	27
99	b,20	S2B,27	>HBOTE2,b,f,b,F71B,20	>3B
PR	99	2
29	c,2	S2B,c,LO8
22	RFCG5-HB2,20	TOHB5-2,2	>3B;22	>2B
00	c,c,b,GO1-3,LOB#2#22
#
INN	B	4
#
21	c,c,c,K
5	b,c,FO2
33	c,FO5
#
INN	T	5
#
15	c,GO6-3
21	f,f,f,b,b,ROEGM6
2	c,b,G91B,21	>2B
27	b,f,f,s,K
32	b,b,G71B,21	>3B;2	>2B
48	b,b,c,21	POHB1-2,LOB#2#32
#
INN	B	5
#
14	LD81B,14	>3BOCE8
4	f,b,F81B,RBI1,SCORE#14
00	c,GO1-3,4	>2B
11	b,f,b,FO9
5	GO3u,LOB#00
#
INN	T	6
#
48	c,ROEGM5
7	s,48	S2B,G91B,48	>3B
20	RFCG6-HB2,RBI1,SCORE#48,7	>2B
99	b,7	PO3B2-5,b,20	S2B,FO7,LOB#20,MSG:Briana called out after entering dugout. Out 3.
#
INN	B	6
#
17	c,c,s,K
01	b,b,b,c,c,c,K
95	c,GO3u
#
GAMEENDS.
