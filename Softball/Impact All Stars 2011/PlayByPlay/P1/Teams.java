import java.util.ArrayList;
import java.util.List;

public class Teams
{
	private Team teamVisitor;
	private Team teamHome;

	///////////////////////////////////////////////////
	// CONSTRUCTORS
	///////////////////////////////////////////////////
	Teams()
	{
	}

	Teams(Team visitor, Team home)
	{
		teamVisitor = visitor;
		teamHome = home;
	}

	///////////////////////////////////////////////////
	// ACCESSORS
	///////////////////////////////////////////////////
	public String getTeamName(int whichTeam)
	{
		if ( whichTeam == PlayByPlay.VISITOR )
			return teamVisitor.getName();
		else
			return teamHome.getName();
	}

	public Player getPlayer(int whichTeam, int pos)
	{
		if ( whichTeam == PlayByPlay.VISITOR )
			return teamVisitor.getPlayer(pos);
		else
			return teamHome.getPlayer(pos);
	}

	public Player getPlayer(int whichTeam, String num)
	{
		if ( whichTeam == PlayByPlay.VISITOR )
			return teamVisitor.getPlayer(num);
		else
			return teamHome.getPlayer(num);
	}

	///////////////////////////////////////////////////
	// MUTATORS
	///////////////////////////////////////////////////
	public void addPlayer(int whichTeam, Player p)
	{
		if ( whichTeam == PlayByPlay.VISITOR )
			teamVisitor.addPlayer( p );
		else
			teamHome.addPlayer( p );
	}

	public void setTeam(int whichTeam, Team team)
	{
		if ( whichTeam == PlayByPlay.VISITOR )
			setVisitorTeam(team);
		else
			setHomeTeam(team);
	}

	public void setVisitorTeam(Team team)
	{
		teamVisitor = team;
	}

	public void setHomeTeam(Team team)
	{
		teamHome = team;
	}

	///////////////////////////////////////////////////
	// UTILITIES
	///////////////////////////////////////////////////
	public String substituteRunner(int whichTeam, String numOut, String numIn)
	{
		Player po = getPlayer( whichTeam, numOut );
		Player pi = getPlayer( whichTeam, numIn );

		return "\t\tSubstitution: %s" + pi.getNumberName() + "to pinch run for" + po.getNumberName() + "\r\n";
	}

	public String substitutePlayer(int whichTeam, String pos, String numOut, String numIn)
	{
		Player po = getPlayer( whichTeam, numOut );
		Player pi = getPlayer( whichTeam, numIn );

		pi.setPosition( pos );
		if (po.getPosition() == Integer.valueOf(pos))
			po.setPosition( "0" ); // Bench

		return "\t\tSubstitution: %s" + pi.getNumberName() + "to %s for" + po.getNumberName() + "\r\n";
	}

	public void printStartingLineUps()
	{
		System.out.println("Starting Lineups:");
		System.out.printf("%-40s%-40s\r\n", getTeamName(PlayByPlay.VISITOR), getTeamName(PlayByPlay.HOME));
		System.out.printf("%-40s%1$-40s\r\n","Pos Num Name");
		System.out.printf("%-40s%1$-40s\r\n","--- --- ------------------------------");

		List<Player> vRoster = teamVisitor.getRoster();
		List<Player> hRoster = teamHome.getRoster();

		int vs = vRoster.size();
		int hs = hRoster.size();
		int ss = Math.min(vs,hs);

		for (int i=0; i < ss; i++)
		{
			Player vp = vRoster.get(i);
			Player hp = hRoster.get(i);
			System.out.printf("%-3s #%-2s %-30s  %-3s #%-2s %-30s\r\n",
							  PlayByPlay.positionAbbr[vp.getPosition()],
							  vp.getNumber(),
							  vp.getName(),
							  PlayByPlay.positionAbbr[hp.getPosition()],
							  hp.getNumber(),
							  hp.getName());
		}

		if (vs > hs)
		{
			for (int i = ss; i < vs; i++)
			{
				Player vp = vRoster.get(i);
				System.out.printf("%-3s #%-2s %-30s\r\n",
								  PlayByPlay.positionAbbr[vp.getPosition()],
								  vp.getNumber(),
								  vp.getName());
			}
		}
		else if (hs > vs)
		{
			for (int i = ss; i < hs; i++)
			{
				Player hp = hRoster.get(i);
				System.out.printf("%40s%-3s #%-2s %-30s\r\n",
								  "",
								  PlayByPlay.positionAbbr[hp.getPosition()],
								  hp.getNumber(),
								  hp.getName());
			}
		}

		System.out.println();
	}
} // End of class Teams
