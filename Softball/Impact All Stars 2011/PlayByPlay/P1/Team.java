import java.util.ArrayList;
import java.util.List;

public class Team
{
	private String name = "";
	private int color = 0;
	private ArrayList<Player> roster = new ArrayList<Player>();
	private Player nullPlayer = new Player("","","");
	private boolean showPix = false;

	///////////////////////////////////////////////////
	// CONSTRUCTORS
	///////////////////////////////////////////////////
	Team()
	{
	}

	Team(String name, String color, boolean showPix)
	{
		this.name    = name;
		this.showPix = showPix;
		// TODO: Determine color reference
	}

	///////////////////////////////////////////////////
	// ACCESSORS
	///////////////////////////////////////////////////
	public String getName()
	{
		return name;
	}

	public Player getPlayer(int pos)
	{
		Player result = nullPlayer;

		for (Player p : roster)
		{
			if (p.getPosition() == pos)
			{
				result = p;
				break;
			}
		}
		return result;
	}

	public Player getPlayer(String num)
	{
		Player result = nullPlayer;

		for (Player p : roster)
		{
			if ( num.equals(p.getNumber()) )
			{
				result = p;
				break;
			}
		}
		return result;
	}

	public List<Player> getRoster()
	{
		return roster;
	}

	///////////////////////////////////////////////////
	// MUTATORS
	///////////////////////////////////////////////////
	public void setName(String name)
	{
		this.name = name;
	}

	public void addPlayer(Player p)
	{
		roster.add( p );
	}

	///////////////////////////////////////////////////
	// UTILITIES
	///////////////////////////////////////////////////
	public String substituteRunner(String numOut, String numIn)
	{
		Player po = getPlayer( numOut );
		Player pi = getPlayer( numIn );

		return "\t\tSubstitution: %s" + pi.getNumberName() + "to pinch run for" + po.getNumberName() + "\r\n";
	}

	public String substitutePlayer(String pos, String numOut, String numIn)
	{
		Player po = getPlayer( numOut );
		Player pi = getPlayer( numIn );

		pi.setPosition( pos );
		if (po.getPosition() == Integer.valueOf(pos))
			po.setPosition( "0" ); // Bench

		return "\t\tSubstitution: %s" + pi.getNumberName() + "to %s for" + po.getNumberName() + "\r\n";
	}



}

