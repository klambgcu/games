public class Player
{
	private int    position;
	private String number;
	private String name;

	///////////////////////////////////////////////////
	// CONSTRUCTORS
	///////////////////////////////////////////////////
	Player( String pos, String num, String name)
	{
		setPosition( pos );
		setNumber( num );
		setName( name );
	}

	Player( Player p )
	{
		setPosition( String.valueOf( p.getPosition() ) );
		setNumber( p.getNumber() );
		setName( p.getName() );
	}
	///////////////////////////////////////////////////
	// ACCESSORS
	///////////////////////////////////////////////////
	public int getPosition()
	{
		return position;
	}

	public String getNumber()
	{
		return number;
	}

	public String getName()
	{
		return name;
	}

	public String getNumberName()
	{
		String result;
		if ( "".equals( getNumber().trim() ) )
			result = " ";
		else
		{
			if ( "".equals(getName().trim()) )
				result = String.format(" #%s ", getNumber().trim());
			else
				result = String.format(" #%s %s ", getNumber().trim(), getName().trim() );
		}
		return result;
	}
	///////////////////////////////////////////////////
	// MUTATORS
	///////////////////////////////////////////////////
	public void setPosition( String p )
	{
		if ( (p == null) || ( "".equals(p) ) )
			this.position = 0;
		else
			this.position = Integer.parseInt( p );
	}

	public void setNumber( String n )
	{
		this.number = n;
	}

	public void setName( String n )
	{
		this.name = n;
	}

	///////////////////////////////////////////////////
	// UTILITIES
	///////////////////////////////////////////////////
	@Override
	public String toString()
	{
		return String.format("Pos[%d], Number[%s], Name[%s]\r\n", this.getPosition(), this.getNumber(), this.getName() );
	}
} // End of class Player
