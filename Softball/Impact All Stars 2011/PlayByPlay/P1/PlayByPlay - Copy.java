import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import javax.swing.filechooser.*;
import javax.swing.JFileChooser;

public class PlayByPlay
{
	private static final String[] positionName = {"home - Catcher", "Pitcher", "Catcher", "1B", "2B", "3B", "SS", "LF", "CF","RF"};
	private static final String[] positionAbbr = {"  ", "P. ", "C. ", "1B.", "2B.", "3B.", "SS.", "LF.", "CF.","RF."};
	private static final HashMap<String, String> hitType  = new HashMap<String, String>();
	private static int VISITOR = 0;
	private static int HOME    = 1;

	private static int RUNS = 0;
	private static int HITS = 1;
	private static int ERRS = 2;
	private static int LOBS = 3;

	private Teams teams = new Teams();
	private int summary[]   = {0,0,0,0};
	private int sumTots[][] = {{0,0,0,0},{0,0,0,0}};

	private String scores[][] = new String[2][15];

	private int playNumber = 0;
	private int outNumber  = 0;
	private int teamAtBat  = VISITOR;
	private int inning     = 0;


	private StringBuffer pitches = new StringBuffer();

	public PlayByPlay()
	{
		hitType.put("1B",  "single");
		hitType.put("2B",  "double");
		hitType.put("3B",  "triple");
		hitType.put("BB",  "walks");
		hitType.put("B",   "bunt");
		hitType.put("BUNT","bunt");
		hitType.put("F",   "fly ball");
		hitType.put("FO",  "fly out");
		hitType.put("G",   "grounder");
		hitType.put("GO",  "ground out");
		hitType.put("GS",  "grand slam");
		hitType.put("HB",  "home run");
		hitType.put("HR",  "home run");
		hitType.put("L",   "line drive");
		hitType.put("LD",  "line drive");
		hitType.put("LO", "line drive out");
		hitType.put("SACB","sacrifice bunt");
		hitType.put("SACF","sacrifice fly out");

		for (int r=0; r < 2; r++)
			for (int c=0; c < 15; c++)
				scores[r][c] = " x";
	}

	public void run()
	{
		// TODO: Create and call a routine to use JFileChooser()
		try
		{
			//File text = new File("test.txt");
			//File text = new File("T3.txt");
			//processFile(text);

        	JFileChooser fileChooser = new JFileChooser(".");
        	fileChooser.setFileSelectionMode( JFileChooser.FILES_ONLY );
        	fileChooser.setAcceptAllFileFilterUsed(true);

        	int result = fileChooser.showOpenDialog( null );

        	if ( result != JFileChooser.CANCEL_OPTION )
        	{
        	    File fileName = fileChooser.getSelectedFile();
        	    processFile(fileName);
        	}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void processFile(File file)
	{
		BufferedReader reader = null;

		try
		{
			reader = new BufferedReader(new FileReader(file));
			String text = null;

			// Repeat until all lines are read
			while ((text = reader.readLine()) != null)
			{
				if (text.startsWith("#") || text.length() == 0)
				{
					continue; // Ignore comment and empty lines
				}
				else if (Character.isDigit(text.charAt(0)) || text.startsWith("?"))
				{
					processAtBat(text);
				}
				else
				{
					processCommands(text);
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (reader != null)
				{
					reader.close();
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void processCommands(String command)
	{
		//System.out.printf("Processing a command.\t%s\r\n", command);
		String[] tokens = command.split("\t"); // Commands are tab delimited.
		String token = tokens[0];

		if ("TEAMNAMEH".equals(token))
		{
			teams.setTeamName( HOME, tokens[1] );
		}
		else if ("TEAMNAMEV".equals(token))
		{
			teams.setTeamName( VISITOR, tokens[1] );
		}
		else if ("SLH".equals(token))
		{
			Player p = new Player( tokens[1], tokens[2], tokens[3] );
			teams.addPlayer( HOME, p );
		}
		else if ("SLV".equals(token))
		{
			Player p = new Player( tokens[1], tokens[2], tokens[3] );
			teams.addPlayer( VISITOR, p );
		}
		else if ("SH".equals(token))
		{
			String format = teams.substitutePlayer( HOME, tokens[1], tokens[2], tokens[3] );
			System.out.printf( format, teams.getTeamName( HOME ), positionName[ Integer.parseInt(tokens[1]) ] );
		}
		else if ("SV".equals(token))
		{
			String format = teams.substitutePlayer( VISITOR, tokens[1], tokens[2], tokens[3] );
			System.out.printf( format, teams.getTeamName( VISITOR ), positionName[ Integer.parseInt(tokens[1]) ] );
		}
		else if ("PR".equals(token))
		{
			String format = teams.substituteRunner( teamAtBat, tokens[1], tokens[2] );
			System.out.printf( format, teams.getTeamName( teamAtBat ) );
		}
		else if ("INN".equals(token))
		{
			if ("T".equals(tokens[1]))
			{
				outNumber = 0;
				int inn = Integer.valueOf(tokens[2].trim());
				if (inn != 1)
				{
					processSummaryTotals();
				}
				else
				{
					teams.printStartingLineUps();
				}

				System.out.printf("Inning %d\r\n", inn);
				System.out.printf("\tTop: %s at bat.\r\n", teams.getTeamName( VISITOR ));
				inning = inn;
				teamAtBat = VISITOR;
			}
			else
			{
				outNumber = 0;
				processSummaryTotals();
				System.out.printf("\tBottom: %s at bat.\r\n", teams.getTeamName( HOME ));
				teamAtBat = HOME;
			}
		}
		else if ("GAMEENDS.".equals(token))
		{
			processSummaryTotals();
			System.out.println("Game Ends.");
			printBoxScores();
		}
		else if ("".equals(token))
		{
		}
		else if ("".equals(token))
		{
		}
		else if ("".equals(token))
		{
		}
		else if ("".equals(token))
		{
		}
		else if ("".equals(token))
		{
		}
		else
		{
			System.out.println("Unknown Command: " + command);
		}
	}

	private void processSummaryTotals()
	{
		System.out.printf("\t\tSummary: Runs:%2d, Hits:%2d, Errors:%2d, LOB:%2d.\r\n\r\n",
						  summary[RUNS],
						  summary[HITS],
						  summary[ERRS],
						  summary[LOBS]);

		sumTots[teamAtBat  ][RUNS] += summary[RUNS];
		sumTots[teamAtBat  ][HITS] += summary[HITS];
		sumTots[1-teamAtBat][ERRS] += summary[ERRS];
		sumTots[teamAtBat  ][LOBS] += summary[LOBS];

		scores[teamAtBat][inning] = String.format("%d", summary[RUNS]);
		summary[RUNS] = summary[HITS] = summary[ERRS] = summary[LOBS] = 0;
	}

	private void printBoxScores()
	{
		System.out.printf("\r\nFinal Score: %s %d, %s %d\r\n\r\n",
						  teams.getTeamName(VISITOR),
						  sumTots[VISITOR][RUNS],
						  teams.getTeamName(HOME),
						  sumTots[HOME][RUNS]);

		System.out.println("Team Name                       1  2  3  4  5  6  7    R  H  E  L");
		System.out.println("------------------------------ -- -- -- -- -- -- --   -- -- -- --");

		System.out.printf("%-30s", teams.getTeamName(VISITOR));
		for (int i = 1; i <= 7; i++)
			System.out.printf("%3s", scores[VISITOR][i]);
		System.out.print("  ");
		for (int i = 0; i < 4; i++)
			System.out.printf("%3d", sumTots[VISITOR][i]);
		System.out.println();

		System.out.printf("%-30s", teams.getTeamName(HOME));
		for (int i = 1; i <= 7; i++)
			System.out.printf("%3s", scores[HOME][i]);
		System.out.print("  ");
		for (int i = 0; i < 4; i++)
			System.out.printf("%3d", sumTots[HOME][i]);
		System.out.printf("\r\n\r\n");
	}

	private void processAtBat(String atbat)
	{
		boolean atBatFinished = false;
		pitches.delete(0, pitches.length());

		 // Atbats are comma delimited
		String[] tokens = atbat.split(",");

		playNumber++;
		String playerNumber = tokens[0].split("\t")[0];

		// Remove number and reassign to beginning of token
		tokens[0] = tokens[0].split("\t")[1];
		System.out.printf("\t\tPlay %d:%s",
						  playNumber,
						  teams.getPlayer(teamAtBat, playerNumber).getNumberName() );

		for (String token : tokens)
		{
			// Check for embedded running events.
			if (token.contains("\t"))
			{
				if (! atBatFinished)
					System.out.print("at bat, ");

				processRunningEvent(token);

				if ((! atBatFinished) && (outNumber < 3))
				{
					playNumber++;
					System.out.printf("\r\n\t\tPlay %d:%s",
									  playNumber,
									  teams.getPlayer(teamAtBat, playerNumber).getNumberName() );
				}
			}
			else if ("b".equals(token) || "bp".equals(token) || "bw".equals(token) || "bwp".equals(token))
			{
				pitches.append("b"); // handle balls (wild, passed balls)
			}
			else if ("s".equals(token) || "sp".equals(token) || "sw".equals(token) || "swp".equals(token))
			{
				pitches.append("s"); // handle swinging strikes (wild, passed balls)
			}
			else if ("c".equals(token) || "cp".equals(token))
			{
				pitches.append("c"); // handle called strikes (passed balls, does not make sense for wild)
			}
			else if ("f".equals(token))
			{
				pitches.append("f"); // handle foul balls
			}
			else if ("K".equals(token))
			{
				outNumber++;
				atBatFinished = true;
				String type = (pitches.toString().endsWith("c")) ? "looking" : "swinging";
				System.out.printf("strikes out %s (%s). Out %d.", type, pitches.toString(), outNumber);
			}
			else if ("BB".equals(token))
			{
				atBatFinished = true;
				System.out.printf("walks (%s).", pitches.toString());
			}
			else if ("HBP".equals(token))
			{
				atBatFinished = true;
				pitches.append("b");
				System.out.printf("is hit by the pitch (%s).", pitches.toString());
			}
			else if (token.startsWith("D3R"))
			{
				outNumber++;
				atBatFinished = true;
				String msg = "";
				if (token.contains("-"))
				{
					int pos1 = Integer.valueOf(token.substring(3,4));
					int pos2 = Integer.valueOf(token.substring(5,6));
					msg = String.format("is thrown out at %s%sby %s%safter a dropped 3rd strike (%s). Out %d.",
										positionName[pos2],
										teams.getPlayer(1-teamAtBat, pos2).getNumberName(),
										positionName[pos1],
										teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
										pitches.toString(),
										outNumber);
				}
				else
				{
					msg = String.format("is tagged out by %s%safter a dropped 3rd strike (%s). Out %d.",
										positionName[2], // Catcher position by definition.
										teams.getPlayer(1-teamAtBat, 2).getNumberName(),
										pitches.toString(),
										outNumber);
				}
				System.out.print(msg);
			}
			else if (token.startsWith("D3C"))
			{
				// Double Error - Dropped 3rd strike and error on catch by position
				// D3C[1-9]
				atBatFinished = true;
				token = token.replace("D3C","");
				int pos1 = Integer.valueOf(token);
				System.out.printf("reaches base on double error after a dropped 3rd strike from %s%sand a missed catch on throw from catcher by %s%s(%s).",
								  positionName[2], // Catcher position by definition.
								  teams.getPlayer(1-teamAtBat, 2).getNumberName(),
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  pitches.toString());
				(summary[ERRS])++;
				(summary[ERRS])++;
			}
			else if (token.startsWith("D3T"))
			{
				// Double Error - Dropped 3rd strike and error on throw by catcher
				// D3T[1-9]
				atBatFinished = true;
				token = token.replace("D3T","");
				int pos1 = Integer.valueOf(token);
				System.out.printf("reaches base on double error after a dropped 3rd strike from %s%sand a wild throw to %s%s(%s).",
								  positionName[2], // Catcher position by definition.
								  teams.getPlayer(1-teamAtBat, 2).getNumberName(),
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  pitches.toString());
				(summary[ERRS])++;
				(summary[ERRS])++;
			}
			else if (token.startsWith("D3"))
			{
				atBatFinished = true;
				System.out.printf("reaches base on error after a dropped 3rd strike from %s%s(%s).",
								  positionName[2], // Catcher position by definition.
								  teams.getPlayer(1-teamAtBat, 2).getNumberName(),
								  pitches.toString());
				(summary[ERRS])++;
			}
			else if (token.startsWith("LOB"))
			{
				// LOB,# delimited player numbers
				token = token.replace("LOB#","").trim();
				String[] pNums = token.split("#");
				if (pNums.length > 0)
				{
					System.out.print(" ");
					for (int i = 0; i < pNums.length; i++)
					{
						System.out.printf("%s%s", teams.getPlayer(teamAtBat, pNums[i]).getNumberName().trim(), (i < pNums.length-1) ? ", " : " ");
					}
					System.out.printf("%s left on base.", (pNums.length > 1) ? "are" : "is" );
				}
				summary[LOBS] = pNums.length;
			}
			else if (token.startsWith("FO"))
			{
				outNumber++;
				atBatFinished = true;
				processHitOut("FO", token);
			}
			else if (token.startsWith("GO"))
			{
				outNumber++;
				atBatFinished = true;
				processHitOut("GO", token);
			}
			else if (token.startsWith("LO"))
			{
				outNumber++;
				atBatFinished = true;
				processHitOut("LO", token);
			}
			else if (token.startsWith("F"))
			{
				atBatFinished = true;
				processHit("F", token);
			}
			else if (token.startsWith("G"))
			{
				atBatFinished = true;
				processHit("G", token);
			}
			else if (token.startsWith("LD"))
			{
				atBatFinished = true;
				processHit("LD",token);
			}
			else if (token.startsWith("RBI"))
			{
				// RBI,count - used in conjunction and prior to SCORE
				token = token.replace("RBI","").trim();
				int count = Integer.valueOf(token);
				System.out.printf(" %d RBI%s", count, (count > 1) ? "s," : ",");
			}
			else if (token.startsWith("RFC"))
			{
				// RFC[G,B][1-9][H,[-?B[1-9]]
				// RFCG6-3B5
				// RFCB6H
				atBatFinished = true;
				String type = (token.substring(3,4).equals("B")) ? "bunt" : "grounder";
				int pos1 = Integer.valueOf(token.substring(4,5));
				String msg = "is thrown";
				if (token.substring(5,6).equals("H"))
				{
					msg = "is held ";
				}
				else
				{
					String base = token.substring(6,8);
					base = (base.equals("HB")) ? "home" : base;
					int pos2 = Integer.valueOf(token.substring(8,9));
					msg = String.format("is thrown to %s%s",
										base,
										teams.getPlayer(1-teamAtBat, pos2).getNumberName());
				}

				System.out.printf("reaches base on fielder's choice as %s to %s%s%s(%s).",
								  type,
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  msg,
								  pitches.toString());
			}
			else if (token.startsWith("ROE"))
			{
				// ROE | hitType [B,G,F,L] | errType [C,M,T] | Position [1-9] |[- base ?B [Position 1-9]
				// Example: ROEBM1:   reaches base on error as bunt to pitcher is mishandled.
				// Example: ROEGT6-1B4: reaches base on error as grounder to SS is thrown wild to 1B (name of 2B).
				atBatFinished = true;
				String msg = "";
				String type = hitType.get(token.substring(3,4));
				String err = token.substring(4,5);
				int pos1 = Integer.valueOf(token.substring(5,6));
				if (token.contains("-")) // Not M errtype
				{
					String base = token.substring(7,9);
					base = (base.equals("HB")) ? "home" : base;
					int pos2 = Integer.valueOf(token.substring(9,10));
					if ("T".equals(err)) // Throwing errtype
					{
						msg = String.format("is thrown wildly to %s%s.",
											base,
											teams.getPlayer(1-teamAtBat, pos2).getNumberName());
					}
					else // Catching errtype
					{
						msg = String.format("is thrown to %s%sand missed.",
											base,
											teams.getPlayer(1-teamAtBat, pos2).getNumberName());
					}
				}
				else // Mishandled errtype
				{
					msg = "is mishandled.";
				}
				System.out.printf("reaches base on error as %s to %s%s%s",
								  type,
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  msg
								  );
				System.out.printf(" (%s).", pitches.toString());
				(summary[ERRS])++;
			}
			else if (token.startsWith("SAC"))
			{
				atBatFinished = true;
				outNumber++;
				String type = hitType.get( token.substring(0,4) );
				token = token.replace("SACB","").replace("SACF","");
				int pos  = Integer.valueOf(token.substring(0,1));
				System.out.printf("hits a %s to %s%s (%s). Out %d.",
								  type,
								  positionName[pos],
								  teams.getPlayer(1-teamAtBat, pos).getNumberName(),
								  pitches.toString(),
								  outNumber);
			}
			else if (token.startsWith("SCORE"))
			{
				// SCORE,# delimited player numbers - used in conjunction and after RBI
				// Example: RBI3, SCORE#1#2#4
				token = token.replace("SCORE#","").trim();
				String[] pNums = token.split("#");
				if (pNums.length > 0)
				{
					System.out.print(" ");
					for (int i = 0; i < pNums.length; i++)
					{
						System.out.printf("%s%s", teams.getPlayer(teamAtBat, pNums[i]).getNumberName().trim(), (i < pNums.length-1) ? ", " : " ");
					}
					System.out.printf("%s.", (pNums.length > 1) ? "all score" : "scores" );
					summary[RUNS] += pNums.length;
				}
			}
			else if (token.startsWith("BUNT"))
			{
				atBatFinished = true;
				processHit("BUNT", token);
			}
			else if (token.startsWith("MSG:"))
			{
				token = token.replace("MSG:","");
				System.out.print(" (" + token + ") ");
			}
			else if ("".equals(token))
			{
			}
			else
			{
				System.out.printf("*** Unknown at-bat token (%s) *** ", token);
			}
		}
		System.out.println();
	}

	private void processHit(String type, String event)
	{
		(summary[HITS])++;
		event = event.replace(type, "");
		int pos = Integer.valueOf(event.substring(0,1));
		System.out.printf("hits a %s towards %s%sfor a %s (%s).",
						  hitType.get(type),
						  positionName[pos],
						  teams.getPlayer(1-teamAtBat, pos).getNumberName(),
						  hitType.get(event.substring(1)),
						  pitches.toString());
	}

	private void processHitOut(String type, String event)
	{
		String throwOut = "";
		event = event.replace(type, "");
		int pos = Integer.valueOf(event.substring(0,1));
		if (event.contains("-"))
		{
			int pos1 = Integer.valueOf(event.substring(event.indexOf("-")+1));
			throwOut = String.format("and is thrown out at %s%s",
									 positionName[pos1],
									 teams.getPlayer(1-teamAtBat, (pos1 < 1) ? 2 : pos1).getNumberName());
		}
		System.out.printf("hits a %s to %s%s%s(%s). Out %d.",
						  hitType.get(type),
						  positionName[pos],
						  teams.getPlayer(1-teamAtBat, pos).getNumberName(),
						  throwOut,
						  pitches.toString(),
						  outNumber);
	}

	private void processRunningEvent(String events)
	{
		String[] plays = events.split(";"); // Multiple events are semicolon delimited
		for (String play : plays)
		{
			String[] tokens = play.split("\t"); // Events are tab delimited - name/action
			String playerNumber = tokens[0];
			String event = tokens[1];
			System.out.printf("%s", teams.getPlayer(teamAtBat, playerNumber).getNumberName() );

			if ("S2B".equals(event))
			{
				System.out.print("steals 2B.");
			}
			else if ("S3B".equals(event))
			{
				System.out.print("steals 3B.");
			}
			else if ("SHB".equals(event))
			{
				System.out.print("steals home and scores.");
				(summary[RUNS])++;
			}
			else if (event.startsWith(">"))
			{
				String plate = event.substring(1,3);
				String msg = "";
				if (plate.equals("HB"))
				{
					(summary[RUNS])++;
					plate = "home and scores";
				}
				if (event.length() > 3)
				{
					String how = event.substring(3);
					if (how.startsWith("OTE"))
					{
						int pos = Integer.valueOf(how.substring(3,4));
						msg = String.format(" on throwing error by %s%s",
											positionName[pos],
											teams.getPlayer(1-teamAtBat, pos).getNumberName());
						(summary[ERRS])++;
					}
					else if (how.startsWith("OCE"))
					{
						int pos = Integer.valueOf(how.substring(3,4));
						msg = String.format(" on catching error by %s%s",
											positionName[pos],
											teams.getPlayer(1-teamAtBat, pos).getNumberName());
						(summary[ERRS])++;
					}
				}
				System.out.printf("advances to %s%s.", plate, msg);
			}
			else if (event.startsWith("APO"))
			{
				// APO?B?-?
				String plate = event.substring(3,5);
				plate = (plate.equals("HB")) ? "home" : plate;
				int pos1 = Integer.valueOf(event.substring(5,6));
				int pos2 = Integer.valueOf(event.substring(7,8));
				System.out.printf("is almost picked off at %s on throw from %s%sto %s%s.",
								  plate,
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  positionName[pos2],
								  teams.getPlayer(1-teamAtBat, pos2).getNumberName());
			}
			else if (event.startsWith("PO"))
			{
				// PO?B?-?
				outNumber++;
				String plate = event.substring(2,4);
				plate = (plate.equals("HB")) ? "home" : plate;
				int pos1 = Integer.valueOf(event.substring(4,5));
				int pos2 = Integer.valueOf(event.substring(6,7));
				System.out.printf("is picked off at %s on throw from %s%sto %s%sOut %d.",
								  plate,
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  positionName[pos2],
								  teams.getPlayer(1-teamAtBat, pos2).getNumberName(),
								  outNumber);
			}
			else if (event.startsWith("TO"))
			{
				// TO?B?-?
				outNumber++;
				String plate = event.substring(2,4);
				plate = (plate.equals("HB")) ? "home" : plate;
				int pos1 = Integer.valueOf(event.substring(4,5));
				int pos2 = Integer.valueOf(event.substring(6,7));
				System.out.printf("is thrown out at %s from %s%sto %s%sOut %d.",
								  plate,
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  positionName[pos2],
								  teams.getPlayer(1-teamAtBat, pos2).getNumberName(),
								  outNumber);
			}
			else if ("".equals(event))
			{
			}
			else
			{
				System.out.printf(" *** Unknown Running Event (%s) *** ", event);
			}
		}
	}

	public static void main(String[] args)
	{
		PlayByPlay app = new PlayByPlay();
		app.run();
	}

	///////////////////////////////////////////////////////////////////////////
	//
	// Inner Class Teams
	//
	///////////////////////////////////////////////////////////////////////////
	class Teams
	{
		private ArrayList<Player> teamVisitor = new ArrayList<Player>();
		private ArrayList<Player> teamHome = new ArrayList<Player>();
		private Player nullPlayer = new Player("","","");
		private String teamNameVisitor = "Visiting Team";
		private String teamNameHome    = "Home Team";

		///////////////////////////////////////////////////
		// CONSTRUCTORS
		///////////////////////////////////////////////////
		Teams()
		{
		}

		///////////////////////////////////////////////////
		// ACCESSORS
		///////////////////////////////////////////////////
		public String getTeamName(int whichTeam)
		{
			if ( whichTeam == VISITOR )
				return teamNameVisitor;
			else
				return teamNameHome;
		}

		public Player getPlayer(int whichTeam, int pos)
		{
			Player result = nullPlayer;

			if ( whichTeam == VISITOR )
			{
				for (Player p : teamVisitor)
				{
					if (p.getPosition() == pos)
					{
						result = p;
						break;
					}
				}
			}
			else
			{
				for (Player p : teamHome)
				{
					if (p.getPosition() == pos)
					{
						result = p;
						break;
					}
				}
			}
			return result;
		}

		public Player getPlayer(int whichTeam, String num)
		{
			Player result = nullPlayer;

			if ( whichTeam == VISITOR )
			{
				for (Player p : teamVisitor)
				{
					if ( num.equals(p.getNumber()) )
					{
						result = p;
						break;
					}
				}
			}
			else
			{
				for (Player p : teamHome)
				{
					if ( num.equals(p.getNumber()) )
					{
						result = p;
						break;
					}
				}
			}
			return result;
		}

		///////////////////////////////////////////////////
		// MUTATORS
		///////////////////////////////////////////////////
		public void setTeamName(int whichTeam, String name)
		{
			if ( whichTeam == VISITOR )
				teamNameVisitor = name;
			else
				teamNameHome = name;
		}

		public void addPlayer(int whichTeam, Player p)
		{
			if ( whichTeam == VISITOR )
				teamVisitor.add( p );
			else
				teamHome.add( p );
		}

		///////////////////////////////////////////////////
		// UTILITIES
		///////////////////////////////////////////////////
		public String substituteRunner(int whichTeam, String numOut, String numIn)
		{
			Player po = getPlayer( whichTeam, numOut );
			Player pi = getPlayer( whichTeam, numIn );

			return "\t\tSubstitution: %s" + pi.getNumberName() + "to pinch run for" + po.getNumberName() + "\r\n";
		}

		public String substitutePlayer(int whichTeam, String pos, String numOut, String numIn)
		{
			Player po = getPlayer( whichTeam, numOut );
			Player pi = getPlayer( whichTeam, numIn );

			pi.setPosition( pos );
			if (po.getPosition() == Integer.valueOf(pos))
				po.setPosition( "0" ); // Bench

			return "\t\tSubstitution: %s" + pi.getNumberName() + "to %s for" + po.getNumberName() + "\r\n";
		}

		public void printStartingLineUps()
		{
			System.out.println("Starting Lineups:");
			System.out.printf("%-40s%-40s\r\n", getTeamName(VISITOR), getTeamName(HOME));
			System.out.printf("%-40s%1$-40s\r\n","Pos Num Name");
			System.out.printf("%-40s%1$-40s\r\n","--- --- ------------------------------");

			int vs = teamVisitor.size();
			int hs = teamHome.size();
			int ss = Math.min(vs,hs);

			for (int i=0; i < ss; i++)
			{
				Player vp = teamVisitor.get(i);
				Player hp = teamHome.get(i);
				System.out.printf("%-3s #%-2s %-30s  %-3s #%-2s %-30s\r\n",
								  positionAbbr[vp.getPosition()],
								  vp.getNumber(),
								  vp.getName(),
								  positionAbbr[hp.getPosition()],
								  hp.getNumber(),
								  hp.getName());
			}

			if (vs > hs)
			{
				for (int i = ss; i < vs; i++)
				{
					Player vp = teamVisitor.get(i);
					System.out.printf("%-3s #%-2s %-30s\r\n",
									  positionAbbr[vp.getPosition()],
									  vp.getNumber(),
									  vp.getName());
				}
			}
			else if (hs > vs)
			{
				for (int i = ss; i < hs; i++)
				{
					Player hp = teamHome.get(i);
					System.out.printf("%40s%-3s #%-2s %-30s\r\n",
									  "",
									  positionAbbr[hp.getPosition()],
									  hp.getNumber(),
									  hp.getName());
				}
			}

			System.out.println();
		}
	} // End of class Teams

	///////////////////////////////////////////////////////////////////////////
	//
	// Inner Class Player
	//
	///////////////////////////////////////////////////////////////////////////
	class Player
	{
		private int    position;
		private String number;
		private String name;

		///////////////////////////////////////////////////
		// CONSTRUCTORS
		///////////////////////////////////////////////////
		Player( String pos, String num, String name)
		{
			setPosition( pos );
			setNumber( num );
			setName( name );
		}

		Player( Player p )
		{
			setPosition( String.valueOf( p.getPosition() ) );
			setNumber( p.getNumber() );
			setName( p.getName() );
		}
		///////////////////////////////////////////////////
		// ACCESSORS
		///////////////////////////////////////////////////
		public int getPosition()
		{
			return position;
		}

		public String getNumber()
		{
			return number;
		}

		public String getName()
		{
			return name;
		}

		public String getNumberName()
		{
			String result;
			if ( "".equals( getNumber().trim() ) )
				result = " ";
			else
			{
				if ( "".equals(getName().trim()) )
					result = String.format(" #%s ", getNumber().trim());
				else
					result = String.format(" #%s %s ", getNumber().trim(), getName().trim() );
			}
			return result;
		}
		///////////////////////////////////////////////////
		// MUTATORS
		///////////////////////////////////////////////////
		public void setPosition( String p )
		{
			if ( (p == null) || ( "".equals(p) ) )
				this.position = 0;
			else
				this.position = Integer.parseInt( p );
		}

		public void setNumber( String n )
		{
			this.number = n;
		}

		public void setName( String n )
		{
			this.name = n;
		}

		///////////////////////////////////////////////////
		// UTILITIES
		///////////////////////////////////////////////////
		@Override
		public String toString()
		{
			return String.format("Pos[%d], Number[%s], Name[%s]\r\n", this.getPosition(), this.getNumber(), this.getName() );
		}
	} // End of class Player
}