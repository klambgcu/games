import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class GetPlayByPlay
{
	private Boolean endInning   = false;

	private void doWork(String fileName) throws Exception
	{
		BufferedReader in  = null;
		PrintWriter    out = null;

		try
		{
			String htmlFileName = "PlayByPlay_" + fileName.substring(0, fileName.lastIndexOf(".")) + ".html";

			in  = new BufferedReader(new FileReader(fileName));
			out = new PrintWriter(new FileWriter(htmlFileName));

			System.out.println("Reading play by play file: " + fileName);
			System.out.println("Creating HTML formatted file: " + htmlFileName);

			// Open file - get first line->titleLine
			String titleLine = in.readLine();
			out.println("<html>");
			out.println("<head>");
			out.println("<title>");
			out.println(titleLine.trim());
			out.println("</title>");
			out.println("<style>");
			out.println("li#inning    {border-top:black solid 1px;border-left:black solid 1px;border-right:black solid 1px;margin: 0px;padding-left: 5px; color: #FFFFFF; background-color: #800000; font:normal normal bold 14px arial;list-style-type:none;}");
			out.println("li#inn_part  {border-top:black solid 1px;margin-left: -5px;padding-left: 5px; color: #000000; background-color: #FF0000; font:normal normal bold 12px arial;list-style-type:none;}");
			out.println("ul           {margin-left: -5px;padding-left: 5px;font:normal normal normal 12px arial;list-style-type:none; color: #000000;}");
			out.println("ul#plays     {margin-left: -5px;padding-left: 5px; color: #000000; background-color: #F8F0F0; font:normal normal normal 12px arial;list-style-type:none;}");
			out.println("li#game_over {border:black solid 1px;margin-left: 0px;padding-left: 5px; color: #000000; background-color: #800000; font:normal normal bold 12px arial;}");
			out.println("</style>");
			out.println("</head>");
			out.println("<body>");
			out.println("<pre>");
			out.println(titleLine);

			// Loop through beginning of file - set up as predefine layout
			// End this initial loop when play by play starts - Inning
			String line = null;
			while ( (line = in.readLine()) != null)
			{
				if (line.trim().startsWith("Inning"))
				{
					out.println("</pre>");
					out.println("<UL>");
					out.println("<LI id=inning>");
					out.println(line.trim());
					out.println("<UL>");
					break;
				}
				out.println(line);
			}

			// Loop through play by play and format output
			// End when Game Ends. is reached
			line = null;
			while ( (line = in.readLine()) != null)
			{
				if (line.trim().startsWith("Inning"))
				{
					out.println("</UL>");
					out.println("</UL>");
					out.println("<LI id=inning>");
					out.println(line.trim());
					out.println("<UL>");
				}
				else if (line.trim().startsWith("Top"))
				{
					out.println("<LI id=inn_part>");
					out.println(line.trim());
					out.println("<UL id=plays>");
				}
				else if (line.trim().startsWith("Bottom"))
				{
					out.println("</UL>");
					out.println("<LI id=inn_part>");
					out.println(line.trim());
					out.println("<UL id=plays>");
				}
				else if (line.trim().startsWith("Game Ends."))
				{
					out.println("</UL>");
					out.println("</UL>");
					out.println("<LI id=game_over>Game Ends.");
					out.println("</UL>");
					break;
				}
				else
					out.println("<LI>" + line.trim());
			}

			// Loop through the rest of the file - set up as predefine layout
			out.println("<pre>");
			line = null;
			while ( (line = in.readLine()) != null)
			{
				out.println(line);
			}
			out.println("</pre>");
			out.println("</body>");
			out.println("</html>");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}

		finally
		{
			if (in != null)
				in.close();

			if (out != null)
				out.close();
			System.out.println("Completed");
		}
	}

	public static void main(String args[])
	{

		Scanner input = new Scanner(System.in);

		System.out.print("Enter the name of the game play by play text file: ");
		String gameFileName = input.next();
		input.close();

		GetPlayByPlay play = new GetPlayByPlay();

		try
		{
			play.doWork(gameFileName);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
