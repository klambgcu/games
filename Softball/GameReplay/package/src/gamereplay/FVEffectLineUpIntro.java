package com.kelsoft.gamereplay;

import java.util.Iterator;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

/**
 *
 * @author Kelly Lamb
 */
public class FVEffectLineUpIntro extends FVEffectAbstract
{
	private double     incX;
	private double     incY;
	private double     x;
	private double     y;
	private int        Count;
	private int        countdown;
	private int        dstX;
	private int        dstY;
	private int        move;
	private int        Scene = -1;
	private int        side;
	private Iterator   mates;
	private Player     p;
	private PlayerView pv;

	public FVEffectLineUpIntro(JComponent parent, Teams teams)
	{
		super(parent, teams);
		setSleepAmount(20);
	}

	public void render()
	{
	}

	public void reset()
	{
		stop();
		Scene = -1;
		countdown = 0;
	}

	public void step()
	{
		if (! isAnimating()) return;
		if (Scene == -1) // Visiting Team LineUp (-1..1)
		{
			Scene++;
			mates = teams.getVisitorTeam().getRoster().iterator();
			countdown = teams.getVisitorTeam().getRoster().size();
			Count = 0;
			side = GameConstants.TEAM_VISITOR;
		}
		else if (Scene == 0)
		{
			if (countdown <= 0)
			{
				Scene = 2; // Do Home Team LineUp
			}
			else
			{
				countdown--;
				Scene = 1;
				Count++;
				move = 0;
				p  = (Player)mates.next();
				((FieldView)parent).setPlayerPicture(p);
				pv = p.getPlayerView();
				pv.setNameAlignment(GameConstants.PLAYER_TYPE_INTRO);
				pv.setVisible(true);
				dstX = GameConstants.locOffense[0][0] + Count * 12;
				dstY = GameConstants.locOffense[0][1] - Count * 7;
				incX = (double)(dstX - GameConstants.locDugOut[side][0]) / 25.0;
				incY = (double)(dstY - GameConstants.locDugOut[side][1]) / 25.0;
				x = pv.getPlayerIcon().getX()+1;
				y = pv.getPlayerIcon().getY();
				pv.setLocation( (int)x, (int)y );
			}
		}
		else if (Scene == 1)
		{
			if (move >= 25)
			{
				pv.setLocation( dstX, dstY );
				Scene = 0;
			}
			else
			{
				move++;
				x += incX;
				y += incY;
				pv.setLocation( (int)x, (int)y );
			}
		}
		else if (Scene == 2) // Home Team LineUp (2..4)
		{
			Scene++;
			mates = teams.getHomeTeam().getRoster().iterator();
			countdown = teams.getHomeTeam().getRoster().size();
			Count = 0;
			side = GameConstants.TEAM_HOME;
		}
		else if (Scene == 3)
		{
			if (countdown <= 0)
			{
				Scene = 99; // Exit Sentinel
			}
			else
			{
				countdown--;
				Scene = 4;
				Count++;
				move = 0;
				p  = (Player)mates.next();
				((FieldView)parent).setPlayerPicture(p);
				pv = p.getPlayerView();
				pv.setNameAlignment(GameConstants.PLAYER_TYPE_INTRO);
				pv.setVisible(true);
				dstX = GameConstants.locOffense[0][0] - Count * 12;
				dstY = GameConstants.locOffense[0][1] - Count * 7;
				incX = (double)(dstX - GameConstants.locDugOut[side][0]) / 25.0;
				incY = (double)(dstY - GameConstants.locDugOut[side][1]) / 25.0;
				x = pv.getPlayerIcon().getX()+1;
				y = pv.getPlayerIcon().getY();
				pv.setLocation( (int)x, (int)y );
			}
		}
		else if (Scene == 4)
		{
			if (move >= 25)
			{
				pv.setLocation( dstX, dstY );
				Scene = 3;
			}
			else
			{
				move++;
				x += incX;
				y += incY;
				pv.setLocation( (int)x, (int)y );
			}
		}
		else
		{
			Scene = -1;
			((FieldView)parent).clearPlayerPicture();
			stop();
		}
	}

	public void setMessage(String Message)
	{
		message = Message;
	}
}

