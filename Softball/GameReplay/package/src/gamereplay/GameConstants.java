package com.kelsoft.gamereplay;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

public class GameConstants
{
	public static final int TEAM_COLOR_PURPLE = 0;
	public static final int TEAM_COLOR_RED    = 1;
	public static final int TEAM_COLOR_GREEN  = 2;
	public static final int TEAM_COLOR_CYAN   = 3;
	public static final int TEAM_COLOR_BLUE   = 4;
	public static final int TEAM_COLOR_YELLOW = 5;
	public static final int TEAM_COLOR_BLACK  = 6;

	public static final String[] teamColors =
	{
		"PURPLE","RED","GREEN","CYAN","BLUE","YELLOW","BLACK"
	};

	public static final String HOME_RUN     = "Home Run";
	public static final String GRAND_SLAM   = "Grand Slam";
	public static final String STRIKE_OUT   = "Strike OUT";
	public static final String WALK         = "WALK";
	public static final String SINGLE       = "SINGLE";
	public static final String DOUBLE       = "DOUBLE";
	public static final String TRIPLE       = "TRIPLE";
	public static final String HIT_BY_PITCH = "HIT BY PITCH";
	public static final String SCORE        = "SCORE";
	public static final String DOUBLEPLAY   = "DOUBLEPLAY";
	public static final String TRIPLEPLAY   = "TRIPLEPLAY";
	public static final String STEAL        = "STEAL";
	public static final String PICKED_OFF   = "PICKED OFF";
	public static final String GAME_OVER    = "GAME OVER";
	public static final String PLAY_BALL    = "PLAY BALL";
	public static final String FLY_OUT      = "FLY OUT";
	public static final String GROUND_OUT   = "GROUND OUT";
	public static final String LINE_OUT     = "LINE OUT";

	public static final int TEAM_VISITOR = 0;
	public static final int TEAM_HOME    = 1;

	public static final int VISIBLE_NONE   = 0;
	public static final int VISIBLE_BEFORE = 1;
	public static final int VISIBLE_AFTER  = 2;

	// index : 0=DR,1=DL,2=OR,3=OL
	// Multiply Player types by 2 and add hand to obtain image index.
	public static final int PLAYER_TYPE_DEFENSE = 0;
	public static final int PLAYER_TYPE_OFFENSE = 1;
	public static final int PLAYER_TYPE_INTRO   = 2;

	public static final int PLAYER_RIGHTHAND = 0;
	public static final int PLAYER_LEFTHAND  = 1;

	public static final int BATTER_ONDECK  = -1;
	public static final int BATTER_ATBAT   = 0;
	public static final int BATTER_RUNTO1B = 1;
	public static final int BATTER_RUNTO2B = 2;
	public static final int BATTER_RUNTO3B = 3;
	public static final int BATTER_RUNHOME = 4;

	public static final int BALLPATH_NORMAL   = 0;
	public static final int BALLPATH_STRAIGHT = 1;
	public static final int BALLPATH_FLYBALL  = 2;
	public static final int BALLPATH_GROUNDER = 4;

	public static final int PITCHED_BALL_STRIKE = 0;
	public static final int PITCHED_BALL_FOUL   = 1;
	public static final int PITCHED_BALL_BALL   = 2;

	public static final String[] pitchedBallTitle = {"Strike","Foul","Ball"};

	public static final String[] playerHands = {"R","L","B"};

	public static final int[][] locDefense =
	{
		{0,0},{250,164},{250,223},{303,152},{287,125},{197,152},{213,125},{151,80},{250,45},{349,80}
	};

	public static final int[][] posBall =
	{
		{0,0},{256,180},{256,235},{309,168},{293,141},{203,168},{219,141},{157,96},{256,61},{355,96}
	};

	public static final int[] posBallHit = {256, 220};

	public static final int[][] locOffense =
	{
		{250,198},{307,160},{250,124},{193,160},{250,198}
	};

	public static final int[][] locBattersBox =
	{
		{243,201},{257,201}
	};

	public static final int[][] locOnDeck =
	{
		{290,205},{208,205}
	};

	public static final int[][] locDugOut =
	{
		{316,210},{184,210}
	};

	public static final String[] titleAbbr =
	{
		"","P","C","1B","2B","3B","SS","LF","CF","RF","DP","PR","PH"
	};

	public static final String[] titleFull =
	{
		"","Pitcher","Catcher","First Base","Second Base","Third Base",
		"Short Stop","Left Field","Center Field","Right Field",
		"Designated Player","Pinch Runner","Pinch Hitter"
	};
}