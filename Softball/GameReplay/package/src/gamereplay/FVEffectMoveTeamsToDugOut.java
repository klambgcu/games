package com.kelsoft.gamereplay;

import java.util.Iterator;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

/**
 *
 * @author Kelly Lamb
 */
public class FVEffectMoveTeamsToDugOut extends FVEffectAbstract
{
	private int Scene = -1;

	public FVEffectMoveTeamsToDugOut(JComponent parent, Teams teams)
	{
		super(parent, teams);
		//setSleepAmount(80);
	}

	public void render()
	{
	}

	public void reset()
	{
		stop();
		Scene = -1;
	}

	public void step()
	{
		if (! isAnimating()) return;
		if (Scene == -1)
		{
			Scene++;
			for (Player p : teams.getVisitorTeam().getRoster())
			{
				p.getPlayerView().setDestinationSteps( (double)GameConstants.locDugOut[GameConstants.TEAM_VISITOR][0],
													   (double)GameConstants.locDugOut[GameConstants.TEAM_VISITOR][1],
													   25 );
			}

			for (Player p : teams.getHomeTeam().getRoster())
			{
				p.getPlayerView().setDestinationSteps( (double)GameConstants.locDugOut[GameConstants.TEAM_HOME][0],
													   (double)GameConstants.locDugOut[GameConstants.TEAM_HOME][1],
													   25 );
			}
		}
		else if (Scene == 0)
		{
			for (Player p : teams.getVisitorTeam().getRoster())
			{
				p.getPlayerView().moveToDestination();
			}

			for (Player p : teams.getHomeTeam().getRoster())
			{
				if ( p.getPlayerView().moveToDestination() )
					Scene = 1;
			}
		}
		else if (Scene == 1)
		{
			for (Player p : teams.getVisitorTeam().getRoster())
			{
				p.getPlayerView().setVisible(false);
			}

			for (Player p : teams.getHomeTeam().getRoster())
			{
				p.getPlayerView().setVisible(false);
			}
			Scene++;
		}
		else
		{
			Scene = -1;
			stop();
		}
	}

	public void setMessage(String Message)
	{
		message = Message;
	}
}
