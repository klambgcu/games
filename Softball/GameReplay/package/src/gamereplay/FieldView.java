package com.kelsoft.gamereplay;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.border.BevelBorder;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.SwingConstants;

public class FieldView extends JComponent
{
	private ArrayList<JLabel> pitchedBalls   = new ArrayList<JLabel>();
	private Ball              ball;
	private FVEffectAbstract  fvEffectCurrent = null;
	private int               ballIndex = 2;
	private int               pitchedBallCount = 0;
	private int[]             pathBatter;
	private JLabel            frame          = new JLabel();
	private JLabel            playerName     = new JLabel();
	private JLabel            playerNumber   = new JLabel();
	private JLabel            playerPicture  = new JLabel();
	private JLabel            playerPosition = new JLabel();
	private JLayeredPane      field;
	private Player[]          runners = new Player[5];
	private Teams             teams;

	public FieldView()
	{
		// Create the component and add border
		super();
		setSize(524,264);
		setOpaque(false);
		setDoubleBuffered(true);
		setMaximumSize(new java.awt.Dimension(524, 264));
		setMinimumSize(new java.awt.Dimension(524, 264));
		setPreferredSize(new java.awt.Dimension(524, 264));
		setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

		// Create layered pane to handle the field elements
		field = new JLayeredPane();
		field.setMaximumSize(new java.awt.Dimension(520, 260));
		field.setMinimumSize(new java.awt.Dimension(520, 260));
		field.setPreferredSize(new java.awt.Dimension(520, 260));
		field.setBounds(2,2,520,260);

		// Load field background image and add to the field
		ImageIcon fieldImage = new ImageIcon(getClass().getResource("images/field5.png"));
		JLabel fieldBackground = new JLabel(fieldImage);
		fieldBackground.setBounds(0, 0, fieldImage.getIconWidth(), fieldImage.getIconHeight());
		field.add(fieldBackground, new Integer(-1), 0);

		ball = new Ball(this);
		ball.setLocation(GameConstants.posBall[1][0] ,GameConstants.posBall[1][1] );
		field.add(ball, new Integer(20), 0);

		frame.setIcon(GameImages.getFrame());
		frame.setBounds(0,150,110,110);
		frame.setVisible(false);
		field.add(frame, new Integer(0),0);

		playerPicture.setBounds(   8, 178,  75, 75);
		playerNumber.setBounds(  100, 215, 200, 15);
		playerName.setBounds(    100, 230, 200, 15);
		playerPosition.setBounds(100, 245, 200, 15);

		field.add(playerPicture,  new Integer(1),0);
		field.add(playerNumber,   new Integer(1),0);
		field.add(playerName,     new Integer(1),0);
		field.add(playerPosition, new Integer(1),0);

		// Add the field to this components display
		add(field);
	}

	public void setTeams(Teams teams)
	{
		this.teams = teams;
		for (Player mate : teams.getVisitorTeam().getRoster())
		{
			new PlayerView( mate, GameConstants.TEAM_HOME, this );
			field.add( mate.getPlayerView().getPlayerIcon(), new Integer(9), -1);
			field.add( mate.getPlayerView().getPlayerName(), new Integer(2), -1);
		}

		for (Player mate : teams.getHomeTeam().getRoster())
		{
			new PlayerView( mate, GameConstants.TEAM_HOME, this );
			field.add( mate.getPlayerView().getPlayerIcon(), new Integer(6), -1);
			field.add( mate.getPlayerView().getPlayerName(), new Integer(1), -1);
		}
	}

	public boolean isBallAnimating()
	{
		return ball.isAnimating();
	}

	public boolean isAnimating()
	{
		if ((fvEffectCurrent != null) && (fvEffectCurrent.isAnimating()))
			return true;
		else
			return false;
	}

	public boolean isRunnersAnimating()
	{
		boolean result = false;

		for (Player p : runners)
		{
			if (p != (Player)null)
				if (p.getPlayerView().isAnimating())
				{
					result = true;
					break;
				}
		}
		return result;
	}

	public boolean isCompleted()
	{
		if ((fvEffectCurrent != null) && (fvEffectCurrent.isCompleted()))
			return true;
		else
			return false;
	}

	public void IntroduceLineUps()
	{
		fvEffectCurrent = new FVEffectLineUpIntro(this, teams);
		fvEffectCurrent.start();
	}

	public void moveTeamsToDugOut()
	{
		fvEffectCurrent = new FVEffectMoveTeamsToDugOut(field, teams);
		fvEffectCurrent.start();
	}

	public void moveDefenseToPosition()
	{
		fvEffectCurrent = new FVEffectMoveDefenseToPosition(field, teams);
		fvEffectCurrent.start();
	}

	public void advanceInning()
	{
		clearPlayerPicture();
		clearPitchedBalls();
		clearRunners();
		fvEffectCurrent = new FVEffectAdvanceInning(field, teams);
		fvEffectCurrent.start();
	}

	public void setPlayerPicture(Player p)
	{
		frame.setVisible(true);
		playerPicture.setIcon( p.getPicture() );
		playerNumber.setText( "Number: " + p.getNumber() );
		playerName.setText( "Name: " + p.getName() );
		playerPosition.setText( "Position: " + GameConstants.titleFull[p.getPosition()] );
	}

	public void clearPlayerPicture()
	{
		frame.setVisible(false);
		playerPicture.setIcon( (ImageIcon)null );
		playerNumber.setText( "" );
		playerName.setText( "" );
		playerPosition.setText( "" );
	}

/*
	public void pitch()
	{
		BallPoint[] b = new BallPoint[7];
		b[0] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  2);
		b[1] = new BallPoint( (double)GameConstants.posBall[2][0],
							  (double)GameConstants.posBall[2][1],
							  10);
		b[2] = new BallPoint( (double)GameConstants.posBall[4][0],
							  (double)GameConstants.posBall[4][1],
							  10);
		b[3] = new BallPoint( (double)GameConstants.posBall[5][0],
							  (double)GameConstants.posBall[5][1],
							  10);
		b[4] = new BallPoint( (double)GameConstants.posBall[3][0],
							  (double)GameConstants.posBall[3][1],
							  10);
		b[5] = new BallPoint( (double)GameConstants.posBall[6][0],
							  (double)GameConstants.posBall[6][1],
							  10);
		b[6] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  10);
		pitch(b);
	}
*/
	public void pitch( BallPoint[] b )
	{
		if (ball.moveToDestination())
		{
			ball.setDestinationSteps(b);
		}
	}

	public void showPitchedBall(int type)
	{
		if (pitchedBallCount >= pitchedBalls.size())
		{
			JLabel pitchedBall = new JLabel(GameConstants.pitchedBallTitle[type],
											GameImages.getPitchedBallIcon(type),
											SwingConstants.LEFT);
			pitchedBall.setForeground(Color.GRAY);
			pitchedBall.setSize(200,20);
			pitchedBall.setLocation(470, 300);
			pitchedBalls.add(pitchedBall);

			field.add(pitchedBall, new Integer(2), 0);
		}
		else
		{
			JLabel pitchedBall = pitchedBalls.get(pitchedBallCount);
			pitchedBall.setText(GameConstants.pitchedBallTitle[type]);
			pitchedBall.setIcon(GameImages.getPitchedBallIcon(type));
		}

		for (int i = 0; i < pitchedBallCount; i++)
		{
			JLabel x = pitchedBalls.get(i);
			x.setLocation(x.getX(),x.getY()-15);
		}
		JLabel x = pitchedBalls.get(pitchedBallCount);
		x.setLocation(x.getX(), 240);

		pitchedBallCount++;
		field.repaint();
	}

	public void clearPitchedBalls()
	{
		if (pitchedBallCount > 0)
		{
			for (JLabel x : pitchedBalls)
				x.setLocation(470,300);
		}
		pitchedBallCount = 0;
		field.repaint();
	}

	public void setNewBatter(Player player)
	{
		if (runners[0] != (Player)null)
			runners[0].getPlayerView().moveToDugOut();
		runners[0] = player;
		setPlayerPicture(player);
		clearPitchedBalls();
		player.getPlayerView().moveToBattersBox();
	}

	public void clearRunners()
	{
		for (int i = 0; i < runners.length; i++)
			runners[i] = (Player)null;
	}

	public void advanceRunners(Player player, int[] path)
	{
		// path[0] always equals current location
		// if path[0]=0, then home is the current location
		// if path[0]=2, then 2B is the current location
		//player.getPlayerView().startRunning(path);
		this.pathBatter = path;

	}

	public void fireBatterMovement()
	{
		if ((runners[0] != (Player)null) && (pathBatter != null))
		{
			int max = pathBatter[pathBatter.length-1];
			runners[0].getPlayerView().startRunning(pathBatter);
			// Hit may force existing runners to advance
			for (int i=1; i < 4; i++)
			{
				if ((runners[i] != (Player)null) && i <= max)
					runners[i].getPlayerView().startRunning( createRunningPath(i, ++max) );
			}
			pathBatter = null;
		}
	}

	public int[] createRunningPath(int oldPosition, int newPosition)
	{
		newPosition = Math.min(4, newPosition);
		int size = (newPosition - oldPosition) + 1;
		int[] result = new int[size];
		for (int i = 0; i < size; i++)
			result[i] = Math.min(oldPosition + i,4);
		return result;
	}

	synchronized public void fireSetRunnerPosition(Player player, int oldPosition, int newPosition)
	{
		if (newPosition >= 4)
		{
			player.getPlayerView().moveToDugOut();
			if (runners[oldPosition] == player)
				runners[oldPosition] = (Player)null;
		}
		else
		{
			if (runners[oldPosition] == player)
				runners[oldPosition] = (Player)null;
			runners[newPosition] = player;
		}

/*
		for (int i=0; i < runners.length; i++)
			if (runners[i] != (Player)null)
				System.out.printf("%d %-20s,",i, runners[i].getNumberName().trim());
			else
				System.out.printf("%d %-20s,",i,"-");
		System.out.println();
*/
	}
}

