package com.kelsoft.gamereplay;

import java.util.ArrayList;
import java.util.List;

public class Teams
{
	private Team teamVisitor;
	private Team teamHome;

	///////////////////////////////////////////////////
	// CONSTRUCTORS
	///////////////////////////////////////////////////
	Teams()
	{
	}

	Teams(Team visitor, Team home)
	{
		teamVisitor = visitor;
		teamHome = home;

		teamVisitor.setSide(GameConstants.TEAM_VISITOR);
		teamVisitor.setType(GameConstants.PLAYER_TYPE_OFFENSE);

		teamHome.setSide(GameConstants.TEAM_HOME);
		teamHome.setType(GameConstants.PLAYER_TYPE_DEFENSE);
	}

	///////////////////////////////////////////////////
	// ACCESSORS
	///////////////////////////////////////////////////
	public String getTeamName(int whichTeam)
	{
		if ( whichTeam == GameConstants.TEAM_VISITOR )
			return teamVisitor.getName();
		else
			return teamHome.getName();
	}

	public Player getPlayer(int whichTeam, int pos)
	{
		if ( whichTeam == GameConstants.TEAM_VISITOR )
			return teamVisitor.getPlayer(pos);
		else
			return teamHome.getPlayer(pos);
	}

	public Player getPlayer(int whichTeam, String num)
	{
		if ( whichTeam == GameConstants.TEAM_VISITOR )
			return teamVisitor.getPlayer(num);
		else
			return teamHome.getPlayer(num);
	}

	public Team getVisitorTeam()
	{
		return teamVisitor;
	}

	public Team getHomeTeam()
	{
		return teamHome;
	}

	///////////////////////////////////////////////////
	// MUTATORS
	///////////////////////////////////////////////////
	public void addPlayer(int whichTeam, Player p)
	{
		if ( whichTeam == GameConstants.TEAM_VISITOR )
			teamVisitor.addPlayer( p );
		else
			teamHome.addPlayer( p );
		p.setPicture( p.getTeam().getShowPix() );
	}

	public void setTeam(int whichTeam, Team team)
	{
		if ( whichTeam == GameConstants.TEAM_VISITOR )
			setVisitorTeam(team);
		else
			setHomeTeam(team);
	}

	public void setVisitorTeam(Team team)
	{
		teamVisitor = team;
		teamVisitor.setSide(GameConstants.TEAM_VISITOR);
		teamVisitor.setType(GameConstants.PLAYER_TYPE_OFFENSE);
	}

	public void setHomeTeam(Team team)
	{
		teamHome = team;
		teamHome.setSide(GameConstants.TEAM_HOME);
		teamHome.setType(GameConstants.PLAYER_TYPE_DEFENSE);
	}

	///////////////////////////////////////////////////
	// UTILITIES
	///////////////////////////////////////////////////
	public String substituteRunner(int whichTeam, String numOut, String numIn)
	{
		Player po = getPlayer( whichTeam, numOut );
		Player pi = getPlayer( whichTeam, numIn );

		return "\t\tSubstitution: %s" + pi.getNumberName() + "to pinch run for" + po.getNumberName() + "\r\n";
	}

	public String substitutePlayer(int whichTeam, String pos, String numOut, String numIn)
	{
		Player po = getPlayer( whichTeam, numOut );
		Player pi = getPlayer( whichTeam, numIn );

		pi.setPosition( pos );
		if (po.getPosition() == Integer.valueOf(pos))
			po.setPosition( "0" ); // Bench

		return "\t\tSubstitution: %s" + pi.getNumberName() + "to %s for" + po.getNumberName() + "\r\n";
	}

	public void printStartingLineUps()
	{
		System.out.println("Starting Lineups:");
		System.out.printf("%-40s%-40s\r\n", getTeamName(GameConstants.TEAM_VISITOR), getTeamName(GameConstants.TEAM_HOME));
		System.out.printf("%-40s%1$-40s\r\n","Pos Num Name");
		System.out.printf("%-40s%1$-40s\r\n","--- --- ------------------------------");

		List<Player> vRoster = teamVisitor.getRoster();
		List<Player> hRoster = teamHome.getRoster();

		int vs = vRoster.size();
		int hs = hRoster.size();
		int ss = Math.min(vs,hs);

		for (int i=0; i < ss; i++)
		{
			Player vp = vRoster.get(i);
			Player hp = hRoster.get(i);
			System.out.printf("%-3s #%-2s %-30s  %-3s #%-2s %-30s\r\n",
							  GameConstants.titleAbbr[vp.getPosition()],
							  vp.getNumber(),
							  vp.getName(),
							  GameConstants.titleAbbr[hp.getPosition()],
							  hp.getNumber(),
							  hp.getName());
		}

		if (vs > hs)
		{
			for (int i = ss; i < vs; i++)
			{
				Player vp = vRoster.get(i);
				System.out.printf("%-3s #%-2s %-30s\r\n",
								  GameConstants.titleAbbr[vp.getPosition()],
								  vp.getNumber(),
								  vp.getName());
			}
		}
		else if (hs > vs)
		{
			for (int i = ss; i < hs; i++)
			{
				Player hp = hRoster.get(i);
				System.out.printf("%40s%-3s #%-2s %-30s\r\n",
								  "",
								  GameConstants.titleAbbr[hp.getPosition()],
								  hp.getNumber(),
								  hp.getName());
			}
		}

		System.out.println();
	}

	public void resetPosition()
	{
		teamVisitor.resetPosition();
		teamHome.resetPosition();
	}
} // End of class Teams
