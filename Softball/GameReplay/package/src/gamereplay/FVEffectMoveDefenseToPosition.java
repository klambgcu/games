package com.kelsoft.gamereplay;

import java.util.Iterator;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

/**
 *
 * @author Kelly Lamb
 */
public class FVEffectMoveDefenseToPosition extends FVEffectAbstract
{
	private int  Scene = -1;
	private int  side;
	private Team team;

	public FVEffectMoveDefenseToPosition(JComponent parent, Teams teams)
	{
		super(parent, teams);
		//setSleepAmount(80);
	}

	public void render()
	{
	}

	public void reset()
	{
		stop();
		Scene = -1;
	}

	public void step()
	{
		if (! isAnimating()) return;
		if (Scene == -1)
		{
			Scene++;

			if (teams.getVisitorTeam().getType() == GameConstants.PLAYER_TYPE_DEFENSE)
			{
				side = GameConstants.TEAM_VISITOR;
				team = teams.getVisitorTeam();
			}
			else
			{
				side = GameConstants.TEAM_HOME;
				team = teams.getHomeTeam();
			}

			for (Player p : team.getRoster())
			{
				int pos = p.getPosition();
				if (pos > 0)
				{
					p.getPlayerView().setDestinationSteps( (double)GameConstants.locDefense[pos][0],
														   (double)GameConstants.locDefense[pos][1],
														   25 );
					p.getPlayerView().setNameAlignment(GameConstants.PLAYER_TYPE_DEFENSE);
					p.getPlayerView().setVisible(true);
				}
			}
		}
		else if (Scene == 0)
		{
			for (Player p : team.getRoster())
			{
				int pos = p.getPosition();
				if (pos > 0)
				{
					if (p.getPlayerView().moveToDestination())
						Scene = 1;
				}
			}
		}
		else
		{
			Scene = -1;
			stop();
		}
	}

	public void setMessage(String Message)
	{
		message = Message;
	}
}
