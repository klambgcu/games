package com.kelsoft.gamereplay;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

public class GameImages
{
	private static ImageIcon[][] teamImageIcons  = new ImageIcon[GameConstants.teamColors.length][4];
	private static ImageIcon[]   ballImages      = new ImageIcon[4];
	private static ImageIcon[]   pitchedBallIcon = new ImageIcon[3];
	private static ImageIcon     placeHolder;
	private static ImageIcon     frame;

	public GameImages()
	{
		ImageIcon allPlayers = new ImageIcon(getClass().getResource("images/players4.png"));

		for (int color = 0; color < GameConstants.teamColors.length; color++)
		{
			ImageIcon[] playerIcons = new ImageIcon[4];

			/* index : 0=DR,1=DL,2=OR,3=OL */
			for (int index=0; index < 4; index++)
			{
				BufferedImage bimage = new BufferedImage(19, 25, BufferedImage.TRANSLUCENT);
				Graphics g = bimage.getGraphics();

				if (index == 0)      g.drawImage(allPlayers.getImage(), 0, 0, 19, 25, color*19,      0, (color+1)*19, 25, null);
				else if (index == 1) g.drawImage(allPlayers.getImage(), 0, 0, 19, 25, (color+1)*19,  0, color*19,     25, null);
				else if (index == 2) g.drawImage(allPlayers.getImage(), 0, 0, 19, 25, color*19,     25, (color+1)*19, 50, null);
				else if (index == 3) g.drawImage(allPlayers.getImage(), 0, 0, 19, 25, (color+1)*19, 25, color*19,     50, null);

				ImageIcon player = new ImageIcon(bimage);
				playerIcons[index] = player;
			}
			teamImageIcons[color] = playerIcons;
		}

		ImageIcon balls = new ImageIcon(getClass().getResource("images/Ball.png"));

		ImageIcon[] playerIcons = new ImageIcon[4];

		for (int index=0; index < 4; index++)
		{
			BufferedImage bimage = new BufferedImage(8, 8, BufferedImage.TRANSLUCENT);
			Graphics g = bimage.getGraphics();

			g.drawImage(balls.getImage(), 0, 0, 8, 8, index*8, 0, (index+1)*8, 8, null);

			ImageIcon ball = new ImageIcon(bimage);
			ballImages[index] = ball;
		}

		placeHolder = new ImageIcon(getClass().getResource("images/placeholder.jpg"));
		frame       = new ImageIcon(getClass().getResource("images/Frame.png"));

		pitchedBallIcon[GameConstants.PITCHED_BALL_STRIKE] = new ImageIcon(getClass().getResource("images/PitchedBallRed.png"));
		pitchedBallIcon[GameConstants.PITCHED_BALL_FOUL  ] = new ImageIcon(getClass().getResource("images/PitchedBallPurple.png"));
		pitchedBallIcon[GameConstants.PITCHED_BALL_BALL  ] = new ImageIcon(getClass().getResource("images/PitchedBallBlue.png"));
	}

	public static ImageIcon[] getTeamIcons(int color)
	{
		return teamImageIcons[color];
	}

	public static ImageIcon getImageIcon( int color, int type, int hand )
	{
		return teamImageIcons[color][ (type * 2) + hand ];
	}

	public static ImageIcon[] getBallIcons()
	{
		return ballImages;
	}

	public static ImageIcon getPlaceHolder()
	{
		return placeHolder;
	}

	public static ImageIcon getFrame()
	{
		return frame;
	}

	public static ImageIcon getPitchedBallIcon(int index)
	{
		if ((index < 0) || (index > 2))
			return pitchedBallIcon[0];
		else
			return pitchedBallIcon[index];
	}
}
