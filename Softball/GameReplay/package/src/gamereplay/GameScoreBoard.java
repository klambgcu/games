package com.kelsoft.gamereplay;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import javax.swing.border.BevelBorder;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import com.kelsoft.gamereplay.GameConstants;

/**
 *
 * @author Kelly Lamb
 */
public class GameScoreBoard extends JComponent
{
	public static final int VISITOR = 0;
	public static final int HOME    = 1;
	public static final int RUNS    = 0;
	public static final int HITS    = 1;
	public static final int ERRS    = 2;

	private ImageIcon gameBoard  = null;
	private ImageIcon gameLightB = null;
	private ImageIcon gameLightO = null;
	private ImageIcon gameLightS = null;
	private ImageIcon gameScore  = null;
	private Font nameFont = new Font("Arial", Font.PLAIN, 14);
	private int balls   = 0;
	private int strikes = 0;
	private int outs    = 0;
	private int inning  = 0; // Even=top, Odd=bottom: 0=Top of 1st; 1=Bottom of 1st.
	private int posVLights = 88;
	private int[] posXBalls   = {269,288,307};
	private int[] posXStrikes = {391,410};
	private int[] posXOuts    = {479,498};
	private int[] posXScore   = {0,13,26,39,52,65,78,91,104,117,130,143,156,169,182,195,208,221,234};
	private int[] posYScore   = {0,34};
	private int[] posXInnInd  = {0,24,48,72,96,120,144};
	private int[] posXInns    = {239,263,287,311,335,359,383};
	private int[][] posVals = {{410,423},{447,460},{484,497}};
	private int[][] totVals = {{0,0,0},{0,0,0}};
	private int[][] innRuns = {{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}};
	private String nameHome     = "Our Team!!!!";
	private String nameVisitors = "Our Opponent";
	private SBEffectAbstract sbEffectFM = null;
	private SBEffectAbstract sbEffectHR = null;
	private SBEffectAbstract sbEffectSO = null;
	private SBEffectAbstract sbEffectCurrent = null;
	private int effectNumber = 0;

	public GameScoreBoard ()
	{
		super();
		setOpaque(false);
		gameBoard  = new ImageIcon(getClass().getResource("images/GameBoard.png"));
		gameLightB = new ImageIcon(getClass().getResource("images/GameLightBall.png"));
		gameLightO = new ImageIcon(getClass().getResource("images/GameLightOut.png"));
		gameLightS = new ImageIcon(getClass().getResource("images/GameLightStrike.png"));
		gameScore  = new ImageIcon(getClass().getResource("images/GameScore.png"));
		setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

		Dimension d = new Dimension(gameBoard.getIconWidth()+4, gameBoard.getIconHeight()+4);
		setPreferredSize(d);
		setMaximumSize(d);
		setMinimumSize(d);
		sbEffectFM = new SBEffectFlashMessage(this, true);
		sbEffectSO = new SBEffectStrikeOut(this, true);
		sbEffectHR = new SBEffectHomeRun(this, true);
	}

	@Override public void paintComponent(Graphics g)
	{
		super.paintComponent(g); //paint background
		if ((sbEffectCurrent != null) && (sbEffectCurrent.isAnimating()))
		{
			Graphics2D g2 = (Graphics2D) g;
			sbEffectCurrent.step(0,0);
			sbEffectCurrent.render(0,0,g2);
		}
		else
		{
			render(g);
		}
	}

	private void render(Graphics g)
	{
		int dx1,dx2,dy1,dy2,sx1,sx2,r;

		Insets insets = getInsets();
		int il = insets.left;
		int it = insets.top;
		int displayableInning = Math.min(inning, 13);

		// Draw scoreboard background
		g.drawImage(gameBoard.getImage(), il, it, this);

		Graphics2D g2d = (Graphics2D)g;
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g2d.setFont(nameFont);
		g2d.setColor(Color.BLACK);
		g2d.drawString(nameVisitors, il + 10, it + 41);
		g2d.drawString(nameHome,     il + 10, it + 75);

		g.setColor(Color.RED);
		g.drawRect(il + 237 + posXInnInd[displayableInning>>1], it + 22 + posYScore[displayableInning&1], 16, 26);

		// Draw Inning Runs - Limit to displayable innings (7)
		for (int i=0; i <= displayableInning; i++)
		{
			dx1 = il + posXInns[i>>1];
			dx2 = dx1 + 13;
			dy1 = it + 24 + posYScore[i&1];
			dy2 = dy1 + 23;
			sx1 = innRuns[i&1][i>>1] > 19 ? posXScore[19] : posXScore[innRuns[i&1][i>>1]];
			sx2 = sx1 + 13;
			g.drawImage(gameScore.getImage(),
						dx1, dy1, dx2, dy2,
						sx1, 0,   sx2, 23,
						this);
		}

		// Draw Totals (Visitors/Home) Runs,Hits,Errs
		for (int vh=VISITOR; vh <= HOME; vh++)
			for (int t=RUNS; t <= ERRS; t++)
			{
				if (totVals[vh][t] > 9)
				{
					r = totVals[vh][t] / 10;
					dx1 = il + posVals[t][0];
					dx2 = dx1 + 13;
					dy1 = it + 24 + posYScore[vh];
					dy2 = dy1 + 23;
					sx1 = r > 19 ? posXScore[19] : posXScore[r];
					sx2 = sx1 + 13;
					g.drawImage(gameScore.getImage(),
								dx1, dy1, dx2, dy2,
								sx1, 0,   sx2, 23,
								this);
				}
				r = totVals[vh][t] % 10;
				dx1 = il + posVals[t][1];
				dx2 = dx1 + 13;
				dy1 = it + 24 + posYScore[vh];
				dy2 = dy1 + 23;
				sx1 = posXScore[r];
				sx2 = sx1 + 13;
				g.drawImage(gameScore.getImage(),
							dx1, dy1, dx2, dy2,
							sx1, 0,   sx2, 23,
							this);
			}

		// Light up number of balls
		int lp = insets.top + posVLights;
		for (int b=0; b < balls; b++)
			g.drawImage(gameLightB.getImage(), insets.left + posXBalls[b], lp, this);

		// Light up number of strikes
		for (int s=0; s < strikes; s++)
			g.drawImage(gameLightB.getImage(), insets.left + posXStrikes[s], lp, this);

		// Light up number of outs
		for (int o=0; o < outs; o++)
			g.drawImage(gameLightB.getImage(), insets.left + posXOuts[o], lp, this);
	}

	public boolean isAnimating()
	{
		if ((sbEffectCurrent != null) && (sbEffectCurrent.isAnimating())) return true;
		else return false;
	}

	public void setAnimation(String message)
	{
		if (GameConstants.STRIKE_OUT.equalsIgnoreCase(message))
		{
			sbEffectCurrent = sbEffectSO;
			sbEffectCurrent.stop();
			sbEffectCurrent.reset(0,0);
			sbEffectCurrent.start();
		}
		else if ((GameConstants.HOME_RUN.equals(message)) ||
				 (GameConstants.GRAND_SLAM.equals(message)))
		{
			sbEffectCurrent = sbEffectHR;
			sbEffectCurrent.stop();
			sbEffectCurrent.reset(0,0);
			sbEffectCurrent.setMessage(message);
			sbEffectCurrent.start();
		}
		else
		{
			sbEffectCurrent = sbEffectFM;
			sbEffectCurrent.stop();
			sbEffectCurrent.reset(0,0);
			sbEffectCurrent.setMessage(message);
			sbEffectCurrent.start();
		}
		repaint();
	}

	public void resetAll(Boolean clearNames)
	{
		resetOuts();
		resetBalls();
		resetStrikes();
		inning = 0;
		for (int vh=VISITOR; vh <= HOME; vh++)
		{
			totVals[vh][RUNS] = 0;
			totVals[vh][HITS] = 0;
			totVals[vh][ERRS] = 0;
		}
		for (int i=0; i < innRuns[VISITOR].length; i++)
		{
			innRuns[VISITOR][i] = 0;
			innRuns[HOME][i]    = 0;
		}

		if (clearNames)
		{
			nameHome     = "";
			nameVisitors = "";
		}
		repaint();
	}

	public void resetOuts()
	{
		outs = 0;
	}

	public void resetBalls()
	{
		balls = 0;
	}

	public void resetStrikes()
	{
		strikes = 0;
	}

	public int getOuts()
	{
		return outs;
	}

	public int getBalls()
	{
		return balls;
	}

	public int getStrikes()
	{
		return strikes;
	}

	public void setOuts(int numOuts)
	{
		if (numOuts > 2)
			numOuts = 2;
		outs = numOuts;
	}

	public void setBalls(int numBalls)
	{
		if (numBalls > 3)
			numBalls = 3;
		balls = numBalls;
	}

	public void setStrikes(int numStrikes)
	{
		if (numStrikes > 2)
			numStrikes = 2;
		strikes = numStrikes;
	}

	public void incrementBalls()
	{
		setBalls( getBalls() + 1 );
		repaint();
	}

	public void incrementStrikes()
	{
		setStrikes( getStrikes() + 1 );
		repaint();
	}

	public void incrementOuts()
	{
		setOuts( getOuts() + 1 );
		repaint();
	}

	public void incrementFouls()
	{
		if (getStrikes() < 2)
			incrementStrikes();
	}

	public void incrementScore()
	{
		(innRuns[inning&1][inning>>1])++;
		(totVals[inning&1][RUNS])++;
		repaint();
	}

	public void incrementHits()
	{
		(totVals[inning&1][HITS])++;
		repaint();
	}

	public void incrementErrors()
	{
		(totVals[(1+inning)&1][ERRS])++;
		repaint();
	}

	public void newBatter()
	{
		resetBalls();
		resetStrikes();
		repaint();
	}

	public void advanceInning()
	{
		inning++;
		resetOuts();
		newBatter();
	}

	public void setTeamNames(String visitor, String home)
	{
		nameVisitors = visitor + "                              ".substring(1,30).trim();
		nameHome     = home + "                                 ".substring(1,30).trim();
		repaint();
	}
}
