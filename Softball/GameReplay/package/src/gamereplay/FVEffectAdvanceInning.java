package com.kelsoft.gamereplay;

import java.util.Iterator;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

/**
 *
 * @author Kelly Lamb
 */
public class FVEffectAdvanceInning extends FVEffectAbstract
{
	private int  Scene = -1;
	private int  side;
	private Team team;

	public FVEffectAdvanceInning(JComponent parent, Teams teams)
	{
		super(parent, teams);
		//setSleepAmount(80);
	}

	public void render()
	{
	}

	public void reset()
	{
		stop();
		Scene = -1;
	}

	// This activity has 3 combined steps.
	// 1. Move Teams to Dug Outs (-1..1)
	// 2. Swap defense/offense types (2)
	// 3. Move Defense To Position (3..4)
	public void step()
	{
		if (! isAnimating()) return;
		if (Scene == -1)
		{
			Scene++;
			for (Player p : teams.getVisitorTeam().getRoster())
			{
				p.getPlayerView().setDestinationSteps( (double)GameConstants.locDugOut[GameConstants.TEAM_VISITOR][0],
													   (double)GameConstants.locDugOut[GameConstants.TEAM_VISITOR][1],
													   25 );
			}

			for (Player p : teams.getHomeTeam().getRoster())
			{
				p.getPlayerView().setDestinationSteps( (double)GameConstants.locDugOut[GameConstants.TEAM_HOME][0],
													   (double)GameConstants.locDugOut[GameConstants.TEAM_HOME][1],
													   25 );
			}
		}
		else if (Scene == 0)
		{
			for (Player p : teams.getVisitorTeam().getRoster())
			{
				p.getPlayerView().moveToDestination();
			}

			for (Player p : teams.getHomeTeam().getRoster())
			{
				if ( p.getPlayerView().moveToDestination() )
					Scene = 1;
			}
		}
		else if (Scene == 1)
		{
			for (Player p : teams.getVisitorTeam().getRoster())
			{
				p.getPlayerView().setVisible(false);
			}

			for (Player p : teams.getHomeTeam().getRoster())
			{
				p.getPlayerView().setVisible(false);
			}
			Scene++;
		}
		else if (Scene == 2)
		{
			int vType = teams.getVisitorTeam().getType();
			int hType = teams.getHomeTeam().getType();
			teams.getVisitorTeam().setType(hType);
			teams.getHomeTeam().setType(vType);
			Scene++;
		}
		else if (Scene == 3)
		{
			Scene++;

			if (teams.getVisitorTeam().getType() == GameConstants.PLAYER_TYPE_DEFENSE)
			{
				side = GameConstants.TEAM_VISITOR;
				team = teams.getVisitorTeam();
			}
			else
			{
				side = GameConstants.TEAM_HOME;
				team = teams.getHomeTeam();
			}

			for (Player p : team.getRoster())
			{
				int pos = p.getPosition();
				if (pos > 0)
				{
					p.getPlayerView().setDestinationSteps( (double)GameConstants.locDefense[pos][0],
														   (double)GameConstants.locDefense[pos][1],
														   25 );
					p.getPlayerView().setNameAlignment(GameConstants.PLAYER_TYPE_DEFENSE);
					p.getPlayerView().setVisible(true);
				}
			}
		}
		else if (Scene == 4)
		{
			for (Player p : team.getRoster())
			{
				int pos = p.getPosition();
				if (pos > 0)
				{
					if (p.getPlayerView().moveToDestination())
						Scene = 5;
				}
			}
		}
		else
		{
			Scene = -1;
			stop();
		}
	}

	public void setMessage(String Message)
	{
		message = Message;
	}
}
