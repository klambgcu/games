package com.kelsoft.gamereplay;

import java.net.URL;
import javax.swing.ImageIcon;

public class Player
{
	private ImageIcon  picture;
	private int        originalPosition = -99;
	private int        position;
	private int        batPosition = 0;
	private String     number;
	private String     name;
	private int        hand;
	private Team       team;
	private PlayerView view;

	///////////////////////////////////////////////////
	// CONSTRUCTORS
	///////////////////////////////////////////////////
	Player( String pos, String num, String name)
	{
		this( null, pos, num, name, "R" );
	}

	Player( Team team, String pos, String num, String name)
	{
		this( team, pos, num, name, "R" );
	}

	Player( Team team, String pos, String num, String name, String hand)
	{
		this.team = team;
		setPosition( pos );
		setNumber( num );
		setName( name );
		setHand( hand );
	}

	Player( Player p )
	{
		team = p.getTeam();
		setPosition( String.valueOf( p.getPosition() ) );
		setNumber( p.getNumber() );
		setName( p.getName() );
	}

	///////////////////////////////////////////////////
	// ACCESSORS
	///////////////////////////////////////////////////
	public int getBatPosition()
	{
		return batPosition;
	}

	public int getHand()
	{
		return hand;
	}

	public PlayerView getPlayerView()
	{
		return view;
	}

	public int getPosition()
	{
		return position;
	}

	public String getNumber()
	{
		return number;
	}

	public String getName()
	{
		return name;
	}

	public String getNumberName()
	{
		String result;
		if ( "".equals( getNumber().trim() ) )
			result = " ";
		else
		{
			if ( "".equals(getName().trim()) )
				result = String.format(" #%s ", getNumber().trim());
			else
				result = String.format(" #%s %s ", getNumber().trim(), getName().trim() );
		}
		return result;
	}

	public ImageIcon getPicture()
	{
		return this.picture;
	}

	public Team getTeam()
	{
		return team;
	}

	///////////////////////////////////////////////////
	// MUTATORS
	///////////////////////////////////////////////////
	public void setBatPosition( int p )
	{
		this.batPosition = p;
	}

	public void setHand ( String h )
	{
		if ( "R".equalsIgnoreCase(h) )
			this.hand = GameConstants.PLAYER_RIGHTHAND;
		else
			this.hand = GameConstants.PLAYER_LEFTHAND;
	}

	public void setPlayerView( PlayerView view )
	{
		this.view = view;
	}

	public void setPicture(boolean b)
	{
		try
		{
			if (b)
			{
				String pn ;
				if (this.number.trim().length() < 2)
					pn = "0" + this.number.trim();
				else
					pn = this.number.trim();

				URL url = new URL("http://www.impactallstars2011.webs.com/Roster/" + pn + ".jpg");

				if (url.getContent() != null)
				{
					picture = new ImageIcon(url);
				}
				else
				{
					picture = GameImages.getPlaceHolder();
				}
			}
			else
			{
				picture = GameImages.getPlaceHolder();
			}
		}
		catch (Exception e)
		{
			picture = GameImages.getPlaceHolder();
		}
	}

	public void setPosition( String p )
	{
		if ( (p == null) || ( "".equals(p) ) )
			this.position = 0;
		else
			this.position = Integer.parseInt( p );
		if (this.originalPosition == -99)
			this.originalPosition = this.position;
	}

	public void setNumber( String n )
	{
		this.number = n;
	}

	public void setName( String n )
	{
		this.name = n;
	}

	public void setTeam( Team team )
	{
		this.team = team;
	}

	public void resetPosition()
	{
		this.position = this.originalPosition;
	}

	///////////////////////////////////////////////////
	// UTILITIES
	///////////////////////////////////////////////////
	@Override
	public String toString()
	{
		return String.format("Pos[%d], Number[%s], Name[%s]\r\n", this.getPosition(), this.getNumber(), this.getName() );
	}
} // End of class Player
