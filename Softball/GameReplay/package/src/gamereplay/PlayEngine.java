package com.kelsoft.gamereplay;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import javax.swing.filechooser.*;
import javax.swing.JFileChooser;
import com.kelsoft.gamereplay.events.*;

public class PlayEngine
{
	public static int RUNS = 0;
	public static int HITS = 1;
	public static int ERRS = 2;
	public static int LOBS = 3;

	public static final String[] positionName = {"home - Catcher", "Pitcher", "Catcher", "1B", "2B", "3B", "SS", "LF", "CF","RF"};

	private static final HashMap<String, String> hitType  = new HashMap<String, String>();

	private boolean   firstTeam   = true;
	private FieldView field;
	private int       inning      = 0;
	private int       outNumber   = 0;
	private int       playNumber  = 0;
	private int       summary[]   = {0,0,0,0};
	private int       sumTots[][] = {{0,0,0,0},{0,0,0,0}};
	private int       teamAtBat   = GameConstants.TEAM_VISITOR;
	private String    scores[][]  = new String[2][15];
	private Teams     teams;

	private GameScoreBoard scoreBoard;
	private ArrayList<Command> GameEvents = new ArrayList<Command>();

	private StringBuffer pitches = new StringBuffer();

	public PlayEngine(GameScoreBoard scoreBoard, FieldView field, Teams teams)
	{
		hitType.put("1B",  "single");
		hitType.put("2B",  "double");
		hitType.put("3B",  "triple");
		hitType.put("BB",  "walks");
		hitType.put("B",   "bunt");
		hitType.put("BUNT","bunt");
		hitType.put("F",   "fly ball");
		hitType.put("FO",  "fly out");
		hitType.put("G",   "grounder");
		hitType.put("GO",  "ground out");
		hitType.put("GS",  "grand slam");
		hitType.put("HB",  "home run");
		hitType.put("HR",  "home run");
		hitType.put("L",   "line drive");
		hitType.put("LD",  "line drive");
		hitType.put("LO",  "line drive out");
		hitType.put("SACB","sacrifice bunt");
		hitType.put("SACF","sacrifice fly out");

		for (int r = 0; r < 2; r++)
			for (int c = 0; c < 15; c++)
				scores[r][c] = " x";

		this.scoreBoard = scoreBoard;
		this.field      = field;
		this.teams      = teams;
	}

	public void processFile(File file)
	{
		BufferedReader reader = null;

		try
		{
			reader = new BufferedReader(new FileReader(file));
			String text = null;

			// Repeat until all lines are read
			while ((text = reader.readLine()) != null)
			{
				if (text.startsWith("#") || text.length() == 0)
				{
					continue; // Ignore comment and empty lines
				}
				else if (Character.isDigit(text.charAt(0)) || text.startsWith("?"))
				{
					processAtBat(text);
				}
				else
				{
					processCommands(text);
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			teams.resetPosition();
			try
			{
				if (reader != null)
				{
					reader.close();
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public Iterator<Command> getGameEvents()
	{
		return GameEvents.iterator();
	}

	public void processCommands(String command)
	{
		//System.out.printf("Processing a command.\t%s\r\n", command);
		String[] tokens = command.split("\t"); // Commands are tab delimited.
		String token = tokens[0];

		if ("TEAMNAMEH".equals(token))
		{
			Team team = new Team();
			team.setName( tokens[1] );
			if (tokens.length >= 3)
				team.setColor( tokens[2].trim().toUpperCase() );
			else
				team.setColor("BLUE");
			team.setShowPix( firstTeam );
			firstTeam = false;

			teams.setHomeTeam( team );
		}
		else if ("TEAMNAMEV".equals(token))
		{
			Team team = new Team();
			team.setName( tokens[1] );
			if (tokens.length >= 3)
				team.setColor( tokens[2].trim().toUpperCase() );
			else
				team.setColor("RED");
			team.setShowPix( firstTeam );
			firstTeam = false;
			teams.setVisitorTeam( team );
		}
		else if ("SLH".equals(token))
		{
			Player p;
			if (tokens.length == 4)
				p = new Player( teams.getHomeTeam(), tokens[1], tokens[2], tokens[3] );
			else
				p = new Player( teams.getHomeTeam(), tokens[1], tokens[2], tokens[3], tokens[4] );
			teams.addPlayer( GameConstants.TEAM_HOME, p );
		}
		else if ("SLV".equals(token))
		{
			Player p;
			if (tokens.length == 4)
				p = new Player( teams.getVisitorTeam(), tokens[1], tokens[2], tokens[3] );
			else
				p = new Player( teams.getVisitorTeam(), tokens[1], tokens[2], tokens[3], tokens[4] );
			teams.addPlayer( GameConstants.TEAM_VISITOR, p );
		}
		else if ("SH".equals(token))
		{
			String format = teams.substitutePlayer( GameConstants.TEAM_HOME, tokens[1], tokens[2], tokens[3] );
			System.out.printf( format, teams.getTeamName( GameConstants.TEAM_HOME ), positionName[ Integer.parseInt(tokens[1]) ] );
		}
		else if ("SV".equals(token))
		{
			String format = teams.substitutePlayer( GameConstants.TEAM_VISITOR, tokens[1], tokens[2], tokens[3] );
			System.out.printf( format, teams.getTeamName( GameConstants.TEAM_VISITOR ), positionName[ Integer.parseInt(tokens[1]) ] );
		}
		else if ("PR".equals(token))
		{
			String format = teams.substituteRunner( teamAtBat, tokens[1], tokens[2] );
			System.out.printf( format, teams.getTeamName( teamAtBat ) );
		}
		else if ("INN".equals(token))
		{
			if ("T".equals(tokens[1]))
			{
				outNumber = 0;
				int inn = Integer.valueOf(tokens[2].trim());
				System.out.printf("Inning %d\r\n", inn);
				System.out.printf("\tTop: %s at bat.\r\n", teams.getTeamName( GameConstants.TEAM_VISITOR ));
				inning = inn;
				teamAtBat = GameConstants.TEAM_VISITOR;
				if ( inn == 1 )
					GameEvents.add( new EventPlayBall( scoreBoard ) );
				else
					GameEvents.add( new EventAdvanceInning( scoreBoard ) );
			}
			else
			{
				outNumber = 0;
				System.out.printf("\tBottom: %s at bat.\r\n", teams.getTeamName( GameConstants.TEAM_HOME ));
				teamAtBat = GameConstants.TEAM_HOME;
				GameEvents.add( new EventAdvanceInning( scoreBoard ) );
			}
		}
		else if ("GAMEENDS.".equals(token))
		{
			System.out.println("Game Ends.");
			GameEvents.add( new EventGameOver( scoreBoard ) );
		}
		else
		{
			System.out.println("Unknown Command: " + command);
		}
	}

	private void processAtBat(String atbat)
	{
		boolean atBatFinished = false;
		pitches.delete(0, pitches.length());

		 // Atbats are comma delimited
		String[] tokens = atbat.split(",");

		playNumber++;
		String playerNumber = tokens[0].split("\t")[0];

		GameEvents.add( new EventNewBatter( scoreBoard, field, teams.getPlayer(teamAtBat, playerNumber) ) );

		// Remove number and reassign to beginning of token
		tokens[0] = tokens[0].split("\t")[1];
		System.out.printf("\t\tPlay %d:%s",
						  playNumber,
						  teams.getPlayer(teamAtBat, playerNumber).getNumberName() );

		for (String token : tokens)
		{
			// Check for embedded running events.
			if (token.contains("\t"))
			{
				if (! atBatFinished)
					System.out.print("at bat, ");

				processRunningEvent(token);

				if ((! atBatFinished) && (outNumber < 3))
				{
					playNumber++;
					System.out.printf("\r\n\t\tPlay %d:%s",
									  playNumber,
									  teams.getPlayer(teamAtBat, playerNumber).getNumberName() );
				}
			}
			else if ("b".equals(token) || "bp".equals(token) || "bw".equals(token) || "bwp".equals(token))
			{
				pitches.append("b"); // handle balls (wild, passed balls)
				GameEvents.add( new EventPitchBall( scoreBoard, field ) );
			}
			else if ("s".equals(token) || "sp".equals(token) || "sw".equals(token) || "swp".equals(token))
			{
				pitches.append("s"); // handle swinging strikes (wild, passed balls)
				GameEvents.add( new EventPitchStrikeSwinging( scoreBoard, field ) );
			}
			else if ("c".equals(token) || "cp".equals(token))
			{
				pitches.append("c"); // handle called strikes (passed balls, does not make sense for wild)
				GameEvents.add( new EventPitchStrikeCalled( scoreBoard, field ) );
			}
			else if ("f".equals(token))
			{
				pitches.append("f"); // handle foul balls
				GameEvents.add( new EventPitchFoul( scoreBoard, field ) );
			}
			else if ("K".equals(token))
			{
				outNumber++;
				atBatFinished = true;
				String type = (pitches.toString().endsWith("c")) ? "looking" : "swinging";
				System.out.printf("strikes out %s (%s). Out %d.", type, pitches.toString(), outNumber);
				GameEvents.add( new EventOutStrikeOut( scoreBoard, field ) );
			}
			else if ("BB".equals(token))
			{
				atBatFinished = true;
				System.out.printf("walks (%s).", pitches.toString());
				GameEvents.add( new EventBatWalk( scoreBoard, field, teams.getPlayer(teamAtBat, playerNumber) ) );
			}
			else if ("HBP".equals(token))
			{
				atBatFinished = true;
				pitches.append("b");
				System.out.printf("is hit by the pitch (%s).", pitches.toString());
				GameEvents.add( new EventBatHitByPitch( scoreBoard, field, teams.getPlayer(teamAtBat, playerNumber) ) );
			}
			else if (token.startsWith("D3R"))
			{
				outNumber++;
				atBatFinished = true;
				String msg = "";
				if (token.contains("-"))
				{
					int pos1 = Integer.valueOf(token.substring(3,4));
					int pos2 = Integer.valueOf(token.substring(5,6));
					msg = String.format("is thrown out at %s%sby %s%safter a dropped 3rd strike (%s). Out %d.",
										positionName[pos2],
										teams.getPlayer(1-teamAtBat, pos2).getNumberName(),
										positionName[pos1],
										teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
										pitches.toString(),
										outNumber);
				}
				else
				{
					msg = String.format("is tagged out by %s%safter a dropped 3rd strike (%s). Out %d.",
										positionName[2], // Catcher position by definition.
										teams.getPlayer(1-teamAtBat, 2).getNumberName(),
										pitches.toString(),
										outNumber);
				}
				System.out.print(msg);
			}
			else if (token.startsWith("D3C"))
			{
				// Double Error - Dropped 3rd strike and error on catch by position
				// D3C[1-9]
				atBatFinished = true;
				token = token.replace("D3C","");
				int pos1 = Integer.valueOf(token);
				System.out.printf("reaches base on double error after a dropped 3rd strike from %s%sand a missed catch on throw from catcher by %s%s(%s).",
								  positionName[2], // Catcher position by definition.
								  teams.getPlayer(1-teamAtBat, 2).getNumberName(),
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  pitches.toString());
				(summary[ERRS])++;
				(summary[ERRS])++;
				GameEvents.add( new EventFieldingError( scoreBoard ) );
				GameEvents.add( new EventFieldingError( scoreBoard ) );
			}
			else if (token.startsWith("D3T"))
			{
				// Double Error - Dropped 3rd strike and error on throw by catcher
				// D3T[1-9]
				atBatFinished = true;
				token = token.replace("D3T","");
				int pos1 = Integer.valueOf(token);
				System.out.printf("reaches base on double error after a dropped 3rd strike from %s%sand a wild throw to %s%s(%s).",
								  positionName[2], // Catcher position by definition.
								  teams.getPlayer(1-teamAtBat, 2).getNumberName(),
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  pitches.toString());
				(summary[ERRS])++;
				(summary[ERRS])++;
				GameEvents.add( new EventFieldingError( scoreBoard ) );
				GameEvents.add( new EventFieldingError( scoreBoard ) );
			}
			else if (token.startsWith("D3"))
			{
				atBatFinished = true;
				System.out.printf("reaches base on error after a dropped 3rd strike from %s%s(%s).",
								  positionName[2], // Catcher position by definition.
								  teams.getPlayer(1-teamAtBat, 2).getNumberName(),
								  pitches.toString());
				(summary[ERRS])++;
				GameEvents.add( new EventFieldingError( scoreBoard ) );
			}
			else if (token.startsWith("LOB"))
			{
				// LOB,# delimited player numbers
				token = token.replace("LOB#","").trim();
				String[] pNums = token.split("#");
				if (pNums.length > 0)
				{
					System.out.print(" ");
					for (int i = 0; i < pNums.length; i++)
					{
						System.out.printf("%s%s", teams.getPlayer(teamAtBat, pNums[i]).getNumberName().trim(), (i < pNums.length-1) ? ", " : " ");
					}
					System.out.printf("%s left on base.", (pNums.length > 1) ? "are" : "is" );
				}
				summary[LOBS] = pNums.length;
			}
			else if (token.startsWith("FO"))
			{
				outNumber++;
				atBatFinished = true;
				processHitOut("FO", token);
			}
			else if (token.startsWith("GO"))
			{
				outNumber++;
				atBatFinished = true;
				processHitOut("GO", token);
			}
			else if (token.startsWith("LO"))
			{
				outNumber++;
				atBatFinished = true;
				processHitOut("LO", token);
			}
			else if (token.startsWith("F"))
			{
				atBatFinished = true;
				processHit("F", token, playerNumber);
			}
			else if (token.startsWith("G"))
			{
				atBatFinished = true;
				processHit("G", token, playerNumber);
			}
			else if (token.startsWith("LD"))
			{
				atBatFinished = true;
				processHit("LD",token, playerNumber);
			}
			else if (token.startsWith("RBI"))
			{
				// RBI,count - used in conjunction and prior to SCORE
				token = token.replace("RBI","").trim();
				int count = Integer.valueOf(token);
				System.out.printf(" %d RBI%s", count, (count > 1) ? "s," : ",");
			}
			else if (token.startsWith("RFC"))
			{
				// RFC[G,B][1-9][H,[-?B[1-9]]
				// RFCG6-3B5
				// RFCB6H
				atBatFinished = true;
				String type = (token.substring(3,4).equals("B")) ? "bunt" : "grounder";
				int pos1 = Integer.valueOf(token.substring(4,5));
				String msg = "is thrown";
				if (token.substring(5,6).equals("H"))
				{
					msg = "is held ";
				}
				else
				{
					String base = token.substring(6,8);
					base = (base.equals("HB")) ? "home" : base;
					int pos2 = Integer.valueOf(token.substring(8,9));
					msg = String.format("is thrown to %s%s",
										base,
										teams.getPlayer(1-teamAtBat, pos2).getNumberName());
				}

				System.out.printf("reaches base on fielder's choice as %s to %s%s%s(%s).",
								  type,
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  msg,
								  pitches.toString());
			}
			else if (token.startsWith("ROE"))
			{
				// ROE | hitType [B,G,F,L] | errType [C,M,T] | Position [1-9] |[- base ?B [Position 1-9]
				// Example: ROEBM1:   reaches base on error as bunt to pitcher is mishandled.
				// Example: ROEGT6-1B4: reaches base on error as grounder to SS is thrown wild to 1B (name of 2B).
				atBatFinished = true;
				String msg = "";
				String type = hitType.get(token.substring(3,4));
				String err = token.substring(4,5);
				int pos1 = Integer.valueOf(token.substring(5,6));
				if (token.contains("-")) // Not M errtype
				{
					String base = token.substring(7,9);
					base = (base.equals("HB")) ? "home" : base;
					int pos2 = Integer.valueOf(token.substring(9,10));
					if ("T".equals(err)) // Throwing errtype
					{
						msg = String.format("is thrown wildly to %s%s.",
											base,
											teams.getPlayer(1-teamAtBat, pos2).getNumberName());
					}
					else // Catching errtype
					{
						msg = String.format("is thrown to %s%sand missed.",
											base,
											teams.getPlayer(1-teamAtBat, pos2).getNumberName());
					}
				}
				else // Mishandled errtype
				{
					msg = "is mishandled.";
				}
				System.out.printf("reaches base on error as %s to %s%s%s",
								  type,
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  msg
								  );
				System.out.printf(" (%s).", pitches.toString());
				(summary[ERRS])++;
				GameEvents.add( new EventFieldingError( scoreBoard ) );
			}
			else if (token.startsWith("SAC"))
			{
				atBatFinished = true;
				outNumber++;
				String type = hitType.get( token.substring(0,4) );
				token = token.replace("SACB","").replace("SACF","");
				int pos  = Integer.valueOf(token.substring(0,1));
				System.out.printf("hits a %s to %s%s (%s). Out %d.",
								  type,
								  positionName[pos],
								  teams.getPlayer(1-teamAtBat, pos).getNumberName(),
								  pitches.toString(),
								  outNumber);
			}
			else if (token.startsWith("SCORE"))
			{
				// SCORE,# delimited player numbers - used in conjunction and after RBI
				// Example: RBI3, SCORE#1#2#4
				token = token.replace("SCORE#","").trim();
				String[] pNums = token.split("#");
				if (pNums.length > 0)
				{
					System.out.print(" ");
					for (int i = 0; i < pNums.length; i++)
					{
						System.out.printf("%s%s", teams.getPlayer(teamAtBat, pNums[i]).getNumberName().trim(), (i < pNums.length-1) ? ", " : " ");
						GameEvents.add( new EventBatScore( scoreBoard ) );
					}
					System.out.printf("%s.", (pNums.length > 1) ? "all score" : "scores" );
					summary[RUNS] += pNums.length;
				}
			}
			else if (token.startsWith("BUNT"))
			{
				atBatFinished = true;
				processHit("BUNT", token, playerNumber);
			}
			else if (token.startsWith("MSG:"))
			{
				token = token.replace("MSG:","");
				System.out.print(" (" + token + ") ");
			}
			else
			{
				System.out.printf("*** Unknown at-bat token (%s) *** ", token);
			}
		}
		System.out.println();
	}

	private void processHit(String type, String event, String playerNumber)
	{
		(summary[HITS])++;
		event = event.replace(type, "");
		int pos = Integer.valueOf(event.substring(0,1));
		String hType = hitType.get(event.substring(1));
		System.out.printf("hits a %s towards %s%sfor a %s (%s).",
						  hitType.get(type),
						  positionName[pos],
						  teams.getPlayer(1-teamAtBat, pos).getNumberName(),
						  hType,
						  pitches.toString());

		if (hType.equals("single"))
			GameEvents.add( new EventBatSingle(    scoreBoard, field, teams.getPlayer(teamAtBat, playerNumber), type, pos ) );
		else if (hType.equals("double"))
			GameEvents.add( new EventBatDouble(    scoreBoard, field, teams.getPlayer(teamAtBat, playerNumber), type, pos ) );
		else if (hType.equals("triple"))
			GameEvents.add( new EventBatTriple(    scoreBoard, field, teams.getPlayer(teamAtBat, playerNumber), type, pos ) );
		else if (hType.equals("home run"))
			GameEvents.add( new EventBatHomeRun(   scoreBoard, field, teams.getPlayer(teamAtBat, playerNumber), type, pos ) );
		else if (hType.equals("grand slam"))
			GameEvents.add( new EventBatGrandSlam( scoreBoard, field, teams.getPlayer(teamAtBat, playerNumber), type, pos ) );
	}

	private void processHitOut(String type, String event)
	{
		int    pos1     = 0;
		String throwOut = "";

		event = event.replace(type, "");
		int pos = Integer.valueOf(event.substring(0,1));
		if (event.contains("-"))
		{
			pos1 = Integer.valueOf(event.substring(event.indexOf("-")+1));
			throwOut = String.format("and is thrown out at %s%s",
									 positionName[pos1],
									 teams.getPlayer(1-teamAtBat, (pos1 < 1) ? 2 : pos1).getNumberName());
		}
		System.out.printf("hits a %s to %s%s%s(%s). Out %d.",
						  hitType.get(type),
						  positionName[pos],
						  teams.getPlayer(1-teamAtBat, pos).getNumberName(),
						  throwOut,
						  pitches.toString(),
						  outNumber);
		if (type.equals("FO"))
			GameEvents.add( new EventOutFlyOut( scoreBoard, field, pos ) );
		else if (type.equals("LO"))
			GameEvents.add( new EventOutLineOut( scoreBoard, field, pos ) );
		else if (type.equals("GO"))
		{
			if (pos1 == 0) pos1 = pos; // handle unassisted play
			GameEvents.add( new EventOutGroundOut( scoreBoard, field, pos, pos1 ) );
		}
	}

	private void processRunningEvent(String events)
	{
		String[] plays = events.split(";"); // Multiple events are semicolon delimited
		for (String play : plays)
		{
			String[] tokens = play.split("\t"); // Events are tab delimited - name/action
			String playerNumber = tokens[0];
			String event = tokens[1];
			System.out.printf("%s", teams.getPlayer(teamAtBat, playerNumber).getNumberName() );

			if ("S2B".equals(event))
			{
				System.out.print("steals 2B.");
				GameEvents.add( new EventRunSteal( scoreBoard ) );
			}
			else if ("S3B".equals(event))
			{
				System.out.print("steals 3B.");
				GameEvents.add( new EventRunSteal( scoreBoard ) );
			}
			else if ("SHB".equals(event))
			{
				System.out.print("steals home and scores.");
				(summary[RUNS])++;
				GameEvents.add( new EventRunSteal( scoreBoard ) );
				GameEvents.add( new EventBatScore( scoreBoard ) );
			}
			else if (event.startsWith(">"))
			{
				String plate = event.substring(1,3);
				String msg = "";
				if (plate.equals("HB"))
				{
					(summary[RUNS])++;
					plate = "home and scores";
					GameEvents.add( new EventBatScore( scoreBoard ) );
				}
				if (event.length() > 3)
				{
					String how = event.substring(3);
					if (how.startsWith("OTE"))
					{
						int pos = Integer.valueOf(how.substring(3,4));
						msg = String.format(" on throwing error by %s%s",
											positionName[pos],
											teams.getPlayer(1-teamAtBat, pos).getNumberName());
						(summary[ERRS])++;
						GameEvents.add( new EventFieldingError( scoreBoard ) );
					}
					else if (how.startsWith("OCE"))
					{
						int pos = Integer.valueOf(how.substring(3,4));
						msg = String.format(" on catching error by %s%s",
											positionName[pos],
											teams.getPlayer(1-teamAtBat, pos).getNumberName());
						(summary[ERRS])++;
						GameEvents.add( new EventFieldingError( scoreBoard ) );
					}
				}
				System.out.printf("advances to %s%s.", plate, msg);
			}
			else if (event.startsWith("APO"))
			{
				// APO?B?-?
				String plate = event.substring(3,5);
				plate = (plate.equals("HB")) ? "home" : plate;
				int pos1 = Integer.valueOf(event.substring(5,6));
				int pos2 = Integer.valueOf(event.substring(7,8));
				System.out.printf("is almost picked off at %s on throw from %s%sto %s%s.",
								  plate,
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  positionName[pos2],
								  teams.getPlayer(1-teamAtBat, pos2).getNumberName());
			}
			else if (event.startsWith("PO"))
			{
				// PO?B?-?
				outNumber++;
				String plate = event.substring(2,4);
				plate = (plate.equals("HB")) ? "home" : plate;
				int pos1 = Integer.valueOf(event.substring(4,5));
				int pos2 = Integer.valueOf(event.substring(6,7));
				System.out.printf("is picked off at %s on throw from %s%sto %s%sOut %d.",
								  plate,
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  positionName[pos2],
								  teams.getPlayer(1-teamAtBat, pos2).getNumberName(),
								  outNumber);
				GameEvents.add( new EventRunPickedOff( scoreBoard ) );
			}
			else if (event.startsWith("TO"))
			{
				// TO?B?-?
				outNumber++;
				String plate = event.substring(2,4);
				plate = (plate.equals("HB")) ? "home" : plate;
				int pos1 = Integer.valueOf(event.substring(4,5));
				int pos2 = Integer.valueOf(event.substring(6,7));
				System.out.printf("is thrown out at %s from %s%sto %s%sOut %d.",
								  plate,
								  positionName[pos1],
								  teams.getPlayer(1-teamAtBat, pos1).getNumberName(),
								  positionName[pos2],
								  teams.getPlayer(1-teamAtBat, pos2).getNumberName(),
								  outNumber);
				GameEvents.add( new EventFieldingOut( scoreBoard ) );
			}
			else
			{
				System.out.printf(" *** Unknown Running Event (%s) *** ", event);
			}
		}
	}
}