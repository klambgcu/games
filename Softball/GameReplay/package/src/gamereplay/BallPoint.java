package com.kelsoft.gamereplay;

public class BallPoint
{
	private double dstX;
	private double dstY;
	private int    steps;
	private int    type;

	///////////////////////////////////////////////////
	// CONSTRUCTORS
	///////////////////////////////////////////////////
	public BallPoint()
	{
		this(0.0, 0.0, 0, GameConstants.BALLPATH_NORMAL);
	}

	public BallPoint(double dx, double dy, int s)
	{
		this(dx, dy, s, GameConstants.BALLPATH_NORMAL);
	}

	public BallPoint(double dx, double dy, int s, int t)
	{
		this.dstX  = dx;
		this.dstY  = dy;
		this.steps = s;
		this.type  = t;
	}

	public BallPoint(BallPoint b)
	{
		this( b.getDstX(), b.getDstY(), b.getSteps(), b.getType() );
	}

	///////////////////////////////////////////////////
	// MUTATORS
	///////////////////////////////////////////////////
	public void setDstX(double x)
	{
		this.dstX = x;
	}

	public void setDstY(double y)
	{
		this.dstY = y;
	}

	public void setSteps(int s)
	{
		this.steps = s;
	}

	public void setType(int t)
	{
		this.type = t;
	}

	///////////////////////////////////////////////////
	// ACCESSORS
	///////////////////////////////////////////////////
	public double getDstX()
	{
		return dstX;
	}

	public double getDstY()
	{
		return dstY;
	}

	public int getSteps()
	{
		return steps;
	}

	public int getType()
	{
		return type;
	}
}