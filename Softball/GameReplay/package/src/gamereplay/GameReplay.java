package com.kelsoft.gamereplay;

//=============================================================================
// Author: Kelly Lamb
// Date:   12/20/2010
// Title:  GameReplay
// Description:
//    Create an application that can display a softball game in a replay format
//    using animation to display a beautiful scoreboard with special effects and
//    field with player interaction for defense and offense.  Should look some-
//    thing like a video game when it is completed. :)
//=============================================================================

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.Toolkit;
import java.io.File;
import java.util.Iterator;
import javax.swing.filechooser.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import javax.swing.UIManager;
import com.kelsoft.gamereplay.events.*;

public class GameReplay extends JFrame implements ActionListener
{
	private GameScoreBoard scoreBoard;
	private FieldView      field;
	private PlayEngine     engine;
	private Teams          teams = new Teams();
	private Iterator       gameEvents;
	private Timer          timer;
	private GameImages     tempGI = new GameImages();

	public GameReplay()
	{
		super("GameReplay - Kelly Lamb");
		initGUI();
		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		setResizable(true);
		pack();

		engine = new PlayEngine( scoreBoard, field, teams );
		engine.processFile( new File("Test.txt") );
		gameEvents = engine.getGameEvents();
		scoreBoard.setTeamNames( teams.getTeamName( GameConstants.TEAM_VISITOR ),
								 teams.getTeamName( GameConstants.TEAM_HOME ) );

		field.setTeams(teams);

		timer = new Timer(500,this);
		timer.setInitialDelay(1000);
		timer.start();
	}

	private static int count = 0;

	public void actionPerformed(ActionEvent e)
	{
		if (count == 0)
		{
			if (field.isAnimating()) return;
			if (! field.isCompleted())
			{
				field.IntroduceLineUps();
				count++;
			}
			return;
		}
		else if (count == 1)
		{
			if (! field.isAnimating())
			{
				field.moveTeamsToDugOut();
				count++;
			}
			return;
		}
		else if (count == 2)
		{
			if (! field.isCompleted())
				return;
			count++;
		}
		else if (count == 3)
		{
			if (! field.isAnimating())
			{
				field.moveDefenseToPosition();
				count++;
			}
			return;
		}
		else if (count == 4)
		{
			if (! field.isCompleted())
				return;
			count++;
		}

		if (scoreBoard.isAnimating()) return;
		if (field.isAnimating()) return;
		if (field.isBallAnimating()) return;
		if (field.isRunnersAnimating()) return;

		if ( gameEvents.hasNext() )
		{
			Command action = (Command) gameEvents.next();

			if (action instanceof EventAdvanceInning)
				field.advanceInning();

			action.execute();
		}
		else
		{
			// Repeat
			scoreBoard.resetAll(false);
			gameEvents = engine.getGameEvents();
		}
	}

	private void initGUI()
	{
		setIconImage(new ImageIcon(getClass().getResource("images/LogoIcon.png"), "Softball Stats LogoIcon").getImage());
		setJMenuBar(createMenu(this));

		scoreBoard = new GameScoreBoard();
		scoreBoard.setTeamNames("Visitors", "Impact All-Stars");

		field = new FieldView();

		add( BorderLayout.NORTH, scoreBoard );
		add( BorderLayout.SOUTH, field );
	}

	private JMenuBar createMenu(JFrame frm)
	{
		final JFrame frame = frm;
		JMenuBar menuBar = new JMenuBar();

		/***********************/
		/* Menu Section - FILE */
		/***********************/
		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic('F');

		JMenuItem itemFileOpen = new JMenuItem("Open...");
		itemFileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		itemFileOpen.addActionListener(new FileOpenHandler(frame));
		menuFile.add(itemFileOpen);

		menuFile.addSeparator();

		JMenuItem itemFileExit = new JMenuItem("Exit...");
		itemFileExit.setMnemonic('x');
		itemFileExit.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					System.exit(0);
				}
			}
		);
		menuFile.add(itemFileExit);

		menuBar.add(menuFile);

		/***********************/
		/* Menu Section - HELP */
		/***********************/
		JMenu menuHelp = new JMenu("Help");
		menuHelp.setMnemonic('H');

		JMenuItem itemHelpHelp = new JMenuItem("Help...");
		itemHelpHelp.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		itemHelpHelp.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					JOptionPane.showMessageDialog(frame,
												  "1. Load a game containing play by play information.\n" +
												  "2. Sit back and enjoy the game. :)\n" +
												  "-----------------------------------------------------------------------------------------------\n" +
												  "Troubleshooting steps\n" +
												  "1. Make sure you choose a file that contains play by play information.\n" +
												  "2. Make sure you have the minimum java version installed correctly.\n" +
												  "3. Contact customer support at Kelly Lamb Software.\n" +
												  "   Provide the application version, java version and play by play file.",
												  "Help",
												  JOptionPane.INFORMATION_MESSAGE);
				}
			}
		);

		menuHelp.add(itemHelpHelp);

		menuHelp.addSeparator();

		JMenuItem itemHelpAbout = new JMenuItem("About...");
		itemHelpAbout.setMnemonic('A');
		itemHelpAbout.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					JOptionPane.showMessageDialog(frame,
												  "GameReplay 1.0.0\n" +
												  "Date: 12/20/2010\n" +
												  "Copyright: 2010 - 2050 Kelly Lamb Software\n" +
												  "All Rights Reserved\n" +
												  "-------------------------------------------------\n" +
												  "Requires Java 1.6.0.18 or higher\n",
												  "About",
												  JOptionPane.INFORMATION_MESSAGE);
				}
			}
		);

		menuHelp.add(itemHelpAbout);

		menuBar.add(menuHelp);

		return menuBar;
	}

	public static void main(String[] args)
	{
		try
		{
			UIManager.setLookAndFeel( "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel" );
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				GameReplay game = new GameReplay();

				// Center the application on the screen
				Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
				Dimension us = game.getSize();
				int x = ( screen.width - us.width ) / 2;
				int y = ( screen.height - us.height ) / 2;
				game.setLocation( x, y );
				game.setVisible( true );
			}
		});
	}

	///////////////////////////////////////////////////////////////////////////
	//
	// INNER CLASS - FileOpenHandler
	//
	///////////////////////////////////////////////////////////////////////////
	class FileOpenHandler implements ActionListener
	{
		JFrame frame;

		FileOpenHandler()
		{
			super();
		}

		public FileOpenHandler(JFrame frm)
		{
			super();
			frame = frm;
		}

		public void actionPerformed( ActionEvent event )
		{
			// Get the current directory
			File currentDirectory = new File (".");

			// Create a file chooser
			JFileChooser fc = new JFileChooser(currentDirectory);
			fc.setAcceptAllFileFilterUsed(true);
			fc.addChoosableFileFilter(new PlayByPlayFileFilter());

			// In response to a button click:
			int returnVal = fc.showOpenDialog(frame);
			if (returnVal == JFileChooser.APPROVE_OPTION)
			{
				File file = fc.getSelectedFile();
			}
		}
	} // end FileOpenHandler

	///////////////////////////////////////////////////////////////////////////
	//
	// INNER CLASS - PlayByPlayFileFilter
	//
	///////////////////////////////////////////////////////////////////////////
	class PlayByPlayFileFilter extends FileFilter
	{
		public boolean accept(File f)
		{
			if (f.isDirectory())
			{
				return true;
			}
			if (getExtension(f).equals("pbp") || getExtension(f).equals("txt"))
				return true;
			else
				return false;
		}

		// The description of this filter
		public String getDescription()
		{
			return "Play By Play Files (*.pbp|*.txt)";
		}

		// Get the extension of a file.
		private String getExtension(File f)
		{
			String ext = "";
			String s = f.getName();
			int i = s.lastIndexOf('.');
			if (i > 0 &&  i < s.length() - 1)
				ext = s.substring(i+1).toLowerCase();
			return ext;
		}
	}  // end PlayByPlayFileFilter


} // End of GameReplay