package com.kelsoft.gamereplay;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JComponent;


/**
 *
 * @author Kelly Lamb
 */
public class SBEffectHomeRun extends SBEffectAbstract
{
	private Random random = new Random();
	private SBEffectFirework[] fireworks = new SBEffectFirework[6];
	private int Scene = -1;
	private int countdownInit = 50;
	private int countdown = countdownInit;
	private String message = null;

	public SBEffectHomeRun(JComponent parent, Boolean needImage)
	{
		super(parent, needImage);
		setSleepAmount(10);
		message = GameConstants.HOME_RUN;
		g2db.setPaint(Color.BLACK);
		g2db.fillRect(0, 0, SBEffectAbstract.WIDTH, SBEffectAbstract.HEIGHT); // fill the background
	}

	public void setMessage(String Message)
	{
		message = Message;
	}

	public void render(int w, int h, Graphics2D g2)
	{
		Insets insets = parent.getInsets();
		int il = insets.left;
		int it = insets.top;
		g2.drawImage(dbImage,il,it,null);
	}

	public void reset(int newwidth, int newheight)
	{
		stop();
		for (int i=0; i<6; i++)
		{
			fireworks[i] = new SBEffectFirework(this.parent, false);
		}
		Scene = -1;
		countdown = countdownInit;
	}

	public void step(int w, int h)
	{
		if (! isAnimating()) return;
		if (Scene == -1)
		{
			Scene++;
		}
		else if (Scene == 0)
		{
			if (countdown == countdownInit)
			{
				g2db.setColor(Color.BLACK);
				g2db.fillRect(0, 0, SBEffectAbstract.WIDTH,SBEffectAbstract.HEIGHT);
				writeMessage(g2db,"Its Going",1);
			}
			countdown--;
			if (countdown == 0)
			{
				Scene++;
				countdown = countdownInit;
			}
		}
		else if (Scene == 1)
		{
			if (countdown == countdownInit)
			{
				g2db.setColor(Color.BLACK);
				g2db.fillRect(0, 0, SBEffectAbstract.WIDTH,SBEffectAbstract.HEIGHT);
				writeMessage(g2db,"Going",Scene*3);
			}
			countdown--;
			if (countdown == 0)
			{
				Scene++;
				countdown = countdownInit;
			}
		}
		else if (Scene == 2)
		{
			if (countdown == countdownInit)
			{
				g2db.setColor(Color.BLACK);
				g2db.fillRect(0, 0, SBEffectAbstract.WIDTH,SBEffectAbstract.HEIGHT);
				writeMessage(g2db,"Going",Scene*3);
			}
			countdown--;
			if (countdown == 0)
			{
				Scene++;
				countdown = countdownInit;
			}
		}
		else if (Scene == 3)
		{
			if (countdown == countdownInit)
			{
				g2db.setColor(Color.BLACK);
				g2db.fillRect(0, 0, SBEffectAbstract.WIDTH,SBEffectAbstract.HEIGHT);
				writeMessage(g2db,"GONE!!!",Scene*3);
			}
			countdown--;
			if (countdown == 0)
			{
				Scene++;
				countdown = countdownInit*10;
			}
		}
		else if (Scene == 4)
		{
			if (countdown == countdownInit*10)
			{
				g2db.setColor(Color.BLACK);
				g2db.fillRect(0, 0, SBEffectAbstract.WIDTH,SBEffectAbstract.HEIGHT);
			}
			countdown--;
			for (int i=0; i<6; i++)
			{
				fireworks[i].step(0,0);
				fireworks[i].render(0,0,g2db);
			}
			writeMessage(g2db,message,9,true);
			if (countdown == 0)
			{
				g2db.setColor(Color.BLACK);
				g2db.fillRect(0, 0, SBEffectAbstract.WIDTH,SBEffectAbstract.HEIGHT);
				countdown = countdownInit;
				Scene = -1;
				stop();
			}
		}
	}

	private void writeMessage(Graphics2D g2, String msg, int size)
	{
		writeMessage(g2, msg, size, false);
	}

	static int cycle = 0;
	static int cycleAmount = 6;
	private void writeMessage(Graphics2D g2, String msg, int size, Boolean cycle_color)
	{
		Font font = new Font("Serif", Font.BOLD, 10); // a basic font
		// a scaled up version
		Font bigfont = font.deriveFont(AffineTransform.getScaleInstance(size,size));
		g2.setFont(bigfont);
		FontMetrics fm = g2.getFontMetrics();
		int x = (520 - fm.stringWidth(msg)) / 2;
		int y = (fm.getAscent() + (105 - (fm.getAscent() + fm.getDescent())) / 2);
		GlyphVector gv = bigfont.createGlyphVector(g2.getFontRenderContext(),msg);
		g2.translate(x,y);
		if (cycle_color)
		{
			g2.setPaint(new GradientPaint(0,0, new Color(255,cycle,0), 0,gv.getGlyphOutline(0).getBounds().height, new Color(255,255-cycle,0), true));
			cycle = cycle + cycleAmount;
			if (cycle > 255)
			{
				cycle = 255;
				cycleAmount = -cycleAmount;
			}
			else if (cycle < 0)
			{
				cycle = 0;
				cycleAmount = -cycleAmount;
			}
			//cycle = cycle & 255;
		}
		else
			g2.setPaint(new GradientPaint(0,0, Color.RED, 0,gv.getGlyphOutline(0).getBounds().height, Color.YELLOW, true));
		for (int d=0; d <gv.getNumGlyphs(); d++)
		{
			g2.fill(gv.getGlyphOutline(d)); // Fill the shape
		}
		g2.translate(-x,-y);
	}
}
