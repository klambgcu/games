package com.kelsoft.gamereplay;

import java.util.ArrayList;
import java.util.List;

public class Team
{
	private ArrayList<Player> roster = new ArrayList<Player>();
	private boolean  showPix = false;
	private int      color = GameConstants.TEAM_COLOR_PURPLE;
	private int      side  = GameConstants.TEAM_VISITOR;
	private Player   nullPlayer = new Player("","","");
	private Player[] positions = new Player[9];
	private String   name = "";
	private int      type = GameConstants.PLAYER_TYPE_DEFENSE;

	///////////////////////////////////////////////////
	// CONSTRUCTORS
	///////////////////////////////////////////////////
	Team()
	{
	}

	Team(String name, String color, boolean showPix)
	{
		this.name    = name;
		this.showPix = showPix;
		setColor(color);
	}

	///////////////////////////////////////////////////
	// ACCESSORS
	///////////////////////////////////////////////////

	public int getColor()
	{
		return this.color;
	}

	public String getName()
	{
		return this.name;
	}

	public Player getPlayer(int pos)
	{
		Player result = nullPlayer;

		for (Player p : roster)
		{
			if (p.getPosition() == pos)
			{
				result = p;
				break;
			}
		}
		return result;
	}

	public Player getPlayer(String num)
	{
		Player result = nullPlayer;

		for (Player p : roster)
		{
			if ( num.equals(p.getNumber()) )
			{
				result = p;
				break;
			}
		}
		return result;
	}

	public List<Player> getRoster()
	{
		return this.roster;
	}

	public int getSide()
	{
		return this.side;
	}

	public boolean getShowPix()
	{
		return this.showPix;
	}

	public int getType()
	{
		return this.type;
	}

	///////////////////////////////////////////////////
	// MUTATORS
	///////////////////////////////////////////////////
	public void setColor( String color )
	{
		// Determine color reference - defaults to last item (Black)
		for (String c : GameConstants.teamColors)
			if (c.equalsIgnoreCase(color))
				break;
			else
				this.color++;
	}

	public void setName( String name )
	{
		this.name = name;
	}

	public void addPlayer( Player p )
	{
		roster.add( p );
	}

	public void setSide( int side )
	{
		this.side = side;
	}

	public void setShowPix(boolean b)
	{
		this.showPix = b;
	}

	public void setType( int type )
	{
		this.type = type;
		for (Player p : roster)
			p.getPlayerView().setPlayerIcon(true);
	}

	///////////////////////////////////////////////////
	// UTILITIES
	///////////////////////////////////////////////////
	public String substituteRunner(String numOut, String numIn)
	{
		Player po = getPlayer( numOut );
		Player pi = getPlayer( numIn );

		return "\t\tSubstitution: %s" + pi.getNumberName() + "to pinch run for" + po.getNumberName() + "\r\n";
	}

	public String substitutePlayer(String pos, String numOut, String numIn)
	{
		Player po = getPlayer( numOut );
		Player pi = getPlayer( numIn );

		pi.setPosition( pos );
		if (po.getPosition() == Integer.valueOf(pos))
			po.setPosition( "0" ); // Bench

		return "\t\tSubstitution: %s" + pi.getNumberName() + "to %s for" + po.getNumberName() + "\r\n";
	}

	public void resetPosition()
	{
		for (int i = 0; i < roster.size(); i++)
			roster.get(i).resetPosition();
	}
}

