package com.kelsoft.gamereplay;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.util.Random;
import javax.swing.JComponent;


/**
 *
 * @author Kelly Lamb
 */
public class SBEffectFirework extends SBEffectAbstract
{
	private Random random = new Random();
	private double[] partX = new double[60];
	private double[] partY = new double[60];
	private int color = 0;
	private int countdown = 0;
	private int energy = 0;
	private int gravity = 200;
	private int index = 0;
	private int posX;
	private int posY;
	private int[] circle1 = { 0, 1 };
	private int[] circle3 = { 0, 1, -1, 3, 0, 1 };
	private int[] circle5 = { -1, 3, -2, 5, -2, 5, -2, 5, -1, 3 };
	private int[] circle7 = { -1, 3, -2, 5, -3, 7, -3, 7, -3, 7, -2, 5, -1, 3 };
	private int[][] circles = { circle1 };

	static final double[] vx =
	{
		1.00,0.86,0.50, 0.00,-0.49,-0.86,-1.00,-0.86,-0.50,0.00,0.50,0.86,
		0.66,0.48,0.17,-0.17,-0.48,-0.66,-0.66,-0.48,-0.17,0.17,0.48,0.66,
		0.35,0.28,0.13,-0.04,-0.21,-0.32,-0.35,-0.28,-0.13,0.04,0.21,0.32,
		0.09,0.07,0.04, 0.00,-0.05,-0.08,-0.09,-0.07,-0.04,0.00,0.05,0.08,
		0.02,0.02,0.01, 0.00,-0.01,-0.02,-0.02,-0.02,-0.01,0.00,0.01,0.02
	};

	static final double[] vy =
	{
		0.00,0.49,0.86,1.00,0.86,0.50, 0.00,-0.49,-0.86,-1.00,-0.86,-0.49,
		0.17,0.48,0.66,0.66,0.48,0.17,-0.17,-0.48,-0.66,-0.66,-0.48,-0.17,
		0.04,0.21,0.32,0.35,0.28,0.13,-0.04,-0.21,-0.32,-0.35,-0.28,-0.13,
		0.00,0.05,0.08,0.09,0.07,0.04, 0.00,-0.05,-0.08,-0.09,-0.07,-0.04,
		0.00,0.01,0.02,0.02,0.02,0.01, 0.00,-0.01,-0.02,-0.02,-0.02,-0.01
	};

	public SBEffectFirework(JComponent parent, Boolean needImage )
	{
		this(	parent,
				needImage,
				new java.util.Random().nextInt(480)+40,
				new java.util.Random().nextInt(50)+25,
				new java.util.Random().nextInt(100)+50,
				new java.util.Random().nextInt(175)+25);
	}

	public SBEffectFirework(JComponent parent, Boolean needImage, int x, int y, int energy, int gravity)
	{
		super(parent, needImage);
		setSleepAmount(20);

		this.posX = x;
		this.posY = y;
		this.energy = energy;
		this.gravity = gravity;

		for (int i=0; i<60; i++)
		{
			partX[i] = vx[i] * this.energy;
			partY[i] = vy[i] * this.energy;
		}
		color = random.nextInt(3);
		index = 0;
		countdown = random.nextInt(75);
	}

	public void reset(int newwidth, int newheight)
	{
		this.posX = random.nextInt(480)+40;
		this.posY = random.nextInt(50)+25;
		this.energy = random.nextInt(100)+50;
		this.gravity = random.nextInt(175)+25;

		for (int i=0; i<60; i++)
		{
			partX[i] = vx[i] * this.energy;
			partY[i] = vy[i] * this.energy;
		}
		color = random.nextInt(3);
		index = 0;
		countdown = random.nextInt(75);
	}

	public void render(int w, int h, Graphics2D g2)
	{
		if (countdown > 0) return;

		for (int i=0; i<60; i++)
		{
			if (color == 0)      g2.setColor(new Color(255,255-(index<<1),0));
			else if (color == 1) g2.setColor(new Color(0,255-(index<<1),255));
			else if (color == 2) g2.setColor(new Color(255-index,255,0));

			double s=(double)index/100;

			int size = ((int)i/12);
			int x=(int)(posX+partX[i]*s);
			int y=(int)(posY+partY[i]*s+gravity*s*s);

			int[] c = circles[Math.min(size, circles.length-1)];
			int yy = y - (size >> 1);
			for (int j = 0; j < c.length; j += 2,yy++)
			{
				int x1 = Math.max(-2, x+c[j]);
				int x2 = Math.min(521, x1+c[j+1]);
				g2.drawLine(x1,yy,x2,yy);
			}

			if(index>=50)
			{
				for(int k=0;k<2;k++)
				{
					s=(double)((index-50)*2+k)/100;
					x=(int)(posX+partX[i]*s);
					y=(int)(posY+partY[i]*s+gravity*s*s);
					g2.setColor(Color.BLACK);
					c = circles[Math.min(size, circles.length-1)];
					yy = y - (size >> 1);
					for (int j = 0; j < c.length; j += 2,yy++)
					{
						int x1 = Math.max(-2, x+c[j]);
						int x2 = Math.min(521, x1+c[j+1]);
						g2.drawLine(x1,yy,x2,yy);
					}
				}
			}
		}
	}

	public void step(int w, int h)
	{
		if (countdown > 0)
		{
			countdown--;
			return;
		}
		index++;
		if (index>100) reset(0,0);
	}

	public void setMessage(String Message)
	{
	}
}
