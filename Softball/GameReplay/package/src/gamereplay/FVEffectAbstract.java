package com.kelsoft.gamereplay;

import javax.swing.JComponent;

/**
 *
 * @author Kelly Lamb
 */
public abstract class FVEffectAbstract implements Runnable
{
	protected Boolean    animating   = false;
	protected Boolean    completed   = false;
	protected JComponent parent      = null;
	protected long       sleepAmount = 50;
	protected Teams      teams;
	protected String     message;

	public String name;
	public Thread thread;

	public abstract void render();
	public abstract void reset();
	public abstract void step();
	public abstract void setMessage(String Message);

	public FVEffectAbstract(JComponent p, Teams t )
	{
		parent = p;
		teams  = t;
		name   = this.getClass().getSimpleName();
	}

	public void setSleepAmount(long amount)
	{
		sleepAmount = amount;
	}

	public long getSleepAmount()
	{
		return sleepAmount;
	}

	public Boolean isAnimating()
	{
		return animating;
	}

	public Boolean isCompleted()
	{
		return completed;
	}

	public void start()
	{
		if (thread == null)
		{
			thread = new Thread(this);
			thread.setPriority(Thread.MIN_PRIORITY);
			thread.setName(name + " Effect");
			thread.start();
			animating = true;
			completed = false;
		}
	}

	public synchronized void stop()
	{
		if (thread != null)
		{
			thread.interrupt();
		}
		thread = null;
		notifyAll();
		animating = false;
		completed = true;
	}

	public void run()
	{
		Thread me = Thread.currentThread();

		while (thread == me)
		{
			step();
			parent.repaint();
			try
			{
				thread.sleep(sleepAmount);
			}
			catch (InterruptedException e) { }
		}
		thread = null;
		animating = false;
		completed = true;
	}
}
