package com.kelsoft.gamereplay;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import javax.swing.ImageIcon;
import javax.swing.JComponent;


/**
 *
 * @author Kelly Lamb
 */
public class SBEffectStrikeOut extends SBEffectAbstract
{
	private Image soImage;
	private ImageIcon ball;
	private int Scene = -1;
	private int eraseindex = 0;
	private int index = 0;
	private int sincX = 20;
	private int sx = 0;
	private String message = null;

	public SBEffectStrikeOut(JComponent parent, Boolean needImage)
	{
		super(parent, needImage);
		setSleepAmount(20);

		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getDefaultScreenDevice();
		GraphicsConfiguration gc = gs.getDefaultConfiguration();

		// Create an image that does not support transparency
		soImage = gc.createCompatibleImage(WIDTH,HEIGHT, Transparency.OPAQUE);
		Graphics2D g2 = (Graphics2D) soImage.getGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setPaint(Color.BLACK);
		g2.fillRect(0, 0, WIDTH, HEIGHT); // fill the background

		message = GameConstants.STRIKE_OUT;
		Font font = new Font("Serif", Font.BOLD, 10); // a basic font
		Font bigfont = // a scaled up version
		font.deriveFont(AffineTransform.getScaleInstance(10.0,10.0));
		GlyphVector gv = bigfont.createGlyphVector(g2.getFontRenderContext(),message);
		g2.translate(8,85);
		g2.setPaint(new GradientPaint(0,0, Color.RED, 0,gv.getGlyphOutline(0).getBounds().height, Color.YELLOW, true));
		for (int d=0; d <gv.getNumGlyphs(); d++)
		{
			g2.fill(gv.getGlyphOutline(d)); // Fill the shape
		}
		try
		{
			ball = new ImageIcon(getClass().getResource("images/Softball.png"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void render(int w, int h, Graphics2D g2)
	{
		Insets insets = parent.getInsets();
		int il = insets.left;
		int it = insets.top;

		g2db.setPaint(Color.BLACK);
		g2db.fillRect(0, 0, WIDTH, HEIGHT); // fill the background
		if (Scene == 0)
		{
			AffineTransform trans = new AffineTransform();
			trans.setToTranslation(il+index,it+10);
			trans.rotate(Math.toRadians(index % 360) , 40,40);
			g2db.setClip(0, 0, WIDTH, HEIGHT);
			g2db.drawImage(ball.getImage(), trans, null);
			g2db.setClip(null);
		}
		g2db.drawImage(soImage,eraseindex,0,index-eraseindex,HEIGHT,0,0,index,HEIGHT, null);
		g2.drawImage(dbImage,il,it,null);
	}

	public void setMessage(String Message)
	{
	}

	public void reset(int newwidth, int newheight)
	{
		stop();
		Scene = -1;
		index = 0;
		eraseindex=0;
	}

	public void step(int w, int h)
	{
		if (! isAnimating()) return;
		if (Scene == -1)
		{
			Scene++;
		}
		else if (Scene == 0)
		{
			index += sincX;
			if (index > WIDTH)
			{
				index = WIDTH;
				Scene++;
			}
		}
		else if (Scene == 1)
		{
			eraseindex += sincX;
			if (eraseindex > WIDTH)
			{
				eraseindex = WIDTH;
				Scene++;
			}
		}
		else if (Scene == 2)
		{
			eraseindex -= sincX;
			if (eraseindex <= 0)
			{
				eraseindex = 0;
				index = 0;
				Scene = -1;
				stop();
			}
		}
	}
}
