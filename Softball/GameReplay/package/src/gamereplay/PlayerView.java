package com.kelsoft.gamereplay;

import com.kelsoft.gamereplay.FieldView;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.Timer;

public class PlayerView extends JComponent implements ActionListener, MouseListener
{
	private boolean   animating = false;
	private double    dstX;
	private double    dstY;
	private double    incX;
	private double    incY;
	private double    x;
	private double    y;
	private FieldView fieldView;
	private int       offsetX;
	private int       offsetY;
	private int       optionVisible;
	private int       pathIndex = 0;
	private int       stepsCount;
	private int       vh;
	private int[]     alignDefense = {0,0,5,2,2,1,1,1,0,2};
	private int[]     alignOffense = {3,4,0,3}; // 0 = batter RH (default) override align to 4 if batter LH
	private int[]     path;
	private int[][]   alignName    = {{-2,-14},{-1,0},{19,0},{-1,12},{19,12},{-2,22}};  //x=-1..Left, x=-2..Center, x>=0..Right
	private JLabel    playerImage;
	private JLabel    playerName;
	private Player    player;
	private Timer     timer;



	public PlayerView( Player p, int vh, FieldView fieldView ) //String name, int pos, int type, int hand)
	{
		timer = new Timer(75,this);
		timer.setInitialDelay(0);

		FontMetrics fm;
		int width;
		String name = p.getName().trim();

		this.player    = p;
		this.vh        = vh;
		this.fieldView = fieldView;

		p.setPlayerView( this );
		playerImage = new JLabel();

		setPlayerIcon(false);

		playerImage.setOpaque(false);
		playerImage.setBackground(Color.WHITE);
		playerImage.setIconTextGap(0);

		playerName = new JLabel(name, JLabel.LEFT);
		playerName.setFont(new Font("Arial",Font.PLAIN,12));
		playerName.setFont(new Font("Dialog",Font.BOLD,12));
		playerName.setOpaque(false);
		playerName.setForeground(Color.BLACK);
		playerName.setVerticalAlignment(JLabel.BOTTOM);
		playerName.setVerticalTextPosition(JLabel.BOTTOM);
		playerName.setIconTextGap(0);
		fm = playerName.getFontMetrics(playerName.getFont());
		width = fm.stringWidth(name);
		playerName.setSize(width, fm.getHeight());

		setNameAlignment( 0 );

		playerImage.setLabelFor(this);
		playerImage.addMouseListener(this);

		playerName.setLabelFor(this);
		playerName.addMouseListener(this);

		this.setVisible(false);
		this.setLocation( GameConstants.locDugOut[ player.getTeam().getSide() ][0],
						  GameConstants.locDugOut[ player.getTeam().getSide() ][1] );
	}

	public void actionPerformed(ActionEvent e)
	{
		if (optionVisible == GameConstants.VISIBLE_BEFORE)
		{
			setVisible(true);
			optionVisible = GameConstants.VISIBLE_NONE;
		}

		if (moveToDestination())
		{
			pathIndex++;
			if (path != null && pathIndex < path.length)
			{
				int p = path[pathIndex];
				setDestinationSteps(GameConstants.locOffense[p][0],
									GameConstants.locOffense[p][1],
									20 );
			}
			else
			{
				timer.stop();
				setPlayerIcon(false);
				if (optionVisible == GameConstants.VISIBLE_AFTER)
				{
					setVisible(false);
					optionVisible = GameConstants.VISIBLE_NONE;
				}
				if (path != null)
				{
					fieldView.fireSetRunnerPosition(this.player, path[0], path[path.length-1]);
					path = null;
				}

				this.animating = false;
			}
		}
	}

	public void startAnimation(int optionVisible)
	{
		this.optionVisible = optionVisible;
		this.animating = true;

		timer.start();
	}

	public void setDestinationSteps(double x, double y, int steps)
	{
		this.dstX = x;
		this.dstY = y;
		this.stepsCount = steps;
		this.x = playerImage.getX()+1.0;
		this.y = playerImage.getY();
		this.incX = (this.dstX - this.x) / (double) this.stepsCount;
		this.incY = (this.dstY - this.y) / (double) this.stepsCount;
		setPlayerIcon(true);
	}

	public Boolean moveToDestination()
	{
		if (stepsCount <= 0)
		{
			this.setLocation( (int)this.dstX, (int)this.dstY );
			return true;
		}
		else
		{
			this.x += incX;
			this.y += incY;
			this.setLocation( (int)this.x, (int)this.y );
			stepsCount--;
			return false;
		}
	}

	public void setNameAlignment(int type)
	{
		int align;
		int px;
		int py;
		int x;
		int width = playerName.getWidth();
		int pos   = player.getPosition();
		int bpos  = player.getBatPosition();

		if (type == GameConstants.PLAYER_TYPE_INTRO)
		{
			int side = player.getTeam().getSide();
			if (side == GameConstants.TEAM_VISITOR)
			{
				offsetX = alignName[4][0];
				offsetY = alignName[4][1];
			}
			else
			{
				offsetX = -width;
				offsetY = alignName[3][1];
			}
			setLocation(playerImage.getX(), playerImage.getY());
		}
		else
		{
			if (player.getTeam().getType() == GameConstants.PLAYER_TYPE_OFFENSE)
			{
				align = alignOffense[bpos];
				if ((player.getHand() == GameConstants.PLAYER_LEFTHAND) && (bpos == 0))
					align = 4;
			}
			else
			{
				align = alignDefense[pos];
			}

			x = alignName[align][0];

			offsetY = alignName[align][1];
			if (x == -2)
				offsetX = 10-(width/2);
			else if (x == -1)
				offsetX = -width;
			else offsetX = x;

			this.setLocation(playerImage.getX(), playerImage.getY());
		}
	}

	public void setPlayerIcon(Boolean running)
	{
		ImageIcon image;

		if (player.getTeam().getType() == GameConstants.PLAYER_TYPE_OFFENSE)
		{
			if (running)
			{
				image = GameImages.getImageIcon(player.getTeam().getColor(),
												player.getTeam().getType(),
												(incX >= 0.00 ? GameConstants.PLAYER_RIGHTHAND : GameConstants.PLAYER_LEFTHAND) );
			}
			else
			{
				int face = player.getHand();
				if (path != null)
				{
					int pp = path[path.length-1];
					if (pp == 3)
						face = GameConstants.PLAYER_RIGHTHAND;
					else if ((pp == 1) || (pp == 2))
						face = GameConstants.PLAYER_LEFTHAND;
				}

				image = GameImages.getImageIcon(player.getTeam().getColor(),
												player.getTeam().getType(),
												face);
			}
		}
		else
		{
			image = GameImages.getImageIcon(player.getTeam().getColor(),
											player.getTeam().getType(),
											player.getHand() );
		}
		playerImage.setIcon( image );
		playerImage.setSize(image.getIconWidth(), image.getIconHeight());
		repaint();
	}

	@Override public void setLocation(int x, int y)
	{
		playerImage.setLocation(x, y);
		playerName.setLocation(x+offsetX, y+offsetY);
	}

	public Boolean isAnimating()
	{
		return this.animating;
	}

	public JLabel getPlayerIcon()
	{
		return playerImage;
	}

	public JLabel getPlayerName()
	{
		return playerName;
	}

	public void highLight(Boolean hilite)
	{
		playerImage.setOpaque(hilite);
		playerName.setForeground((hilite == false ? Color.BLACK : Color.WHITE));
		playerImage.repaint();
		playerName.repaint();
	}

	public String getPlayerInfo()
	{
		return	"Number:   " + player.getNumber() + "\n" +
				"Name:     " + player.getName()   + "\n" +
				"Position: " + GameConstants.titleFull[player.getPosition()] + "\n";
	}

	public void mouseEntered(MouseEvent me)
	{
		PlayerView p = ((PlayerView)((JLabel) me.getSource()).getLabelFor());
		p.highLight(true);
	}

	public void mouseExited(MouseEvent me)
	{
		PlayerView p = ((PlayerView)((JLabel) me.getSource()).getLabelFor());
		p.highLight(false);
	}

	public void mouseClicked(MouseEvent me)
	{
		JFrame f = new JFrame();
		JDialog d = new JDialog( f );
		String text = ((PlayerView)((JLabel) me.getSource()).getLabelFor()).getPlayerInfo();
		d.add( new JTextArea(text));
		d.pack();
		d.setVisible(true);
	}

	public void mouseReleased(MouseEvent me)
	{
	}

	public void mousePressed(MouseEvent me)
	{
	}

	public void moveToBattersBox()
	{
		int sideBat = player.getHand();
		setNameAlignment( GameConstants.PLAYER_TYPE_OFFENSE );
		setDestinationSteps( (double)GameConstants.locBattersBox[sideBat][0],
							 (double)GameConstants.locBattersBox[sideBat][1],
							 20 );
		path = new int[1]; path[0] = 0;
		startAnimation(GameConstants.VISIBLE_BEFORE);
	}

	public void moveToDugOut()
	{
		int sideDugOut = player.getTeam().getSide();
		setNameAlignment( GameConstants.PLAYER_TYPE_DEFENSE );
		setDestinationSteps( (double)GameConstants.locDugOut[sideDugOut][0],
							 (double)GameConstants.locDugOut[sideDugOut][1],
							 20 );
		startAnimation(GameConstants.VISIBLE_AFTER);
	}

	public void startRunning(int[] path)
	{
		this.path = path;
		this.pathIndex = 1;
		int p = path[pathIndex];
		setDestinationSteps(GameConstants.locOffense[p][0],
							GameConstants.locOffense[p][1],
							20 );
		startAnimation(GameConstants.VISIBLE_NONE);
	}

	@Override public void setVisible(boolean v)
	{
		playerImage.setVisible(v);
		playerName.setVisible(v);
	}
}
