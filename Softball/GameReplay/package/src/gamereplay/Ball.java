package com.kelsoft.gamereplay;

import com.kelsoft.gamereplay.FieldView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.Timer;

public class Ball extends JLabel implements ActionListener
{
	private BallPoint[] bp;
	private boolean     animating = false;
	private double      dstX = 0.0;
	private double      dstY = 0.0;
	private double      incX = 0.0;
	private double      incY = 0.0;
	private double      xx = 0.0;
	private double      yy = 0.0;
	private FieldView   fieldView;
	private ImageIcon   ballImages[] = new ImageIcon[4];
	private int         ballInc  = -1;
	private int         ballMid;
	private int         ballPath = GameConstants.BALLPATH_NORMAL;
	private int         ballSize = 8;
	private int         bpIndex = 0;
	private int         bpLength = 0;
	private int         index = 0;
	private int         stepsCount = 0;
	private Timer       timer;

	public Ball(FieldView fieldView)
	{
		this.fieldView = fieldView;

		ballImages = GameImages.getBallIcons();
		setBounds(0, 0, ballSize, ballSize);

		timer = new Timer(75,this);
		timer.setInitialDelay(75);
		timer.start();
	}

	public void actionPerformed(ActionEvent e)
	{
		boolean b;

		index++;
		if (index > 3) index = 0;

		if (this.ballPath == GameConstants.BALLPATH_FLYBALL)
		{
			ballSize = ballSize + (int)Math.signum(stepsCount - ballMid);
			// Truncate size of ball
			if (ballSize > 16)
				ballSize = 16;
			else if (ballSize < 8)
				ballSize = 8;
		}
		else if (this.ballPath == GameConstants.BALLPATH_GROUNDER)
		{
			ballSize += ballInc;

			if ((ballSize > 8) || (ballSize < 4))
				ballInc = -ballInc;
		}

//System.out.printf("path=%d, mid=%d, size=%d, steps=%d\n",ballPath, ballMid, ballSize, stepsCount);

		setSize(ballSize,ballSize);
		if (stepsCount > 0)
		{
			b = moveToDestination();
			animating = true;
		}
		else
			animating = false;
	}

	public void setDestinationSteps(double x, double y, int steps)
	{
		this.dstX = x;
		this.dstY = y;
		this.xx = getX()+1.0;
		this.yy = getY();
		this.incX = (this.dstX - this.xx) / (double) steps;
		this.incY = (this.dstY - this.yy) / (double) steps;
		this.stepsCount = steps;
		this.bpLength = 0;
		this.bpIndex  = 0;
		this.ballPath = GameConstants.BALLPATH_NORMAL;
	}

	public void setDestinationSteps(BallPoint b)
	{
		this.dstX = b.getDstX();
		this.dstY = b.getDstY();
		this.xx = getX()+1.0;
		this.yy = getY();
		this.incX = (this.dstX - this.xx) / (double) b.getSteps();
		this.incY = (this.dstY - this.yy) / (double) b.getSteps();
		this.stepsCount = b.getSteps();
		this.ballPath = b.getType();
		if (this.ballPath != GameConstants.BALLPATH_NORMAL)
		{
			fieldView.fireBatterMovement();
		}
		if (this.ballPath == GameConstants.BALLPATH_FLYBALL)
		{
			this.ballMid = (this.stepsCount / 2) - 2;
		}
		else if (this.ballPath == GameConstants.BALLPATH_GROUNDER)
		{
			ballInc = -1;
		}
		else
			this.ballSize = 8;
	}

	public void setDestinationSteps(BallPoint[] b)
	{
		this.bp = b;
		this.setDestinationSteps(bp[0]);
		this.bpLength = b.length;
		this.bpIndex = 0;
	}

	public Boolean moveToDestination()
	{
		this.stepsCount--;
		if (stepsCount <= 0)
		{
			this.setLocation( (int)this.dstX, (int)this.dstY );
			if (bpIndex < bpLength-1)
			{
				this.bpIndex++;
				this.setDestinationSteps(this.bp[bpIndex]);
				this.bpLength = bp.length;
				return false;
			}
			else
				return true;
		}
		else
		{
			this.xx += incX;
			this.yy += incY;
			this.setLocation( (int)this.xx, (int)this.yy );
			return false;
		}
	}

	public Boolean isAnimating()
	{
		return animating;
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		g.drawImage( ballImages[index].getImage(),0,0,ballSize,ballSize,null);
	}
}