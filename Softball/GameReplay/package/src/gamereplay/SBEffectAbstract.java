package com.kelsoft.gamereplay;

import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Transparency;

import javax.swing.JComponent;


/**
 *
 * @author Kelly Lamb
 */
public abstract class SBEffectAbstract implements Runnable
{
	// Size of our scoreboard
	static final int HEIGHT = 105;
	static final int WIDTH  = 520;

	protected Boolean animating = false;
	protected Graphics2D g2db = null;
	protected Image dbImage = null;
	protected JComponent parent = null;
	protected long sleepAmount = 50;

	public String name;
	public Thread thread;

	public abstract void render(int w, int h, Graphics2D g2);
	public abstract void reset(int newwidth, int newheight);
	public abstract void step(int w, int h);
	public abstract void setMessage(String Message);

	public SBEffectAbstract(JComponent p, Boolean needImage )
	{
		parent = p;
		name = this.getClass().getSimpleName();
		if (needImage)
		{
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice gs = ge.getDefaultScreenDevice();
			GraphicsConfiguration gc = gs.getDefaultConfiguration();

			// Create an image that does not support transparency
			dbImage = gc.createCompatibleImage(SBEffectAbstract.WIDTH,SBEffectAbstract.HEIGHT, Transparency.OPAQUE);
			g2db = (Graphics2D) dbImage.getGraphics();
			g2db.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		}
	}

	public void setSleepAmount(long amount)
	{
		sleepAmount = amount;
	}

	public long getSleepAmount()
	{
		return sleepAmount;
	}

	public Boolean isAnimating()
	{
		return animating;
	}

	public void start()
	{
		if (thread == null)
		{
			thread = new Thread(this);
			thread.setPriority(Thread.MIN_PRIORITY);
			thread.setName(name + " Effect");
			thread.start();
			animating = true;
		}
	}

	public synchronized void stop()
	{
		if (thread != null)
		{
			thread.interrupt();
		}
		thread = null;
		notifyAll();
		animating = false;
	}

	public void run()
	{
		Thread me = Thread.currentThread();

		while (thread == me)
		{
			parent.repaint();
			try
			{
				thread.sleep(sleepAmount);
			}
			catch (InterruptedException e) { }
		}
		thread = null;
		animating = false;
	}
}
