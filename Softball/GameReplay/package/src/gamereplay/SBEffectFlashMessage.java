package com.kelsoft.gamereplay;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.TexturePaint;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import javax.swing.JComponent;

public class SBEffectFlashMessage extends SBEffectAbstract
{
	private Area areaText = null;
	private Boolean toggle = true;
	private Font font = new Font("System", Font.BOLD, 76);
	private int countdown = 15;
	private int Scene = -1;
	private int x = 0;
	private int xx = 0;
	private int y = 0;
	private int yy = 0;
	private Shape shapeText = null;
	private TexturePaint tp = null;
	private String message = "UNDEFINED";

	public SBEffectFlashMessage(JComponent parent, Boolean needImage)
	{
		super(parent, needImage);
		setSleepAmount(10);
		tp = getTextureImage();
		g2db.setFont(font);
	}

	private TexturePaint getTextureImage()
	{
		int size = 4;
		BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bi.createGraphics();
		g2.setColor(Color.BLACK);
		g2.fillRect(0, 0, size, size);
		g2.setColor(Color.YELLOW);
		g2.fillRect(1,0,2,2);
		TexturePaint texture = new TexturePaint(bi, new Rectangle(0,0,size,size));
		return texture;
	}

	public void render(int w, int h, Graphics2D g2)
	{
		Insets insets = parent.getInsets();
		int il = insets.left;
		int it = insets.top;

		if (toggle)
		{
			g2db.setPaint(tp);
			g2db.fillRect(0,0,SBEffectAbstract.WIDTH,SBEffectAbstract.HEIGHT);
			g2db.setColor(Color.BLACK);
		}
		else
		{
			g2db.setColor(Color.BLACK);
			g2db.fillRect(0,0,SBEffectAbstract.WIDTH,SBEffectAbstract.HEIGHT);
			g2db.setPaint(tp);
		}

		Shape shapeOval = null;
		if (Scene < 4)
			shapeOval = new Ellipse2D.Float(xx>>1, yy>>1, SBEffectAbstract.WIDTH-xx, SBEffectAbstract.HEIGHT-yy);
		else
			shapeOval = new Rectangle(0,0,SBEffectAbstract.WIDTH,SBEffectAbstract.HEIGHT);

		Area areaOval = new Area(shapeOval);
		areaOval.exclusiveOr(areaText);
		g2db.clip(areaOval);
		g2db.fill(areaOval);
		g2db.setClip(null);
		g2.drawImage(dbImage,il,it,null);
	}

	public void reset(int newwidth, int newheight)
	{
		stop();
		Scene = -1;
		countdown = 15;
	}

	public void step(int w, int h)
	{
		if (! isAnimating()) return;
		if (Scene == -1)
		{
			Scene++;
		}
		else if (Scene < 4)
		{
			xx += 20;
			yy += 4;
			if (xx>=SBEffectAbstract.WIDTH)
			{
				xx = yy = 0;
				toggle = ! toggle;
				Scene++;
			}
		}
		else
		{
			countdown--;
			if (countdown == 0)
			{
				countdown = 15;
				Scene++;
				toggle = ! toggle;
				if (Scene > 8)
				{
					message = "UNDEFINED";
					Scene = -1;
					stop();
				}
			}
		}
	}

	public void setMessage(String Message)
	{
		message = Message;
		initMessage();
	}

	private void initMessage()
	{
		FontRenderContext frc = g2db.getFontRenderContext();
		TextLayout tl = new TextLayout(message, font, frc);
		FontMetrics fm = g2db.getFontMetrics();
		x = (SBEffectAbstract.WIDTH - fm.stringWidth(message)) / 2;
		y = (fm.getAscent() + (SBEffectAbstract.HEIGHT - (fm.getAscent() + fm.getDescent())) / 2);
		AffineTransform transform = new AffineTransform();
		transform.setToTranslation(x,y);
		shapeText = tl.getOutline(transform);
		areaText = new Area(shapeText);
		toggle = true;
		Scene = -1;
	}
}

