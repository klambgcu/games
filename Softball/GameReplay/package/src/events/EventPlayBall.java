package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;

public class EventPlayBall implements Command
{
	private GameScoreBoard scoreBoard;

	public EventPlayBall(GameScoreBoard scoreBoard)
	{
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.setAnimation( GameConstants.PLAY_BALL );
	}
}