package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.GameScoreBoard;

public class EventFieldingError implements Command
{
	private GameScoreBoard scoreBoard;

	public EventFieldingError(GameScoreBoard scoreBoard)
	{
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.incrementErrors();
	}
}