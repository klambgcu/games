package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.BallPoint;
import com.kelsoft.gamereplay.FieldView;
import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;
import com.kelsoft.gamereplay.Player;

public class EventBatHitByPitch implements Command
{
	private FieldView      fieldView;
	private GameScoreBoard scoreBoard;
	private Player         player;

	public EventBatHitByPitch(GameScoreBoard scoreBoard, FieldView fieldView, Player player)
	{
		this.fieldView  = fieldView;
		this.player     = player;
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.setAnimation( GameConstants.HIT_BY_PITCH );

		int amount = player.getHand() * 20 - 10;

		BallPoint[] b = new BallPoint[6];
		b[0] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  2);
		b[1] = new BallPoint( (double)GameConstants.posBallHit[0]+amount,
							  (double)GameConstants.posBallHit[1],
							  10);
		b[2] = new BallPoint( (double)GameConstants.posBall[2][0],
							  (double)GameConstants.posBall[2][1],
							  10,
							  GameConstants.BALLPATH_GROUNDER);
		b[3] = new BallPoint( (double)GameConstants.posBall[2][0],
							  (double)GameConstants.posBall[2][1],
							  2);
		b[4] = new BallPoint( (double)GameConstants.posBall[2][0],
							  (double)GameConstants.posBall[2][1],
							  5);
		b[5] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  10);
		fieldView.pitch(b);
		int[] path = {0,1};
		fieldView.advanceRunners(player, path);
	}
}