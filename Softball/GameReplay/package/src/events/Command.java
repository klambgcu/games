package com.kelsoft.gamereplay.events;

public interface Command
{
	public void execute();
}