package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.GameScoreBoard;

public class EventAdvanceInning implements Command
{
	private GameScoreBoard scoreBoard;

	public EventAdvanceInning(GameScoreBoard scoreBoard)
	{
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.advanceInning();
	}
}