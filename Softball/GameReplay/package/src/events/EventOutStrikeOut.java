package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.BallPoint;
import com.kelsoft.gamereplay.FieldView;
import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;

public class EventOutStrikeOut implements Command
{
	private FieldView      fieldView;
	private GameScoreBoard scoreBoard;

	public EventOutStrikeOut(GameScoreBoard scoreBoard, FieldView fieldView)
	{
		this.fieldView  = fieldView;
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.incrementOuts();
		scoreBoard.setAnimation( GameConstants.STRIKE_OUT );
	}
}