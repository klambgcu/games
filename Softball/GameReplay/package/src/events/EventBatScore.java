package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;

public class EventBatScore implements Command
{
	private GameScoreBoard scoreBoard;

	public EventBatScore(GameScoreBoard scoreBoard)
	{
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.incrementScore();
		scoreBoard.setAnimation( GameConstants.SCORE );
	}
}