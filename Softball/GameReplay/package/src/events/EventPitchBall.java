package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.BallPoint;
import com.kelsoft.gamereplay.FieldView;
import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;
import java.util.Random;

public class EventPitchBall implements Command
{
	private FieldView      fieldView;
	private GameScoreBoard scoreBoard;
	private int            amount;
	private Random         random = new Random();

	public EventPitchBall(GameScoreBoard scoreBoard, FieldView fieldView)
	{
		this.fieldView  = fieldView;
		this.scoreBoard = scoreBoard;
		amount = 3 - (6 * random.nextInt(2));
	}

	public void execute()
	{
		scoreBoard.incrementBalls();

		BallPoint[] b = new BallPoint[4];
		b[0] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  2);
		b[1] = new BallPoint( (double)GameConstants.posBall[2][0]+amount,
							  (double)GameConstants.posBall[2][1],
							  10);
		b[2] = new BallPoint( (double)GameConstants.posBall[2][0],
							  (double)GameConstants.posBall[2][1],
							  5);
		b[3] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  10);
		fieldView.pitch(b);
		fieldView.showPitchedBall(GameConstants.PITCHED_BALL_BALL);
	}
}