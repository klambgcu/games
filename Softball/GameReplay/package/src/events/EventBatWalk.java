package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.BallPoint;
import com.kelsoft.gamereplay.FieldView;
import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;
import com.kelsoft.gamereplay.Player;

public class EventBatWalk implements Command
{
	private FieldView      fieldView;
	private GameScoreBoard scoreBoard;
	private Player         player;

	public EventBatWalk(GameScoreBoard scoreBoard, FieldView fieldView, Player player)
	{
		this.fieldView  = fieldView;
		this.player     = player;
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.setAnimation( GameConstants.WALK );
		int[] path = {0,1};
		fieldView.advanceRunners(player, path);
		fieldView.fireBatterMovement();
	}
}