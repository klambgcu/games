package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.GameScoreBoard;

public class EventFieldingOut implements Command
{
	private GameScoreBoard scoreBoard;

	public EventFieldingOut(GameScoreBoard scoreBoard)
	{
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.incrementOuts();
	}
}