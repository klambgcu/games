package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.BallPoint;
import com.kelsoft.gamereplay.FieldView;
import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;
import com.kelsoft.gamereplay.Player;

public class EventNewBatter implements Command
{
	private FieldView      fieldView;
	private GameScoreBoard scoreBoard;
	private Player         player;

	public EventNewBatter(GameScoreBoard scoreBoard, FieldView fieldView, Player player)
	{
		this.fieldView  = fieldView;
		this.player     = player;
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		fieldView.setNewBatter(player);
		scoreBoard.newBatter();
	}
}