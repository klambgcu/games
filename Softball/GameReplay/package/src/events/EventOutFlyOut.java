package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.BallPoint;
import com.kelsoft.gamereplay.FieldView;
import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;

public class EventOutFlyOut implements Command
{
	private FieldView      fieldView;
	private GameScoreBoard scoreBoard;
	private int            placement;

	public EventOutFlyOut(GameScoreBoard scoreBoard, FieldView fieldView, int placement)
	{
		this.fieldView  = fieldView;
		this.scoreBoard = scoreBoard;
		this.placement  = placement;
	}

	public void execute()
	{
		scoreBoard.incrementOuts();
		scoreBoard.setAnimation( GameConstants.FLY_OUT );

		int amount = (placement > 6) ? 6 : 5;
		BallPoint[] b = new BallPoint[amount];
		b[0] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  2);
		b[1] = new BallPoint( (double)GameConstants.posBallHit[0],
							  (double)GameConstants.posBallHit[1],
							  10);
		b[2] = new BallPoint( (double)GameConstants.posBall[placement][0],
							  (double)GameConstants.posBall[placement][1],
							  20,
							  GameConstants.BALLPATH_FLYBALL);
		b[3] = new BallPoint( (double)GameConstants.posBall[placement][0],
							  (double)GameConstants.posBall[placement][1],
							  5); // Pause after catch
		if ((placement == 7) || (placement == 8))
		{
			b[4] = new BallPoint( (double)GameConstants.posBall[6][0],
								  (double)GameConstants.posBall[6][1],
								  10);
			b[5] = new BallPoint( (double)GameConstants.posBall[1][0],
								  (double)GameConstants.posBall[1][1],
								  10);
		}
		else if (placement == 9)
		{
			b[4] = new BallPoint( (double)GameConstants.posBall[4][0],
								  (double)GameConstants.posBall[4][1],
								  10);
			b[5] = new BallPoint( (double)GameConstants.posBall[1][0],
								  (double)GameConstants.posBall[1][1],
								  10);
		}
		else
		{
			b[4] = new BallPoint( (double)GameConstants.posBall[1][0],
								  (double)GameConstants.posBall[1][1],
								  10);
		}
		fieldView.pitch(b);
	}
}