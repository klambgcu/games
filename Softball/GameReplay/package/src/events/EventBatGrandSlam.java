package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.BallPoint;
import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;
import com.kelsoft.gamereplay.FieldView;
import com.kelsoft.gamereplay.Player;

public class EventBatGrandSlam implements Command
{
	private FieldView      fieldView;
	private GameScoreBoard scoreBoard;
	private int            placement;
	private Player         player;
	private String         hitType;

	public EventBatGrandSlam(GameScoreBoard scoreBoard, FieldView fieldView, Player player, String hitType, int placement)
	{
		this.fieldView  = fieldView;
		this.hitType    = hitType;
		this.placement  = placement;
		this.player     = player;
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.incrementHits();
		scoreBoard.setAnimation( GameConstants.GRAND_SLAM );

		int ballPath = GameConstants.BALLPATH_STRAIGHT;
		if (hitType.equals("F"))
			ballPath = GameConstants.BALLPATH_FLYBALL;
		else if (hitType.equals("G"))
			ballPath = GameConstants.BALLPATH_GROUNDER;

		BallPoint[] b = new BallPoint[5];
		b[0] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  2);
		b[1] = new BallPoint( (double)GameConstants.posBallHit[0],
							  (double)GameConstants.posBallHit[1],
							  10);
		b[2] = new BallPoint( (double)GameConstants.posBall[placement][0],
							  (double)GameConstants.posBall[0][1],
							  20,
							  ballPath);
		b[3] = new BallPoint( (double)GameConstants.posBall[placement][0],
							  (double)GameConstants.posBall[0][1],
							  20);
		b[4] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  10);
		fieldView.pitch(b);
		int[] path = {0,1,2,3,4};
		fieldView.advanceRunners(player, path);
	}
}