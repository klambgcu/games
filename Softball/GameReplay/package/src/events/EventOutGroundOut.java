package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.BallPoint;
import com.kelsoft.gamereplay.FieldView;
import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;

public class EventOutGroundOut implements Command
{
	private FieldView      fieldView;
	private GameScoreBoard scoreBoard;
	private int            placement1;
	private int            placement2;

	public EventOutGroundOut(GameScoreBoard scoreBoard, FieldView fieldView, int placement1, int placement2)
	{
		this.fieldView  = fieldView;
		this.scoreBoard = scoreBoard;
		this.placement1 = placement1;
		this.placement2 = placement2;
	}

	public void execute()
	{
		scoreBoard.incrementOuts();
		scoreBoard.setAnimation( GameConstants.GROUND_OUT );

		BallPoint[] b = new BallPoint[7];
		b[0] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  2);
		b[1] = new BallPoint( (double)GameConstants.posBallHit[0],
							  (double)GameConstants.posBallHit[1],
							  10);
		b[2] = new BallPoint( (double)GameConstants.posBall[placement1][0],
							  (double)GameConstants.posBall[placement1][1],
							  20,
							  GameConstants.BALLPATH_GROUNDER);
		b[3] = new BallPoint( (double)GameConstants.posBall[placement1][0],
							  (double)GameConstants.posBall[placement1][1],
							  2);
		b[4] = new BallPoint( (double)GameConstants.posBall[placement2][0],
							  (double)GameConstants.posBall[placement2][1],
							  10);
		b[5] = new BallPoint( (double)GameConstants.posBall[placement2][0],
							  (double)GameConstants.posBall[placement2][1],
							  2);
		b[6] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  10);
		fieldView.pitch(b);
	}
}