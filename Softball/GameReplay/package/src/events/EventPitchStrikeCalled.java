package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.BallPoint;
import com.kelsoft.gamereplay.FieldView;
import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;

public class EventPitchStrikeCalled implements Command
{
	private FieldView      fieldView;
	private GameScoreBoard scoreBoard;

	public EventPitchStrikeCalled(GameScoreBoard scoreBoard, FieldView fieldView)
	{
		this.fieldView  = fieldView;
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.incrementStrikes();

		BallPoint[] b = new BallPoint[4];
		b[0] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  2);
		b[1] = new BallPoint( (double)GameConstants.posBall[2][0],
							  (double)GameConstants.posBall[2][1],
							  10);
		b[2] = new BallPoint( (double)GameConstants.posBall[2][0],
							  (double)GameConstants.posBall[2][1],
							  5);
		b[3] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  10);
		fieldView.pitch(b);
		fieldView.showPitchedBall(GameConstants.PITCHED_BALL_STRIKE);
	}
}