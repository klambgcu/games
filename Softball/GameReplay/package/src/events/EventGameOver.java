package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;

public class EventGameOver implements Command
{
	private GameScoreBoard scoreBoard;

	public EventGameOver(GameScoreBoard scoreBoard)
	{
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.setAnimation( GameConstants.GAME_OVER );
	}
}