package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.BallPoint;
import com.kelsoft.gamereplay.FieldView;
import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;
import com.kelsoft.gamereplay.Player;

public class EventBatSingle implements Command
{
	private FieldView      fieldView;
	private GameScoreBoard scoreBoard;
	private int            placement;
	private Player         player;
	private String         hitType;

	public EventBatSingle(GameScoreBoard scoreBoard, FieldView fieldView, Player player, String hitType, int placement)
	{
		this.fieldView  = fieldView;
		this.hitType    = hitType;
		this.placement  = placement;
		this.player     = player;
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.incrementHits();
		scoreBoard.setAnimation( GameConstants.SINGLE );

		int ballPath = GameConstants.BALLPATH_STRAIGHT;
		if (hitType.equals("F"))
			ballPath = GameConstants.BALLPATH_FLYBALL;
		else if (hitType.equals("G"))
			ballPath = GameConstants.BALLPATH_GROUNDER;

		int amount = (placement > 6) ? 5 : 4;
		BallPoint[] b = new BallPoint[amount];
		b[0] = new BallPoint( (double)GameConstants.posBall[1][0],
							  (double)GameConstants.posBall[1][1],
							  2);
		b[1] = new BallPoint( (double)GameConstants.posBallHit[0],
							  (double)GameConstants.posBallHit[1],
							  10);
		b[2] = new BallPoint( (double)GameConstants.posBall[placement][0],
							  (double)GameConstants.posBall[placement][1],
							  20,
							  ballPath);
		if ((placement == 7) || (placement == 8))
		{
			b[3] = new BallPoint( (double)GameConstants.posBall[6][0],
								  (double)GameConstants.posBall[6][1],
								  10);
			b[4] = new BallPoint( (double)GameConstants.posBall[1][0],
								  (double)GameConstants.posBall[1][1],
								  10);
		}
		else if (placement == 9)
		{
			b[3] = new BallPoint( (double)GameConstants.posBall[4][0],
								  (double)GameConstants.posBall[4][1],
								  10);
			b[4] = new BallPoint( (double)GameConstants.posBall[1][0],
								  (double)GameConstants.posBall[1][1],
								  10);
		}
		else
		{
			b[3] = new BallPoint( (double)GameConstants.posBall[1][0],
								  (double)GameConstants.posBall[1][1],
								  10);
		}
		fieldView.pitch(b);
		int[] path = {0,1};
		fieldView.advanceRunners(player, path);
	}
}