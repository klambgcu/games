package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;

public class EventRunPickedOff implements Command
{
	private GameScoreBoard scoreBoard;

	public EventRunPickedOff(GameScoreBoard scoreBoard)
	{
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.incrementOuts();
		scoreBoard.setAnimation( GameConstants.PICKED_OFF );
	}
}