package com.kelsoft.gamereplay.events;

import com.kelsoft.gamereplay.GameConstants;
import com.kelsoft.gamereplay.GameScoreBoard;

public class EventRunSteal implements Command
{
	private GameScoreBoard scoreBoard;

	public EventRunSteal(GameScoreBoard scoreBoard)
	{
		this.scoreBoard = scoreBoard;
	}

	public void execute()
	{
		scoreBoard.setAnimation( GameConstants.STEAL );
	}
}