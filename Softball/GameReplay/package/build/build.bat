cls
cd ..
javac -d build src\gamereplay\*.java src\events\*.java
cd build

del GameReplay.jar
jar -cfm GameReplay.jar META-INF/MANIFEST.MF com
java -jar GameReplay.jar
