import java.net.*;
import java.io.*;
import java.util.*;

public class FileServer
{
	private static final byte[] EOL = {(byte)'\r', (byte)'\n' };
	private static Hashtable<String,String> content_type = new Hashtable<String,String>();

	public static void main(String[] args)
	{
		// read arguments
		if (args.length!=2)
		{
			System.out.println("Usage: java FileServer <port> <wwwhome>");
			System.exit(-1);
		}
		int port = Integer.parseInt(args[0]);
		String wwwhome = args[1];

		content_type.put("",        "content/unknown");
		content_type.put(".323",    "text/h323");
		content_type.put(".acx",    "application/internet-property-stream");
		content_type.put(".ai",     "application/postscript");
		content_type.put(".aif",    "audio/x-aiff");
		content_type.put(".aifc",   "audio/x-aiff");
		content_type.put(".aiff",   "audio/x-aiff");
		content_type.put(".asf",    "video/x-ms-asf");
		content_type.put(".asr",    "video/x-ms-asf");
		content_type.put(".asx",    "video/x-ms-asf");
		content_type.put(".au",     "audio/basic");
		content_type.put(".avi",    "video/x-msvideo");
		content_type.put(".axs",    "application/olescript");
		content_type.put(".bas",    "text/plain");
		content_type.put(".bcpio",  "application/x-bcpio");
		content_type.put(".bin",    "application/octet-stream");
		content_type.put(".bmp",    "image/bmp");
		content_type.put(".c",      "text/plain");
		content_type.put(".c++",    "text/plain");
		content_type.put(".cat",    "application/vnd.ms-pkiseccat");
		content_type.put(".cc",     "text/plain");
		content_type.put(".cdf",    "application/x-cdf");
		content_type.put(".cer",    "application/x-x509-ca-cert");
		content_type.put(".class",  "application/octet-stream");
		content_type.put(".clp",    "application/x-msclip");
		content_type.put(".cmx",    "image/x-cmx");
		content_type.put(".cod",    "image/cis-cod");
		content_type.put(".cpio",   "application/x-cpio");
		content_type.put(".crd",    "application/x-mscardfile");
		content_type.put(".crl",    "application/pkix-crl");
		content_type.put(".crt",    "application/x-x509-ca-cert");
		content_type.put(".csh",    "application/x-csh");
		content_type.put(".css",    "text/css");
		content_type.put(".dcr",    "application/x-director");
		content_type.put(".der",    "application/x-x509-ca-cert");
		content_type.put(".dir",    "application/x-director");
		content_type.put(".dll",    "application/x-msdownload");
		content_type.put(".dms",    "application/octet-stream");
		content_type.put(".doc",    "application/msword");
		content_type.put(".dot",    "application/msword");
		content_type.put(".dvi",    "application/x-dvi");
		content_type.put(".dxr",    "application/x-director");
		content_type.put(".eps",    "application/postscript");
		content_type.put(".etx",    "text/x-setext");
		content_type.put(".evy",    "application/envoy");
		content_type.put(".exe",    "application/octet-stream");
		content_type.put(".fif",    "application/fractals");
		content_type.put(".flr",    "x-world/x-vrml");
		content_type.put(".gif",    "image/gif");
		content_type.put(".gtar",   "application/x-gtar");
		content_type.put(".gz",     "application/x-gzip");
		content_type.put(".h",      "text/plain");
		content_type.put(".hdf",    "application/x-hdf");
		content_type.put(".hlp",    "application/winhlp");
		content_type.put(".hqx",    "application/mac-binhex40");
		content_type.put(".hta",    "application/hta");
		content_type.put(".htc",    "text/x-component");
		content_type.put(".htm",    "text/html");
		content_type.put(".html",   "text/html");
		content_type.put(".htt",    "text/webviewhtml");
		content_type.put(".ico",    "image/x-icon");
		content_type.put(".ief",    "image/ief");
		content_type.put(".iii",    "application/x-iphone");
		content_type.put(".ins",    "application/x-internet-signup");
		content_type.put(".isp",    "application/x-internet-signup");
		content_type.put(".java",   "text/plain");
		content_type.put(".jfif",   "image/pipeg");
		content_type.put(".jpe",    "image/jpeg");
		content_type.put(".jpeg",   "image/jpeg");
		content_type.put(".jpg",    "image/jpeg");
		content_type.put(".js",     "application/x-javascript");
		content_type.put(".ksh",    "application/x-ksh");
		content_type.put(".lha",    "application/octet-stream");
		content_type.put(".lsf",    "video/x-la-asf");
		content_type.put(".lsx",    "video/x-la-asf");
		content_type.put(".lzh",    "application/octet-stream");
		content_type.put(".m13",    "application/x-msmediaview");
		content_type.put(".m14",    "application/x-msmediaview");
		content_type.put(".m3u",    "audio/x-mpegurl");
		content_type.put(".man",    "application/x-troff-man");
		content_type.put(".man",    "application/x-troff-man");
		content_type.put(".mdb",    "application/x-msaccess");
		content_type.put(".me",     "application/x-troff-me");
		content_type.put(".mht",    "message/rfc822");
		content_type.put(".mhtml",  "message/rfc822");
		content_type.put(".mid",    "audio/mid");
		content_type.put(".mny",    "application/x-msmoney");
		content_type.put(".mov",    "video/quicktime");
		content_type.put(".movie",  "video/x-sgi-movie");
		content_type.put(".mp2",    "video/mpeg");
		content_type.put(".mp3",    "audio/mpeg");
		content_type.put(".mpa",    "video/mpeg");
		content_type.put(".mpe",    "video/mpeg");
		content_type.put(".mpeg",   "video/mpeg");
		content_type.put(".mpg",    "video/mpeg");
		content_type.put(".mpp",    "application/vnd.ms-project");
		content_type.put(".mpv2",   "video/mpeg");
		content_type.put(".ms",     "application/x-troff-ms");
		content_type.put(".mvb",    "application/x-msmediaview");
		content_type.put(".nws",    "message/rfc822");
		content_type.put(".oda",    "application/oda");
		content_type.put(".p10",    "application/pkcs10");
		content_type.put(".p12",    "application/x-pkcs12");
		content_type.put(".p7b",    "application/x-pkcs7-certificates");
		content_type.put(".p7c",    "application/x-pkcs7-mime");
		content_type.put(".p7m",    "application/x-pkcs7-mime");
		content_type.put(".p7r",    "application/x-pkcs7-certreqresp");
		content_type.put(".p7s",    "application/x-pkcs7-signature");
		content_type.put(".pbm",    "image/x-portable-bitmap");
		content_type.put(".pdf",    "application/pdf");
		content_type.put(".pfx",    "application/x-pkcs12");
		content_type.put(".pgm",    "image/x-portable-graymap");
		content_type.put(".pko",    "application/ynd.ms-pkipko");
		content_type.put(".pl",     "text/plain");
		content_type.put(".pma",    "application/x-perfmon");
		content_type.put(".pmc",    "application/x-perfmon");
		content_type.put(".pml",    "application/x-perfmon");
		content_type.put(".pmr",    "application/x-perfmon");
		content_type.put(".pmw",    "application/x-perfmon");
		content_type.put(".pnm",    "image/x-portable-anymap");
		content_type.put(".pot,",   "application/vnd.ms-powerpoint");
		content_type.put(".ppm",    "image/x-portable-pixmap");
		content_type.put(".pps",    "application/vnd.ms-powerpoint");
		content_type.put(".ppt",    "application/vnd.ms-powerpoint");
		content_type.put(".prf",    "application/pics-rules");
		content_type.put(".proc",   "text/plain");
		content_type.put(".ps",     "application/postscript");
		content_type.put(".pub",    "application/x-mspublisher");
		content_type.put(".qt",     "video/quicktime");
		content_type.put(".ra",     "audio/x-pn-realaudio");
		content_type.put(".ram",    "audio/x-pn-realaudio");
		content_type.put(".ras",    "image/x-cmu-raster");
		content_type.put(".rgb",    "image/x-rgb");
		content_type.put(".rmi",    "audio/mid");
		content_type.put(".roff",   "application/x-troff");
		content_type.put(".rtf",    "application/rtf");
		content_type.put(".rtx",    "text/richtext");
		content_type.put(".scd",    "application/x-msschedule");
		content_type.put(".sct",    "text/scriptlet");
		content_type.put(".setpay", "application/set-payment-initiation");
		content_type.put(".setreg", "application/set-registration-initiation");
		content_type.put(".sh",     "application/x-sh");
		content_type.put(".shar",   "application/x-shar");
		content_type.put(".sit",    "application/x-stuffit");
		content_type.put(".snd",    "audio/basic");
		content_type.put(".spc",    "application/x-pkcs7-certificates");
		content_type.put(".spl",    "application/futuresplash");
		content_type.put(".sql",    "text/plain");
		content_type.put(".src",    "application/x-wais-source");
		content_type.put(".sst",    "application/vnd.ms-pkicertstore");
		content_type.put(".stl",    "application/vnd.ms-pkistl");
		content_type.put(".stm",    "text/html");
		content_type.put(".sv4cpio","application/x-sv4cpio");
		content_type.put(".sv4crc", "application/x-sv4crc");
		content_type.put(".svg",    "image/svg+xml");
		content_type.put(".swf",    "application/x-shockwave-flash");
		content_type.put(".t",      "application/x-troff");
		content_type.put(".tar",    "application/x-tar");
		content_type.put(".tcl",    "application/x-tcl");
		content_type.put(".tex",    "application/x-tex");
		content_type.put(".texi",   "application/x-texinfo");
		content_type.put(".texinfo","application/x-texinfo");
		content_type.put(".text",   "text/plain");
		content_type.put(".tgz",    "application/x-compressed");
		content_type.put(".tif",    "image/tiff");
		content_type.put(".tiff",   "image/tiff");
		content_type.put(".tr",     "application/x-troff");
		content_type.put(".trm",    "application/x-msterminal");
		content_type.put(".tsv",    "text/tab-separated-values");
		content_type.put(".txt",    "text/plain");
		content_type.put(".uls",    "text/iuls");
		content_type.put(".ustar",  "application/x-ustar");
		content_type.put(".uu",     "application/octet-stream");
		content_type.put(".vcf",    "text/x-vcard");
		content_type.put(".vrml",   "x-world/x-vrml");
		content_type.put(".wav",    "audio/x-wav");
		content_type.put(".wcm",    "application/vnd.ms-works");
		content_type.put(".wdb",    "application/vnd.ms-works");
		content_type.put(".wks",    "application/vnd.ms-works");
		content_type.put(".wmf",    "application/x-msmetafile");
		content_type.put(".wps",    "application/vnd.ms-works");
		content_type.put(".wri",    "application/x-mswrite");
		content_type.put(".wrl",    "x-world/x-vrml");
		content_type.put(".wrz",    "x-world/x-vrml");
		content_type.put(".xaf",    "x-world/x-vrml");
		content_type.put(".xbm",    "image/x-xbitmap");
		content_type.put(".xla",    "application/vnd.ms-excel");
		content_type.put(".xlc",    "application/vnd.ms-excel");
		content_type.put(".xlm",    "application/vnd.ms-excel");
		content_type.put(".xls",    "application/vnd.ms-excel");
		content_type.put(".xlt",    "application/vnd.ms-excel");
		content_type.put(".xlw",    "application/vnd.ms-excel");
		content_type.put(".xof",    "x-world/x-vrml");
		content_type.put(".xpm",    "image/x-xpixmap");
		content_type.put(".xwd",    "image/x-xwindowdump");
		content_type.put(".z",      "application/x-compress");
		content_type.put(".zip",    "application/zip");

		// open server socket
		ServerSocket socket = null;
		try
		{
			socket = new ServerSocket(port);
		}
		catch (IOException e)
		{
			System.err.println("Could not start server: " + e);
			System.exit(-1);
		}
		System.out.println("FileServer accepting connections on port " + port);

		// request handler loop
		while (true)
		{
			Socket connection = null;
			try
			{
				// wait for request
				connection = socket.accept();
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				OutputStream out = new BufferedOutputStream(connection.getOutputStream());
				PrintStream pout = new PrintStream(out);

				// read first line of request (ignore the rest)
				String request = in.readLine();
				if (request==null)
					continue;
				request = request.replace("%20"," ");
				log(connection, request);
				while (true)
				{
					String misc = in.readLine();
					if (misc==null || misc.length()==0)
					break;
				}

				// parse the line
				if (!request.startsWith("GET") || request.length()<14 ||
					!(request.endsWith("HTTP/1.0") || request.endsWith("HTTP/1.1")))
				{
					// bad request
					errorReport(pout, connection, "400", "Bad Request",
								"Your browser sent a request that " +
								"this server could not understand.");
				}
				else
				{
					String req = request.substring(4, request.length()-9).trim();
					if (req.indexOf("..")!=-1 ||
						req.indexOf("/.ht")!=-1 ||
						req.endsWith("~"))
					{
						// evil hacker trying to read non-wwwhome or secret file
						errorReport(pout, connection, "403", "Forbidden",
									"You don't have permission to access the requested URL.");
					}
					else
					{
						if (req.length() > 0)
						{
							String path = wwwhome + "/" + req;
							File f = new File(path);
							if (f.isDirectory() && !path.endsWith("/"))
							{
								// redirect browser if referring to directory without final '/'
								pout.print("HTTP/1.0 301 Moved Permanently\r\n" +
											"Location: http://" +
											connection.getLocalAddress().getHostAddress() + ":" +
											connection.getLocalPort() + "/" + req + "/\r\n\r\n");
								log(connection, "301 Moved Permanently");
							}
							else
							{
								if (f.isDirectory())
								{
									File ind = new File(f, "index.html");
									if (ind.exists())
									{
										f = ind;
										path = path + "index.html";
									}
									// if directory, implicitly add 'index.html'
									//path = path + "index.html";
									//f = new File(path);
								}
								try
								{
									pout.print("HTTP/1.0 200 OK\r\n" +
												"Content-Type: " + guessContentType(path) + "\r\n" +
												"Date: " + new Date() + "\r\n" +
												"Server: FileServer 1.0\r\n");
									// send file
									//InputStream file = new FileInputStream(f);
									sendFile(f, out, pout); // send raw file
									log(connection, "200 OK");
								}
								catch (FileNotFoundException e)
								{
									// file not found
									errorReport(pout, connection, "404", "Not Found",
												"The requested URL was not found on this server.");
								}
								catch (IOException e)
								{
									// file not found
									errorReport(pout, connection, "404", "Not Found",
												"The requested URL was not found on this server.");
								}
							}
						}
					}
				}
				out.flush();
			}
			catch (IOException e)
			{
				System.err.println(e);
			}
			try
			{
				if (connection != null) connection.close();
			}
			catch (IOException e)
			{
				System.err.println(e);
			}
		}
	}

	private static void log(Socket connection, String msg)
	{
		System.err.println(new Date() + " [" +
							connection.getInetAddress().getHostAddress() + ":" +
							connection.getPort() + "] " + msg);
	}

	private static void errorReport(PrintStream pout,
									Socket connection,
									String code,
									String title,
									String msg)
	{
		pout.print("HTTP/1.0 " + code + " " + title + "\r\n" +
					"\r\n" +
					"<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n" +
					"<TITLE>" + code + " " + title + "</TITLE>\r\n" +
					"</HEAD><BODY>\r\n" +
					"<H1>" + title + "</H1>\r\n" + msg + "<P>\r\n" +
					"<HR><ADDRESS>FileServer 1.0 at " +
					connection.getLocalAddress().getHostName() +
					" Port " + connection.getLocalPort() + "</ADDRESS>\r\n" +
					"</BODY></HTML>\r\n");
		log(connection, code + " " + title);
	}

	private static String guessContentType(String path)
	{
		int    ind  = path.lastIndexOf('.');
		String ct   = null;
		if (ind > 0)
		{
			ct = (String) content_type.get(path.substring(ind));
		}
		if (ct == null)
		{
			ct = "text/html";
		}
		return ct;
	}

	private static void sendFile(File targ, OutputStream out, PrintStream ps) throws FileNotFoundException, IOException
	{
		InputStream file = null;
		try
		{
			ps.write(EOL);
			if (targ.isDirectory())
			{
				listDirectory(targ, ps);
				return;
			}
			else
			{
				file = new FileInputStream(targ.getAbsolutePath());
			}
		}
		catch (FileNotFoundException e)
		{
			throw e;
		}

		catch (IOException e)
		{
			throw e;
		}

		try
		{
			byte[] buffer = new byte[1000];
			while (file.available()>0)
				out.write(buffer, 0, file.read(buffer));
		}
		catch (IOException e)
		{
			System.err.println(e);
		}
		finally
		{
			file.close();
		}
	}

	private static void listDirectory(File dir, PrintStream ps)
	{
		String[] list = dir.list();
		java.util.Arrays.sort(list, String.CASE_INSENSITIVE_ORDER);

		ps.println("<HTML><HEAD>");
		ps.println("<TITLE>Directory listing</TITLE><P>");
		ps.println("<STYLE>TABLE {FONT: 12px Arial;}</STYLE>");
		ps.println("</HEAD>\n<BODY>");
		ps.println("<P><I>" + (new Date()) + "</I><HR><BR>");
		ps.println("<TABLE border=0>");
		ps.println("<TR><TH width=300px align=left>Filename</TH><TH width=100px align=right>Size</TH></TR>");
		ps.println("<TR><TD><A HREF=\"..\">Parent Directory</A></TD><TD>&nbsp;</TD></TR>");

		for (int i = 0; list != null && i < list.length; i++)
		{
			File f = new File(dir, list[i]);
			if (f.isDirectory())
			{
				ps.println("<TR><TD><A HREF=\""+list[i]+"/\">"+list[i]+"/</A></TD><TD>&nbsp;</TD></TR>");
			}
			else
			{
				ps.println("<TR><TD><A HREF=\""+list[i]+"\">"+list[i]+"</A></TD><TD align=right>"+f.length()+"</TD></TR>");
			}
		}
		ps.println("</TABLE></BODY></HTML>");
	}
}
